# RobotAPI

cmake_minimum_required(VERSION 2.8)

find_package("ArmarXCore" REQUIRED
    PATHS "$ENV{HOME}/armarx/Core/build"
)

include(${ArmarXCore_USE_FILE})

set(ARMARX_ENABLE_DEPENDENCY_VERSION_CHECK_DEFAULT TRUE)
set(ARMARX_ENABLE_AUTO_CODE_FORMATTING TRUE)

armarx_project("RobotAPI")
depends_on_armarx_package(ArmarXGui "OPTIONAL")

set(ArmarX_Simox_VERSION 2.3.9)

add_subdirectory(source)

list(APPEND CPACK_DEBIAN_PACKAGE_DEPENDS "simox")

install_project()

add_subdirectory(scenarios)
