# Add your components below as shown in the following example:

set(SCENARIO_COMPONENTS
    KinematicUnitSimulation
    KinematicUnitObserver
    SystemObserver
    ConditionHandler
    RobotStateComponent
    RobotControl
    MotionControlTest
    )

# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario("MotionControlTest" "${SCENARIO_COMPONENTS}" "./config/Global.cfg")

#set(SCENARIO_CONFIGS
#    config/ComponentName.optionalString.cfg
#    )
# optional 3rd parameter: "path/to/global/config.cfg"
#armarx_scenario_from_configs("statecharttestscenario" "${SCENARIO_CONFIGS}")
