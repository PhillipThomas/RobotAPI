
export CORE_PATH=../../../Core
export GUI_PATH=../../../Gui
export SCRIPT_PATH=$CORE_PATH/build/bin

export GUI_BIN_PATH=$GUI_PATH/build/bin

# Gui
$SCRIPT_PATH/startApplication.sh $GUI_BIN_PATH/ArmarXGuiRun --Ice.Config=./configs/ArmarXLogger.cfg &
