# RemoteRobotTestProject project

cmake_minimum_required(VERSION 2.8)
if (COMMAND cmake_policy)
    if (POLICY CMP0011)
        cmake_policy(SET CMP0011 NEW)
    endif()
endif()

find_package("ArmarXCore" REQUIRED
    PATHS "$ENV{HOME}/armarx/Core/build/cmake"
    	  "$ENV{HOME}/Projects/armarx-8.04/Core/build/cmake"
          "$ENV{HOME}/armarx-install/share/cmake/ArmarXCore"
          "/org/share/archive/SFB588_RefDist/armarx/share/cmake/ArmarXCore"
)


include(${ArmarXCore_CMAKE_DIR}/ArmarXProject.cmake)

armarx_project("RemoteRobotTestProject")

#add_subdirectory(interface)


#########################################################################
#
# start of RemoteRobotTestProject content
#
#########################################################################

armarx_component_set_name(RemoteRobotTestProject)

set(COMPONENT_BUILD TRUE)

find_package(Eigen3 QUIET)
find_package(Simox QUIET)

armarx_component_if(COMPONENT_BUILD "component disabled")
armarx_component_if(Eigen3_FOUND "Eigen3 not available")
armarx_component_if(Simox_FOUND "VirtualRobot not available")


if (ARMARX_BUILD)

    include_directories(${ArmarXCore_INCLUDE_DIRS} )
    include_directories(${Eigen3_INCLUDE_DIR})
    include_directories(${Simox_INCLUDE_DIR})
    
    set(COMPONENT_LIBS RemoteRobotTestProjectInterfaces ArmarXInterfaces ArmarXCoreRemoteRobot ArmarXCore  ${Simox_LIBRARIES} ${Coin3D_LIBRARIES})

    set(SOURCES RemoteRobotTestProject.cpp
        RemoteRobotTestProject.h
        RemoteRobotTestProjectApp.h)

    armarx_add_component_executable("${SOURCES}")

    armarx_add_component_test(RemoteRobotTestProjectTest test/RemoteRobotTestProjectTest.cpp)
endif()

#########################################################################

install_project()
