/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RemoteRobotTestProject::
* @author     ( at kit dot edu)
* @date       
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef ARMARX_COMPONENT_RemoteRobotTestProject_RemoteRobotTestProject_H
#define ARMARX_COMPONENT_RemoteRobotTestProject_RemoteRobotTestProject_H

#include <Core/core/Component.h>
#include <Core/core/ImportExportComponent.h>
//#include <interface/cpp/RemoteRobotTestProject/RemoteRobotTestProject.h>

#include <VirtualRobot/VirtualRobot.h>
#include <Core/core/remoterobot/RemoteRobot.h>

namespace armarx
{
	class ARMARXCOMPONENT_IMPORT_EXPORT RemoteRobotTestProject :
		virtual public Component
	{
		public:
			// inherited from Component
			virtual std::string getDefaultName() { return "RemoteRobotTestProject"; };
			virtual void onInitComponent();
			virtual void onStartComponent();

		private: 
			std::string robotNodeSetName;
			RobotStateComponentInterfacePrx server;
			VirtualRobot::RobotPtr  robot ;
	};

}

#endif
