armarx_component_set_name("HapticObserverApp")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")

if (Eigen3_FOUND)
    include_directories(
        ${Eigen3_INCLUDE_DIR})
endif()

set(COMPONENT_LIBS RobotAPIUnits)

set(EXE_SOURCE main.cpp HapticObserverApp.h)

armarx_add_component_executable("${EXE_SOURCE}")
