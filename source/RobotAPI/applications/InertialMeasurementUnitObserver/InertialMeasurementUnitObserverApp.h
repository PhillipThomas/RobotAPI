/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::application::InertialMeasurementUnitObserver
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_APPLICATION_RobotAPI_InertialMeasurementUnitObserver_H
#define _ARMARX_APPLICATION_RobotAPI_InertialMeasurementUnitObserver_H


#include <RobotAPI/components/units/InertialMeasurementUnitObserver.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>


namespace armarx
{
    /**
     * @class InertialMeasurementUnitObserverApp
     * @brief A brief description
     *
     * Detailed Description
     */
    class InertialMeasurementUnitObserverApp :
        virtual public armarx::Application
    {
        /**
         * @see armarx::Application::setup()
         */
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties)
        {
            registry->addObject(Component::create<InertialMeasurementUnitObserver>(properties));
        }
    };
}

#endif
