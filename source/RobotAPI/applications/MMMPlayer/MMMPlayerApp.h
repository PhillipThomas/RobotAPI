/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::application::MMMPlayer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_APPLICATION_RobotAPI_MMMPlayer_H
#define _ARMARX_APPLICATION_RobotAPI_MMMPlayer_H


#include <RobotAPI/components/MMMPlayer/MMMPlayer.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>


namespace armarx
{
    /**
     * @class MMMPlayerApp
     * @brief A brief description
     *
     * Detailed Description
     */
    class MMMPlayerApp :
        virtual public armarx::Application
    {
        /**
         * @see armarx::Application::setup()
         */
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties)
        {
            registry->addObject(Component::create<MMMPlayer>(properties));
        }
    };
}

#endif
