/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::applications
 * @author     Kai Welke (weöle dot at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/core/application/Application.h>
#include <RobotAPI/components/units/TCPControlUnit.h>
#include <RobotAPI/components/units/TCPControlUnitObserver.h>

namespace armarx
{
    class TCPControlUnitApp :
        virtual public armarx::Application
    {
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties)
        {
            registry->addObject(Component::create<TCPControlUnit>(properties));
            registry->addObject(Component::create<TCPControlUnitObserver>(properties));
        }
    };
}



