/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::application::XsensIMU
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_APPLICATION_RobotAPI_XsensIMU_H
#define _ARMARX_APPLICATION_RobotAPI_XsensIMU_H


#include <RobotAPI/libraries/drivers/XsensIMU/XsensIMU.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>


namespace armarx
{
    /**
     * @class XsensIMUApp
     * @brief A brief description
     *
     * Detailed Description
     */
    class XsensIMUApp :
        virtual public armarx::Application
    {
        /**
         * @see armarx::Application::setup()
         */
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties)
        {
            registry->addObject(Component::create<XsensIMU>(properties));
        }
    };
}

#endif
