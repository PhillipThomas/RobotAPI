
armarx_component_set_name(RobotIK)

find_package(Eigen3 QUIET)
find_package(Simox ${ArmarX_Simox_VERSION} QUIET)
armarx_build_if(Eigen3_FOUND "component disabled")
armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")

include_directories(${Eigen3_INCLUDE_DIR})
include_directories(${Simox_INCLUDE_DIRS})

set(COMPONENT_LIBS RobotAPICore RobotAPIRobotIK)

set(SOURCES main.cpp RobotIKApp.h)

armarx_add_component_executable("${SOURCES}")
