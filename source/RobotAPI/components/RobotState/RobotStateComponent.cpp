/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotStateComponent::
 * @author     ( at kit dot edu)
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotStateComponent.h"
#include <boost/foreach.hpp>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <boost/format.hpp>
#include <boost/foreach.hpp>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/application/Application.h>

using namespace std;
using namespace VirtualRobot;
using namespace Eigen;
using namespace Ice;
using namespace boost;

std::ofstream out_file;

namespace armarx
{
    RobotStateComponent::~RobotStateComponent()
    {
        try
        {
            if (_synchronizedPrx)
            {
                _synchronizedPrx->unref();
            }
        }
        catch (...)
        {}
    }


    void RobotStateComponent::onInitComponent()
    {
        relativeRobotFile = getProperty<std::string>("RobotFileName").getValue();

        if (!ArmarXDataPath::getAbsolutePath(relativeRobotFile, robotFile))
        {
            throw UserException("Could not find robot file " + robotFile);
        }

        this->_synchronized = VirtualRobot::RobotIO::loadRobot(robotFile, VirtualRobot::RobotIO::eStructure);
        _synchronized->setName(getProperty<std::string>("AgentName").getValue());

        if (this->_synchronized)
        {
            ARMARX_VERBOSE << "Loaded robot from file " << robotFile << ". Robot name: " << this->_synchronized->getName();
        }
        else
        {
            ARMARX_VERBOSE << "Failed loading robot from file " << robotFile;
        }

        string robotNodeSetName = getProperty<std::string>("RobotNodeSetName").getValue();


        if (robotNodeSetName == "")
        {
            throw UserException("RobotNodeSet not defined");
        }

        VirtualRobot::RobotNodeSetPtr rns =  this->_synchronized->getRobotNodeSet(robotNodeSetName);

        if (!rns)
        {
            throw UserException("RobotNodeSet not defined");
        }

        vector<RobotNodePtr> nodes = rns->getAllRobotNodes();

        ARMARX_VERBOSE << "Using RobotNodeSet " << robotNodeSetName << " with " << nodes.size() << " robot nodes.";
        BOOST_FOREACH(RobotNodePtr node, nodes)
        {
            ARMARX_VERBOSE << "Node: " << node->getName() << endl;
        }
        usingTopic(robotNodeSetName + "State");

        /*VirtualRobot::RobotNodeSetPtr pns = this->_synchronized->getRobotNodeSet("Platform");
        if (pns)
        {
            usingTopic("PlatformState");
            vector<RobotNodePtr> nodes = pns->getAllRobotNodes();

            ARMARX_VERBOSE << "Using RobotNodeSet Platform with " << nodes.size() << " robot nodes.";
            BOOST_FOREACH(RobotNodePtr node, nodes)
            {
                ARMARX_VERBOSE << "Node: " << node->getName() << endl;
            }
        }*/
        _sharedRobotServant =  new SharedRobotServant(this->_synchronized);
        _sharedRobotServant->ref();
    }


    void RobotStateComponent::onConnectComponent()
    {

        this->_synchronizedPrx = SharedRobotInterfacePrx::uncheckedCast(getObjectAdapter()->addWithUUID(_sharedRobotServant));
        ARMARX_INFO << "Started RobotStateComponent" << flush;
        robotStateObs->setRobot(_synchronized);
    }

    void RobotStateComponent::onDisconnectComponent()
    {
    }


    SharedRobotInterfacePrx RobotStateComponent::getSynchronizedRobot(const Current& current) const
    {
        if (!this->_synchronizedPrx)
        {
            throw NotInitializedException("Shared Robot is NULL", "getSynchronizedRobot");
        }

        return this->_synchronizedPrx;
    }


    SharedRobotInterfacePrx RobotStateComponent::getRobotSnapshot(const string& time, const Current& current)
    {
        if (!_synchronized)
        {
            throw NotInitializedException("Shared Robot is NULL", "getRobotSnapshot");
        }

        auto clone = this->_synchronized->clone(_synchronized->getName());

        SharedRobotInterfacePtr p = new SharedRobotServant(clone);
        auto result = getObjectAdapter()->addWithUUID(p);
        // virtal robot clone is buggy -> set global pose here
        p->setGlobalPose(new Pose(_synchronized->getGlobalPose()));
        return SharedRobotInterfacePrx::uncheckedCast(result);
    }

    void RobotStateComponent::reportJointAngles(const NameValueMap& jointAngles, bool aValueChanged, const Current& c)
    {
        //        IceUtil::Time start = IceUtil::Time::now();
        if (aValueChanged)
        {
            {
                WriteLockPtr lock = _synchronized->getWriteLock();

                std::vector<float> jv;
                std::vector< RobotNodePtr > nodes;
                BOOST_FOREACH(NameValueMap::value_type namedAngle, jointAngles)
                {
                    RobotNodePtr node = this->_synchronized->getRobotNode(namedAngle.first);
                    nodes.push_back(node);
                    jv.push_back(namedAngle.second);
                }
                _synchronized->setJointValues(nodes, jv);
            }

            if (robotStateObs)
            {
                robotStateObs->updatePoses();
            }
        }

        if (_sharedRobotServant)
        {
            _sharedRobotServant->setTimestamp(IceUtil::Time::now());
        }

        //        ARMARX_VERBOSE << deactivateSpam(2) << "my duration: " << (IceUtil::Time::now() - start).toMicroSecondsDouble() << " ms";


    }

    std::string RobotStateComponent::getRobotFilename(const Ice::Current&) const
    {
        return relativeRobotFile;
    }

    std::vector<string> RobotStateComponent::getArmarXPackages(const Current&) const
    {
        std::vector<string> result;
        auto packages = armarx::Application::GetProjectDependencies();
        packages.push_back(Application::GetProjectName());

        for (const std::string& projectName : packages)
        {
            if (projectName.empty())
            {
                continue;
            }

            result.push_back(projectName);
        }

        return result;
    }

    void RobotStateComponent::reportControlModeChanged(const NameControlModeMap& jointModes,  bool aValueChanged, const Current& c) {}
    void RobotStateComponent::reportJointVelocities(const NameValueMap& jointVelocities,  bool aValueChanged, const Current& c)
    {
        if (robotStateObs)
        {
            robotStateObs->updateNodeVelocities(jointVelocities);
        }
    }
    void RobotStateComponent::reportJointTorques(const NameValueMap& jointTorques,  bool aValueChanged, const Current& c) {}

    void RobotStateComponent::reportJointAccelerations(const NameValueMap& jointAccelerations, bool aValueChanged, const Current& c)
    {

    }
    void RobotStateComponent::reportJointCurrents(const NameValueMap& jointCurrents,  bool aValueChanged, const Current& c) {}
    void RobotStateComponent::reportJointMotorTemperatures(const NameValueMap& jointMotorTemperatures, bool aValueChanged, const Current& c) {}
    void RobotStateComponent::reportJointStatuses(const NameStatusMap& jointStatuses, bool aValueChanged, const Current& c) {}

    /*void RobotStateComponent::reportPlatformPose(float currentPlatformPositionX, float currentPlatformPositionY, float currentPlatformRotation, const Ice::Current& c)
    {
        if (!this->_synchronized->hasRobotNodeSet("Platform") && !this->_synchronized->hasRobotNode("Platform"))
        {
            return;
        }
        {
            WriteLockPtr lock = _synchronized->getWriteLock();
            _synchronized->setJointValue("X_Platform", currentPlatformPositionX);
            _synchronized->setJointValue("Y_Platform", currentPlatformPositionY);
            _synchronized->setJointValue("Yaw_Platform", currentPlatformRotation);
            _synchronized->applyJointValues();
        }
    }

    void RobotStateComponent::reportNewTargetPose(float newPlatformPositionX, float newPlatformPositionY, float newPlatformRotation, const Ice::Current& c)
    {
    }

    void RobotStateComponent::reportPlatformVelocity(float currentPlatformVelocityX, float currentPlatformVelocityY, float currentPlatformVelocityRotation, const Ice::Current &c)
    {

    }*/

    PropertyDefinitionsPtr RobotStateComponent::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new RobotStatePropertyDefinitions(
                                          getConfigIdentifier()));
    }

    void RobotStateComponent::setRobotStateObserver(RobotStateObserverPtr observer)
    {
        robotStateObs = observer;
    }

    string RobotStateComponent::getRobotName(const Current&) const
    {
        if (_synchronized)
        {
            return _synchronizedPrx->getName();
        }
        else
        {
            throw NotInitializedException("Robot Ptr is NULL", "getName");
        }

    }
}


