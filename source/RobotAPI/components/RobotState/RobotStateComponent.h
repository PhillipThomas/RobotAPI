/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotStateComponent::
 * @author     ( stefan dot ulbrich at kit dot edu)
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotStateComponent_RobotStateComponent_H
#define _ARMARX_COMPONENT_RobotStateComponent_RobotStateComponent_H

#include "SharedRobotServants.h"

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <RobotAPI/interface/core/RobotState.h>
#include "SharedRobotServants.h"

#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>

#include <RobotAPI/libraries/core/remoterobot/RobotStateObserver.h>

namespace armarx
{
    /**
     * \class RobotStatePropertyDefinition
     * \brief
     */
    class RobotStatePropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        RobotStatePropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("RobotNodeSetName", "Set of nodes that is controlled by the KinematicUnit");
            defineRequiredProperty<std::string>("RobotFileName", "Filename of VirtualRobot robot model (e.g. robot_model.xml)");
            defineRequiredProperty<std::string>("AgentName", "Name of the agent for which the sensor values are provided");
        }
    };


    /**
     * \defgroup Component-RobotStateComponent RobotStateComponent
     * \ingroup RobotAPI-Components
     * \brief Maintains a robot representation based on VirtualRobot (see [Simox](http://simox.sourceforge.net/)).
     *
     * The robot can be loaded from a Simox robot XML file.
     * Upon request, an Ice proxy to a shared instance of this internal robot
     * can be acquired through a call to RobotStateComponent::getSynchronizedRobot().
     * Additionally it is possible to retrieve a proxy for robot snapshot with
     * RobotStateComponent::getRobotSnapshot().
     * While the synchronized robot will constantly update its internal state
     * the robot snapshot is a clone of the original robot and won't update its
     * configuration over time.  This is useful, if several components need
     * to calculate on the same values.
     * See \ref armarx::RemoteRobot "RemoteRobot" for more details and the usage of this component.
     */

    /**
     * @brief The RobotStateComponent class
     * @ingroup Component-RobotStateComponent
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT RobotStateComponent :
        virtual public Component,
        virtual public RobotStateComponentInterface
    {
    public:


        /**
         * \return SharedRobotInterface proxy to the internal synchronized robot.
         */
        virtual SharedRobotInterfacePrx getSynchronizedRobot(const Ice::Current&) const;

        /**
         * \return Clone of the internal synchronized robot.
         */
        virtual SharedRobotInterfacePrx getRobotSnapshot(const std::string& time, const Ice::Current&);

        /**
         * \return the robot xml filename as specified in the configuration
         */
        virtual std::string getRobotFilename(const Ice::Current&) const;

        /*!
         * \brief getArmarXPackages
         * \return All dependent packages, which might contain a robot file.
         */
        virtual std::vector< std::string > getArmarXPackages(const Ice::Current&) const;

        /**
         *
         * \return  The name of this robot instance.
         */
        virtual std::string getRobotName(const Ice::Current&) const;

        /**
         * Create an instance of RobotStatePropertyDefinitions.
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        virtual std::string getDefaultName() const
        {
            return "RobotStateComponent";
        }
        void setRobotStateObserver(RobotStateObserverPtr observer);
    protected:
        /**
         * Load and create a VirtualRobot::Robot instance from the RobotFileName
         * property. Additionally listen on the KinematicUnit topic for the
         * RobotNodeSet specified in the RobotNodeSetName property.
         */
        virtual void onInitComponent();
        /**
         * Setup RobotStateObjectFactories needed for creating RemoteRobot instances.
         */
        virtual void onConnectComponent();
        virtual void onDisconnectComponent();
        /**
         * Calls unref() on RobotStateComponent::_synchronizedPrx.
         */
        virtual ~RobotStateComponent();

        // inherited from KinematicUnitInterface
        void reportControlModeChanged(const NameControlModeMap& jointModes, bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointAngles(const NameValueMap& jointAngles,  bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointVelocities(const NameValueMap& jointVelocities,  bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointTorques(const NameValueMap& jointTorques,  bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointAccelerations(const NameValueMap& jointAccelerations, bool aValueChanged, const Ice::Current& c);
        void reportJointCurrents(const NameValueMap& jointCurrents,  bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointMotorTemperatures(const NameValueMap& jointMotorTemperatures,  bool aValueChanged, const Ice::Current& c = ::Ice::Current());
        void reportJointStatuses(const NameStatusMap& jointStatuses,  bool aValueChanged, const Ice::Current& c = ::Ice::Current());
    private:
        SharedRobotInterfacePrx _synchronizedPrx;
        SharedRobotServantPtr _sharedRobotServant;
        VirtualRobot::RobotPtr _synchronized;
        std::string robotFile;
        std::string relativeRobotFile;
        RobotStateObserverPtr robotStateObs;


    };

}

#endif
