#include "SharedRobotServants.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <boost/foreach.hpp>

#include <Eigen/Geometry>

#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/Nodes/RobotNodeRevolute.h>
#include <VirtualRobot/Nodes/RobotNodePrismatic.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <Ice/ObjectAdapter.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>


using namespace std;
using namespace VirtualRobot;
using namespace Eigen;
using namespace Ice;



#undef VERBOSE
//#define VERBOSE


namespace armarx
{

    typedef std::map<std::string, SharedRobotNodeInterfacePrx> NodeCache;

    ///////////////////////////////
    // SharedObjectBase
    ///////////////////////////////

    SharedObjectBase::SharedObjectBase()
    {
        this-> _referenceCount = 0;
#ifdef VERBOSE
        ARMARX_LOG_S << "construct " << this << flush;
#endif
    }


    void SharedObjectBase::ref(const Current& current)
    {
        boost::lock_guard<boost::mutex> lock(this->_counterMutex);

        _referenceCount++;

#ifdef VERBOSE
        ARMARX_LOG_S << "ref: " <<  _referenceCount << " " << this << flush;
#endif
    }

    void SharedObjectBase::unref(const Current& current)
    {
        boost::lock_guard<boost::mutex> lock(this->_counterMutex);

#ifdef VERBOSE
        ARMARX_LOG_S << "unref: " <<   _referenceCount << " " << this << flush;
#endif
        _referenceCount --;

        if (_referenceCount == 0)
        {
            this->destroy(current);
        }
    }

    void SharedObjectBase::destroy(const Current& current)
    {
        try
        {
            current.adapter->remove(current.id);
#ifdef VERBOSE
            ARMARX_ERROR_S << "[SharedObject] destroy " << " " << this << flush;
#endif
        }
        catch (const NotRegisteredException& e)
        {
            //        ARMARX_INFO_S << "destroy failed with: " << e.what();
            throw ObjectNotExistException(__FILE__, __LINE__);
        };
    }

    ///////////////////////////////
    // SharedRobotNodeServant
    ///////////////////////////////

    SharedRobotNodeServant::SharedRobotNodeServant(RobotNodePtr node) :
        _node(node)
    {
#ifdef VERBOSE
        ARMARX_LOG_S << "[SharedRobotNodeServant] construct " << " " << this << flush;
#endif
    }


    SharedRobotNodeServant::~SharedRobotNodeServant()
    {
#ifdef VERBOSE
        ARMARX_FATAL_S << "[SharedRobotNodeServant] destruct " << " " << this << flush;
#endif
    }

    float SharedRobotNodeServant::getJointValue(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        return _node->getJointValue();
    }

    string SharedRobotNodeServant::getName(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        return _node->getName();
    }

    PoseBasePtr SharedRobotNodeServant::getLocalTransformation(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        return new Pose(_node->getLocalTransformation());
    }

    FramedPoseBasePtr SharedRobotNodeServant::getGlobalPose(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        return new FramedPose(_node->getGlobalPose(),
                              GlobalFrame,
                              "");
    }

    FramedPoseBasePtr SharedRobotNodeServant::getPoseInRootFrame(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        return new FramedPose(_node->getPoseInRootFrame(),
                              _node->getRobot()->getRootNode()->getName(),
                              _node->getRobot()->getName());
    }


    JointType SharedRobotNodeServant::getType(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();

        if (_node->isRotationalJoint())
        {
            return eRevolute;
        }
        else if (_node->isTranslationalJoint())
        {
            return ePrismatic;
        }
        else
        {
            return eFixed;
        }
    }

    Vector3BasePtr SharedRobotNodeServant::getJointTranslationDirection(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        boost::shared_ptr<RobotNodePrismatic> prismatic = dynamic_pointer_cast<RobotNodePrismatic>(_node);

        if (prismatic)
        {
            return new Vector3(prismatic->getJointTranslationDirection());
        }
        else
        {
            return new Vector3;
        }
    }

    Vector3BasePtr SharedRobotNodeServant::getJointRotationAxis(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        boost::shared_ptr<RobotNodeRevolute> revolute = dynamic_pointer_cast<RobotNodeRevolute>(_node);

        if (revolute)
        {
            return new Vector3(revolute->getJointRotationAxis());
        }
        else
        {
            return new Vector3;
        }
    }


    bool SharedRobotNodeServant::hasChild(const string& name, bool recursive, const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        //return _node->hasChild(name,recursive);
        return false;
    }

    string SharedRobotNodeServant::getParent(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        SceneObjectPtr parent = _node->getParent();

        if (!parent)
        {
            throw UserException("This RobotNode has no parent.");
        }

        return parent->getName();
    }

    NameList SharedRobotNodeServant::getChildren(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        vector<SceneObjectPtr> children = _node->getChildren();
        NameList names;
        BOOST_FOREACH(SceneObjectPtr node, children)
        {
            names.push_back(node->getName());
        }
        return names;
    }

    NameList SharedRobotNodeServant::getAllParents(const std::string& name, const Ice::Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        vector<RobotNodePtr> parents = _node->getAllParents(_node->getRobot()->getRobotNodeSet(name));
        NameList names;
        BOOST_FOREACH(RobotNodePtr node, parents)
        {
            names.push_back(node->getName());
        }
        return names;
    }

    float SharedRobotNodeServant::getJointValueOffest(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        return _node->getJointValueOffset();
    }

    float SharedRobotNodeServant::getJointLimitHigh(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        return _node->getJointLimitHigh();
    }

    float SharedRobotNodeServant::getJointLimitLow(const Current& current) const
    {
        ReadLockPtr lock = _node->getRobot()->getReadLock();
        return _node->getJointLimitLow();
    }


    ///////////////////////////////
    // SharedRobotServant
    ///////////////////////////////

    SharedRobotServant::SharedRobotServant(RobotPtr robot)
        : _robot(robot)
    {
#ifdef VERBOSE
        ARMARX_WARNING_S << "construct " << this << flush;
#endif
    }

    SharedRobotServant::~SharedRobotServant()
    {
#ifdef VERBOSE
        ARMARX_WARNING_S << "destruct " << this << flush;
#endif
        boost::recursive_mutex::scoped_lock cloneLock(m);

        for (auto value : this->_cachedNodes)
        {
            try
            {
                value.second->unref();
            }
            catch (...) {}
        }
    }

    SharedRobotNodeInterfacePrx SharedRobotServant::getRobotNode(const string& name, const Current& current)
    {
        //    ARMARX_LOG_S << "Looking for node: " << name << flush;
        assert(_robot);
        boost::recursive_mutex::scoped_lock cloneLock(m);
        SharedRobotNodeInterfacePrx prx;

        if (this->_cachedNodes.find(name) == this->_cachedNodes.end())
        {
            RobotNodePtr robotNode = _robot->getRobotNode(name);

            if (!robotNode)
            {
                ARMARX_WARNING_S << "RobotNode \"" + name + "\" not defined.";
                throw UserException("RobotNode \"" + name + "\" not defined.");
            }

            SharedRobotNodeInterfacePtr servant = new SharedRobotNodeServant(
                _robot->getRobotNode(name));
            //servant->ref();
            prx = SharedRobotNodeInterfacePrx::uncheckedCast(current.adapter->addWithUUID(servant));
            prx->ref();
            //        return prx;
            this->_cachedNodes[name] = prx;
        }

        return this->_cachedNodes[name];
    }

    SharedRobotNodeInterfacePrx SharedRobotServant::getRootNode(const Current& current)
    {
        assert(_robot);
        boost::recursive_mutex::scoped_lock cloneLock(m);
        string name = _robot->getRootNode()/*,current*/->getName();
        return this->getRobotNode(name, current);
    }


    bool SharedRobotServant::hasRobotNode(const string& name, const Current& current)
    {
        return _robot->hasRobotNode(name);
    }

    NameList SharedRobotServant::getRobotNodes(const Current& current)
    {
        vector<RobotNodePtr> robotNodes = _robot->getRobotNodes();
        NameList names;
        BOOST_FOREACH(RobotNodePtr node, robotNodes)
        {
            names.push_back(node->getName());
        }
        return names;
    }

    RobotNodeSetInfoPtr SharedRobotServant::getRobotNodeSet(const string& name, const Current& current)
    {
        RobotNodeSetPtr robotNodeSet = _robot->getRobotNodeSet(name);

        if (!robotNodeSet)
        {
            throw UserException("RobotNodeSet \"" + name + "\" not defined");
        }

        vector<RobotNodePtr> robotNodes = robotNodeSet->getAllRobotNodes();
        NameList names;
        BOOST_FOREACH(RobotNodePtr node, robotNodes)
        {
            names.push_back(node->getName());
        }

        RobotNodeSetInfoPtr info = new RobotNodeSetInfo;
        info->names = names;
        info->name = name;
        info->tcpName = robotNodeSet->getTCP()->getName();
        info->rootName = robotNodeSet->getKinematicRoot()->getName();

        return info;

    }

    NameList SharedRobotServant::getRobotNodeSets(const Current& current)
    {
        vector<RobotNodeSetPtr> robotNodeSets = _robot->getRobotNodeSets();
        NameList names;
        BOOST_FOREACH(RobotNodeSetPtr set, robotNodeSets)
        {
            names.push_back(set->getName());
        }

        return names;
    }

    bool SharedRobotServant::hasRobotNodeSet(const string& name, const Current& current)
    {
        return _robot->hasRobotNodeSet(name);
    }

    string SharedRobotServant::getName(const Current& current)
    {
        //ARMARX_VERBOSE_S << "SharedRobotServant::getname:" << _robot->getName();
        return _robot->getName();
    }

    string SharedRobotServant::getType(const Current& current)
    {
        return _robot->getType();
    }

    void SharedRobotServant::setTimestamp(const IceUtil::Time& updateTime)
    {
        updateTimestamp = updateTime;
    }

    PoseBasePtr SharedRobotServant::getGlobalPose(const Current& current)
    {
        ReadLockPtr lock = _robot->getReadLock();
        return new Pose(_robot->getGlobalPose());
    }

    NameValueMap SharedRobotServant::getConfig(const Current& current)
    {
        if (!_robot)
        {
            ARMARX_WARNING_S << "no robot set";
            return NameValueMap();
        }

        ReadLockPtr lock = _robot->getReadLock();
        std::map < std::string, float > values = _robot->getConfig()->getRobotNodeJointValueMap();
        NameValueMap result(values.begin(), values.end());
        return result;
    }

    TimestampBasePtr SharedRobotServant::getTimestamp(const Current&) const
    {
        return new TimestampVariant(updateTimestamp);
    }

    void SharedRobotServant::setGlobalPose(const armarx::PoseBasePtr& pose, const Current&)
    {
        WriteLockPtr lock = _robot->getWriteLock();
        _robot->setGlobalPose(PosePtr::dynamicCast(pose)->toEigen(), true);
    }

    RobotPtr SharedRobotServant::getRobot() const
    {
        return this->_robot;
    }

}



