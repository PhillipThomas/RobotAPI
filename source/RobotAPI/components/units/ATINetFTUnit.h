/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ATINetFTUnit
 * @author     Jonas Rauber ( kit at jonasrauber dot de )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ROBOTAPI_UNITS_ATINetFTUnit_H
#define _ROBOTAPI_UNITS_ATINetFTUnit_H


#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/units/ATINetFTUnit.h>
#include <RobotAPI/interface/units/UnitInterface.h>
#include <netinet/in.h>
#include <fstream>

namespace armarx
{
    /**
     * @class ATINetFTUnitPropertyDefinitions
     * @brief
     */
    class ATINetFTUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        ATINetFTUnitPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>(
                "ATINetFTHostName",
                "i61ATINetFT1:801",
                "ATINetFT host name and port, usually i61ATINetFT1:801 or i61ATINetFT2:801");
            defineOptionalProperty<std::string>(
                "DatabasePathName",
                "",
                "Path name to motion database on receiving ATINetFT host");
            defineOptionalProperty<std::string>(
                "CaptureName",
                "Trial01",
                "Custom name of first trial in motion recording session");
            defineOptionalProperty<std::string>(
                "ATINetFTReceiveHostName",
                "i61ATINetFT1:801",
                "ATINetFT host name and port, usually i61ATINetFT1:801 or i61ATINetFT2:801");
            defineOptionalProperty<float>(
                "xOffset",
                0.0f,
                "Offset along x axis from VirtualRobot origin to ATINetFT reference marker");
            defineOptionalProperty<float>(
                "yOffset",
                0.0f,
                "Offset along y axis from VirtualRobot origin to ATINetFT reference marker");
            defineOptionalProperty<float>(
                "zOffset",
                0.0f,
                "Offset along z axis from VirtualRobot origin to ATINetFT reference marker");
        }
    };

    /**
     * @class ATINetFTUnit
     * @ingroup VisionX-Components
     * @brief ArmarX wrapper of the ATINetFT DataStream SDK
     *
     * Documentation mainly uses the term subject instead of object, as that's the
     * common term in the context of ATINetFT.
     */
    class ATINetFTUnit :
        virtual public armarx::Component,
        virtual public ATINetFTUnitInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "ATINetFTUnit";
        }

        virtual void startRecording(const std::string& customName, const Ice::Current& c);


        virtual void stopRecording(const Ice::Current& c);

        bool isComponentOnline(const Ice::Current& c){return remoteTriggerEnabled;};

        void request(const Ice::Current& c){};


        void release(const Ice::Current& c) { }

        void init(const Ice::Current& c) { }

        void start(const Ice::Current& c) { }

        void stop(const Ice::Current& c) { }

        armarx::UnitExecutionState getExecutionState(const Ice::Current& c) { return armarx::eUndefinedUnitExecutionState; }
    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();


    private:
        /**
         * The ATINetFT DataStream SDK Client
         */


	void periodicExec();
        void establishATINetFTReceiveHostConnection(std::string receiverHostName);
        bool sendPacket(char *bytePacket, int bpSize);
        bool receivePacket(char *bytePacket);
		void convertToFTValues(char *receiveBuf, float *ftdata, int ftdataSize);
		void writeFTValuesToFile(float *ftdata, int ftdataSize);

		std::ofstream recordingFile;

        bool remoteTriggerEnabled;
        bool recordingStarted;
        bool recordingStopped;
        bool recordingCompleted;

        int packetID;
        int ftDataSize;
        int sendPacketSize;
        int receivePacketSize;
        int captureNumber;
        int senderSocket;
        int nPaddingZeros;
        int sampleIndex;

        std::string captureNamePrefix;
        std::string databasePathName;
        struct sockaddr_in receiveHostAddr;
        struct sockaddr_in senderHostAddr;

    };
}

#endif
