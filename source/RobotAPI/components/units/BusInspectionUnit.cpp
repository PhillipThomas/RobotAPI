/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core
 * @author     Matthias Hadlich (matthias dot hadlich at student dot kit dot edu)
 * @copyright  2014 Matthias Hadlich
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef BUSINSPECTIONUNIT_CPP
#define BUSINSPECTIONUNIT_CPP

#include "BusInspectionUnit.h"
#include <vector>
#include <string>

using namespace std;
using namespace armarx;


void BusInspectionUnit::onInitComponent()
{
    this->onInitBusInspectionUnit();
    offeringTopic("HardwareValues");
}

void BusInspectionUnit::onConnectComponent()
{

    ARMARX_INFO << flush;

    // TODO: muss das hier gleich sein wie der Channel der Kinematicunit?!?!
    //std::string busInspectionChannel = "busInspection";

    busInspectionValuePrx = getTopic<BusInspectionInterfacePrx>("HardwareValues");
    ARMARX_INFO << "subscribing topic: " << "HardwareValues" << flush;



    this->onConnectBusInspectionUnit();
}

void BusInspectionUnit::onExitComponent()
{
    this->onExitBusInspectionUnit();
}


BusNames BusInspectionUnit::getConfiguredBusses(const Ice::Current& c)
{
    return *(new vector<string>());
}
BusInformation BusInspectionUnit::getBusInformation(const std::string& bus, const Ice::Current& c)
{
    return *(new BusInformation);

}
DeviceNames BusInspectionUnit::getDevicesOnBus(const std::string& bus, const Ice::Current& c)
{
    return *(new vector<string>());
}

DeviceInformation BusInspectionUnit::getDeviceInformation(const std::string& device,  const Ice::Current& c)
{
    return *(new DeviceInformation);
}

Ice::Int BusInspectionUnit::performDeviceOperation(BasicOperation operation, Ice::Int device,  const Ice::Current& c)
{
    return 0;
}

Ice::Int BusInspectionUnit::performBusOperation(BasicOperation operation, const std::string& bus,  const Ice::Current& c)
{
    return 0;
}

bool BusInspectionUnit::isInRealTimeMode(const Ice::Current& c)
{
    return false;
}


std::string BusInspectionUnit::sendCommand(const std::string& command,  Ice::Int device, const Ice::Current& c)
{
    return "";
}


PropertyDefinitionsPtr BusInspectionUnit::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new BusInspectionUnitPropertyDefinitions(
                                      getConfigIdentifier()));
}



#endif
