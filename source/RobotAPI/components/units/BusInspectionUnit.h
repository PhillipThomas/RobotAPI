/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core
 * @author     Matthias Hadlich (matthias dot hadlich at student dot kit dot edu)
 * @copyright  2014 Matthias Hadlich
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _BUSINSPECTIONUNIT_H
#define _BUSINSPECTIONUNIT_H

#include <string>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/interface/hardware/BusInspectionInterface.h>

namespace armarx
{
    /**
     * \class BusInspectionUnitPropertyDefinitions
     * \brief
     */
    class BusInspectionUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        BusInspectionUnitPropertyDefinitions(std::string prefix) :
            ComponentPropertyDefinitions(prefix)
        {
        }
    };

    /**
     * \class BusInspectionUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Provides information about connected hardware devices.
     *
     * BusInspectionUnit is the base class of a unit that allows the introspection of a connected bus system.
     */
    class BusInspectionUnit :
        virtual public BusInspectionInterface, virtual public Component
    {
    public:
        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "BusInspectionUnit";
        }
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onExitComponent();

        virtual void onInitBusInspectionUnit() = 0;
        virtual void onConnectBusInspectionUnit() = 0;
        virtual void onExitBusInspectionUnit() = 0;

        virtual BusNames getConfiguredBusses(const Ice::Current& c = ::Ice::Current());
        virtual BusInformation getBusInformation(const std::string& bus, const Ice::Current& c = ::Ice::Current());
        virtual DeviceNames getDevicesOnBus(const std::string& bus, const Ice::Current& c = ::Ice::Current());
        virtual DeviceInformation getDeviceInformation(const std::string& device,  const Ice::Current& c = ::Ice::Current());

        virtual Ice::Int performDeviceOperation(BasicOperation operation, Ice::Int device,  const Ice::Current& c = ::Ice::Current());
        virtual Ice::Int performBusOperation(BasicOperation operation, const std::string& bus,  const Ice::Current& c = ::Ice::Current());
        virtual bool isInRealTimeMode(const Ice::Current& c = ::Ice::Current());

        virtual std::string sendCommand(const std::string& command,  Ice::Int device, const Ice::Current& c = ::Ice::Current());

        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    protected:
        std::string channel;
        BusInspectionInterfacePrx busInspectionValuePrx; // gets commands from hardware

    };
}




#endif
