/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ForceTorqueObserver.h"

#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>
#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckInRange.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/checks/ConditionCheckEqualsWithTolerance.h>
#include <RobotAPI/libraries/core/checks/ConditionCheckMagnitudeChecks.h>
#include <RobotAPI/libraries/core/observerfilters/OffsetFilter.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>

#define RAWFORCE "_rawforcevectors"
#define OFFSETFORCE "_forceswithoffsetvectors"
#define FILTEREDFORCE "_filteredforcesvectors"
#define RAWTORQUE "_rawtorquevectors"
#define OFFSETTORQUE "_torqueswithoffsetvectors"
#define FORCETORQUEVECTORS "_forceTorqueVectors"
#define POD_SUFFIX "_pod"


using namespace armarx;

ForceTorqueObserver::ForceTorqueObserver()
{
}

void ForceTorqueObserver::setTopicName(std::string topicName)
{
    this->topicName = topicName;
}

void ForceTorqueObserver::onInitObserver()
{
    if (topicName.empty())
    {
        usingTopic(getProperty<std::string>("ForceTorqueTopicName").getValue());
    }
    else
    {
        usingTopic(topicName);
    }

    // register all checks
    offerConditionCheck("updated", new ConditionCheckUpdated());
    offerConditionCheck("larger", new ConditionCheckLarger());
    offerConditionCheck("equals", new ConditionCheckEquals());
    offerConditionCheck("smaller", new ConditionCheckSmaller());
    offerConditionCheck("magnitudeinrange", new ConditionCheckMagnitudeInRange());
    offerConditionCheck("approx", new ConditionCheckEqualsWithTolerance());
    offerConditionCheck("magnitudelarger", new ConditionCheckMagnitudeLarger());
    offerConditionCheck("magnitudesmaller", new ConditionCheckMagnitudeSmaller());

}

void ForceTorqueObserver::onConnectObserver()
{


}


PropertyDefinitionsPtr ForceTorqueObserver::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ForceTorqueObserverPropertyDefinitions(
                                      getConfigIdentifier()));
}


void ForceTorqueObserver::offerValue(std::string nodeName, const std::string& type, FramedDirectionBasePtr value, const DataFieldIdentifierPtr& id)
{
    FramedDirectionPtr vec = FramedDirectionPtr::dynamicCast(value);

    if (!existsChannel(id->channelName))
    {
        offerChannel(id->channelName, type + " vectors on specific parts of the robot.");
    }

    if (!existsDataField(id->channelName, id->datafieldName))
    {
        offerDataFieldWithDefault(id->channelName, id->datafieldName, Variant(value), "3D vector for " + type + " of " + nodeName);
    }
    else
    {
        setDataField(id->channelName, id->datafieldName, Variant(value));
    }


    // pod = plain old data
    std::string pod_channelName = id->channelName + POD_SUFFIX;

    if (!existsChannel(pod_channelName))
    {
        offerChannel(pod_channelName, id->datafieldName + " on " + nodeName + " as plain old data (pod)");
    }

    offerOrUpdateDataField(pod_channelName, id->datafieldName + "_x", Variant(vec->x), type + " on X axis");
    offerOrUpdateDataField(pod_channelName, id->datafieldName + "_y", Variant(vec->y), type + " on Y axis");
    offerOrUpdateDataField(pod_channelName, id->datafieldName + "_z", Variant(vec->z), type + " on Z axis");
    offerOrUpdateDataField(pod_channelName, id->datafieldName + "_frame", Variant(vec->frame), "Frame of " + value->frame);
}

void armarx::ForceTorqueObserver::reportSensorValues(const std::string& sensorNodeName, const FramedDirectionBasePtr& forces, const FramedDirectionBasePtr& torques, const Ice::Current&)
{
    ScopedLock lock(dataMutex);

    try
    {
        DataFieldIdentifierPtr id;

        if (forces)
        {
            id = getForceDatafieldId(sensorNodeName, forces->frame);
            std::string type = "forces";
            offerValue(sensorNodeName, type, forces, id);
        }

        if (torques)
        {
            id = getTorqueDatafieldId(sensorNodeName, torques->frame);
            std::string type = "torques";
            offerValue(sensorNodeName, type, torques, id);
        }

        updateChannel(id->channelName);
        updateChannel(id->channelName + POD_SUFFIX);
    }
    catch (std::exception& e)
    {
        ARMARX_ERROR << "Reporting force torque for " << sensorNodeName << " failed! ";
        handleExceptions();
    }
}


DatafieldRefBasePtr armarx::ForceTorqueObserver::createNulledDatafield(const DatafieldRefBasePtr& forceTorqueDatafieldRef, const Ice::Current&)
{
    return createFilteredDatafield(new filters::OffsetFilter(), forceTorqueDatafieldRef);
}

DatafieldRefBasePtr ForceTorqueObserver::getForceDatafield(const std::string& nodeName, const Ice::Current&)
{
    auto id = getForceDatafieldId(nodeName, nodeName);

    if (!existsChannel(id->channelName))
    {
        throw UserException("No sensor for node '" + nodeName + "'available: channel " + id->channelName);
    }

    if (!existsDataField(id->channelName, id->datafieldName))
    {
        throw UserException("No sensor for node '" + nodeName + "'available: datafield " + id->datafieldName);
    }

    return new DatafieldRef(this, id->channelName, id->datafieldName);

}

DatafieldRefBasePtr ForceTorqueObserver::getTorqueDatafield(const std::string& nodeName, const Ice::Current&)
{
    auto id = getTorqueDatafieldId(nodeName, nodeName);

    if (!existsChannel(id->channelName))
    {
        throw UserException("No sensor for node '" + nodeName + "'available: channel " + id->channelName);
    }

    if (!existsDataField(id->channelName, id->datafieldName))
    {
        throw UserException("No sensor for node '" + nodeName + "'available: datafield " + id->datafieldName);
    }

    return new DatafieldRef(this, id->channelName, id->datafieldName);
}

DataFieldIdentifierPtr ForceTorqueObserver::getForceDatafieldId(const std::string& nodeName, const std::string& frame)
{
    std::string channelName;

    if (frame == nodeName)
    {
        channelName = nodeName;
    }
    else
    {
        channelName = nodeName + "_" + frame;
    }

    std::string datafieldName = "forces";
    DataFieldIdentifierPtr id = new DataFieldIdentifier(getName(), channelName, datafieldName);
    return id;
}

DataFieldIdentifierPtr ForceTorqueObserver::getTorqueDatafieldId(const std::string& nodeName, const std::string& frame)
{
    std::string channelName;

    if (frame == nodeName)
    {
        channelName = nodeName;
    }
    else
    {
        channelName = nodeName + "_" + frame;
    }

    std::string datafieldName = "torques";
    DataFieldIdentifierPtr id = new DataFieldIdentifier(getName(), channelName, datafieldName);
    return id;
}
