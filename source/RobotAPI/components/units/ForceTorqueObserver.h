/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_ROBOTAPI_FORCETORQUEOBSERVER_H
#define _ARMARX_ROBOTAPI_FORCETORQUEOBSERVER_H

#include <RobotAPI/interface/units/ForceTorqueUnit.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXCore/observers/Observer.h>

namespace armarx
{
    /**
     * \class ForceTorqueObserverPropertyDefinitions
     * \brief
     */
    class ForceTorqueObserverPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        ForceTorqueObserverPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("ForceTorqueTopicName", "Name of the ForceTorqueUnit Topic");
        }
    };

    /**
     * \class ForceTorqueObserver
     * \ingroup RobotAPI-SensorActorUnits-observers
     * \brief Observer monitoring Force/Torque values
     *
     * The ForceTorqueObserver monitors F/T values published by ForceTorqueUnit-implementations and offers condition checks on these values.
     * Available condition checks are: *updated*, *larger*, *equals*, *smaller* and *magnitudelarger*.
     */
    class ForceTorqueObserver :
        virtual public Observer,
        virtual public ForceTorqueUnitObserverInterface
    {
    public:
        ForceTorqueObserver();

        void setTopicName(std::string topicName);

        // framework hooks
        virtual std::string getDefaultName() const
        {
            return "ForceTorqueUnitObserver";
        }
        void onInitObserver();
        void onConnectObserver();

        virtual void reportSensorValues(const std::string& sensorNodeName, const FramedDirectionBasePtr& forces, const FramedDirectionBasePtr& torques, const Ice::Current&);

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        // DOES NOT COMPILE:
        // sorry, unimplemented: non-static data member initializers
        // error: in-class initialization of static data member ‘FORCES_STRING’ of non-literal type
        //const std::string FORCES_STRING = "forces";
        //const std::string TORQUES_STRING = "torques";
    private:
        armarx::Mutex dataMutex;
        std::string topicName;


        void offerValue(std::string nodeName, const std::string& type, FramedDirectionBasePtr value, const DataFieldIdentifierPtr& id);

        // ForceTorqueUnitObserverInterface interface
    public:
        DatafieldRefBasePtr createNulledDatafield(const DatafieldRefBasePtr& forceTorqueDatafieldRef, const Ice::Current&);

        DatafieldRefBasePtr getForceDatafield(const std::string& nodeName, const Ice::Current&);
        DatafieldRefBasePtr getTorqueDatafield(const std::string& nodeName, const Ice::Current&);

        DataFieldIdentifierPtr getForceDatafieldId(const std::string& nodeName, const std::string& frame);
        DataFieldIdentifierPtr getTorqueDatafieldId(const std::string& nodeName, const std::string& frame);
    };
}

#endif
