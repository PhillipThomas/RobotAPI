/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Peter Kaiser <peter dot kaiser at kit dot edu>
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ForceTorqueUnit.h"

using namespace armarx;

void ForceTorqueUnit::onInitComponent()
{
    listenerName = getProperty<std::string>("ForceTorqueTopicName").getValue();
    offeringTopic(listenerName);
    onInitForceTorqueUnit();
}

void ForceTorqueUnit::onConnectComponent()
{
    ARMARX_INFO << "setting report topic to " << listenerName << flush;
    listenerPrx = getTopic<ForceTorqueUnitListenerPrx>(listenerName);
    onStartForceTorqueUnit();
}

void ForceTorqueUnit::onExitComponent()
{
    onExitForceTorqueUnit();
}

PropertyDefinitionsPtr ForceTorqueUnit::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ForceTorqueUnitPropertyDefinitions(getConfigIdentifier()));
}
