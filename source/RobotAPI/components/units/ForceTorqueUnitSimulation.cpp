/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ForceTorqueUnitSimulation.h"

#include <boost/algorithm/string.hpp>

using namespace armarx;

void ForceTorqueUnitSimulation::onInitForceTorqueUnit()
{
    int intervalMs = getProperty<int>("IntervalMs").getValue();
    std::string sensorName = getProperty<std::string>("SensorName").getValue();

    //std::vector<std::string> sensorNamesList;
    //boost::split(sensorNamesList, sensorNames, boost::is_any_of(","));

    //for(std::vector<std::string>::iterator s = sensorNamesList.begin(); s != sensorNamesList.end(); s++)
    //{
    //    forces[*s] = new armarx::FramedDirection(Eigen::Vector3f(0, 0, 0), *s);
    //    torques[*s] = new armarx::FramedDirection(Eigen::Vector3f(0, 0, 0), *s);
    // }
    forces = new armarx::FramedDirection(Eigen::Vector3f(0, 0, 0), sensorName, getProperty<std::string>("AgentName").getValue());
    torques = new armarx::FramedDirection(Eigen::Vector3f(0, 0, 0), sensorName, getProperty<std::string>("AgentName").getValue());

    ARMARX_VERBOSE << "Starting ForceTorqueUnitSimulation with " << intervalMs << " ms interval";
    simulationTask = new PeriodicTask<ForceTorqueUnitSimulation>(this, &ForceTorqueUnitSimulation::simulationFunction, intervalMs, false, "ForceTorqueUnitSimulation");
}

void ForceTorqueUnitSimulation::onStartForceTorqueUnit()
{
    simulationTask->start();
}


void ForceTorqueUnitSimulation::onExitForceTorqueUnit()
{
    simulationTask->stop();
}


void ForceTorqueUnitSimulation::simulationFunction()
{
    listenerPrx->reportSensorValues(getProperty<std::string>("SensorName").getValue(), forces, torques);

    //listenerPrx->reportSensorValues(forces);
    //listenerPrx->reportSensorValues(torques);
}

void ForceTorqueUnitSimulation::setOffset(const FramedDirectionBasePtr& forceOffsets, const FramedDirectionBasePtr& torqueOffsets, const Ice::Current& c)
{
    // Ignore
}

void ForceTorqueUnitSimulation::setToNull(const Ice::Current& c)
{
    // Ignore
}


PropertyDefinitionsPtr ForceTorqueUnitSimulation::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ForceTorqueUnitSimulationPropertyDefinitions(getConfigIdentifier()));
}

