/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "HandUnit.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/EndEffector/EndEffector.h>
#include <VirtualRobot/XML/RobotIO.h>

#include <vector>

using namespace armarx;
using namespace VirtualRobot;


void HandUnit::onInitComponent()
{
    std::string endeffectorFile = getProperty<std::string>("RobotFileName").getValue();
    std::string endeffectorName = getProperty<std::string>("EndeffectorName").getValue();

    if (!ArmarXDataPath::getAbsolutePath(endeffectorFile, endeffectorFile))
    {
        throw UserException("Could not find robot file " + endeffectorFile);
    }


    try
    {
        robot = VirtualRobot::RobotIO::loadRobot(endeffectorFile, VirtualRobot::RobotIO::eStructure);
    }
    catch (VirtualRobot::VirtualRobotException& e)
    {
        throw UserException(e.what());
    }

    if (endeffectorName == "")
    {
        throw UserException("EndeffectorName not defined");
    }

    if (!robot->hasEndEffector(endeffectorName))
    {
        throw UserException("Robot does not contain an endeffector with name: " + endeffectorName);
    }

    eef = robot->getEndEffector(endeffectorName);
    robotName = robot->getName();

    if (eef->getTcp())
    {
        tcpName = robot->getEndEffector(endeffectorName)->getTcp()->getName();
    }
    else
    {
        throw UserException("Endeffector without TCP: " + endeffectorName);
    }

    // get all joints
    std::vector<EndEffectorActorPtr> actors;
    eef->getActors(actors);

    for (size_t i = 0; i < actors.size(); i++)
    {
        EndEffectorActorPtr a = actors[i];
        std::vector<EndEffectorActor::ActorDefinition> ads;// = a->getDefinition();

        for (size_t j = 0; j < ads.size(); j++)
        {
            EndEffectorActor::ActorDefinition ad = ads[j];

            if (ad.directionAndSpeed != 0 && ad.robotNode)
            {
                handJoints[ad.robotNode->getName()] = ad.directionAndSpeed;
            }
        }
    }

    const std::vector<std::string> endeffectorPreshapes = robot->getEndEffector(endeffectorName)->getPreshapes();

    shapeNames = new SingleTypeVariantList(VariantType::String);
    std::vector<std::string>::const_iterator iter = endeffectorPreshapes.begin();

    while (iter != endeffectorPreshapes.end())
    {
        Variant currentPreshape;
        currentPreshape.setString(*iter);
        shapeNames->addVariant(currentPreshape);
        iter++;
    }


    // component dependencies
    listenerChannelName = endeffectorName + "State";
    offeringTopic(listenerChannelName);

    this->onInitHandUnit();
}


void HandUnit::onConnectComponent()
{
    ARMARX_INFO << "setting report topic to " << listenerChannelName << flush;
    listenerPrx = getTopic<HandUnitListenerPrx>(listenerChannelName);

    this->onStartHandUnit();
}


void HandUnit::onExitComponent()
{
    this->onExitHandUnit();
}



void HandUnit::setShape(const std::string& shapeName, const Ice::Current& c)
{
}


SingleTypeVariantListBasePtr HandUnit::getShapeNames(const Ice::Current& c)
{
    return shapeNames;
}


PropertyDefinitionsPtr HandUnit::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new HandUnitPropertyDefinitions(
                                      getConfigIdentifier()));
}


NameValueMap HandUnit::getShapeJointValues(const std::string& shapeName, const Ice::Current&)
{
    EndEffectorPtr efp = robot->getEndEffector(getProperty<std::string>("EndeffectorName").getValue());
    RobotConfigPtr rc = efp->getPreshape(shapeName);
    return rc->getRobotNodeJointValueMap();
}

NameValueMap HandUnit::getCurrentJointValues(const Ice::Current& c)
{
    NameValueMap result;

    for (auto j : handJoints)
    {
        result[j.first] = 0.0f;
    }

    return result;
}

void HandUnit::setObjectGrasped(const std::string& objectName, const Ice::Current&)
{
    ARMARX_INFO << "Object grasped " << objectName << flush;
    graspedObject = objectName;
}

void HandUnit::setObjectReleased(const std::string& objectName, const Ice::Current&)
{
    ARMARX_INFO << "Object released " << objectName << flush;
    graspedObject = "";
}



std::string armarx::HandUnit::getHandName(const Ice::Current&)
{
    return eef->getName();
}


void armarx::HandUnit::setJointAngles(const armarx::NameValueMap& targetJointAngles, const Ice::Current& c)
{

}
