/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     David Schiebener ( schiebener at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_HEADIKUNIT_H
#define _ARMARX_HEADIKUNIT_H


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <RobotAPI/interface/units/HeadIKUnit.h>


namespace armarx
{
    /**
     * \class HeadIKUnitPropertyDefinitions
     * \brief
     */
    class HeadIKUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        HeadIKUnitPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the KinematicUnit Proxy");
            defineOptionalProperty<std::string>("RobotStateTopicName", "RobotState", "Name of the RobotComponent State topic.");
            defineOptionalProperty<int>("CycleTime", 30, "Cycle time of the tcp control in ms");
        }
    };

    /**
     * \defgroup Component-HeadIKUnit HeadIKUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Unit for controlling a robot head via IK targets.
     */

    /**
     * @ingroup Component-HeadIKUnit
     * @brief The HeadIKUnit class
     */
    class HeadIKUnit : virtual public Component, virtual public HeadIKUnitInterface
    {
    public:
        HeadIKUnit();
        void onInitComponent();
        void onConnectComponent();
        void onDisconnectComponent();
        void onExitComponent();
        std::string getDefaultName() const
        {
            return "HeadIKUnit";
        }

        // HeadIKUnitInterface interface
        void setCycleTime(Ice::Int milliseconds, const Ice::Current& c = Ice::Current());
        void setHeadTarget(const std::string& robotNodeSetName, const FramedPositionBasePtr& targetPosition, const Ice::Current& c = Ice::Current());

        // UnitExecutionManagementInterface interface
        void init(const Ice::Current& c = Ice::Current());
        void start(const Ice::Current& c = Ice::Current());
        void stop(const Ice::Current& c = Ice::Current());
        UnitExecutionState getExecutionState(const Ice::Current& c = Ice::Current());

        // UnitResourceManagementInterface interface
        void request(const Ice::Current& c = Ice::Current());
        void release(const Ice::Current& c = Ice::Current());

        // PropertyUser interface
        PropertyDefinitionsPtr createPropertyDefinitions();


    private:
        void periodicExec();

        Mutex accessMutex;
        bool requested;
        int cycleTime;
        PeriodicTask<HeadIKUnit>::pointer_type execTask;

        RobotStateComponentInterfacePrx robotStateComponentPrx;
        KinematicUnitInterfacePrx kinematicUnitPrx;
        //SharedRobotInterfacePrx remoteRobotPrx;
        VirtualRobot::RobotPtr localRobot;
        DebugDrawerInterfacePrx drawer;

        std::string robotNodeSetName;
        FramedPositionPtr targetPosition;
        bool newTargetSet;
    };

}


#endif // _ARMARX_HEADIKUNIT_H
