/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::InertialMeasurementUnit
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotAPI_InertialMeasurementUnit_H
#define _ARMARX_COMPONENT_RobotAPI_InertialMeasurementUnit_H


#include "SensorActorUnit.h"

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/units/InertialMeasurementUnit.h>

namespace armarx
{
    /**
     * \class InertialMeasurementUnitPropertyDefinitions
     * \brief
     */
    class InertialMeasurementUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        InertialMeasurementUnitPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("IMUTopicName", "IMUValues", "Name of the IMU Topic.");
        }
    };

    /**
     * \class InertialMeasurementUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Base unit for %IMU sensors.
     */
    class InertialMeasurementUnit :
        virtual public armarx::InertialMeasurementUnitInterface,
        virtual public armarx::SensorActorUnit
    {
    public:
        /**
         * \see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "InertialMeasurementUnit";
        }


    protected:
        /**
         * \see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * \see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * \see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * \see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();


        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();


        virtual void onInitIMU() = 0;
        virtual void onStartIMU() = 0;
        virtual void onExitIMU() = 0;

        InertialMeasurementUnitListenerPrx IMUTopicPrx;
    };
}

#endif
