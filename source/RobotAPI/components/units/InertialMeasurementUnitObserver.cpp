#include "InertialMeasurementUnitObserver.h"


#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/observers/variant/TimestampVariant.h>



using namespace armarx;


void InertialMeasurementUnitObserver::onInitObserver()
{
    usingTopic(getProperty<std::string>("IMUTopicName").getValue());

    offerConditionCheck("updated", new ConditionCheckUpdated());
    offerConditionCheck("larger", new ConditionCheckLarger());
    offerConditionCheck("equals", new ConditionCheckEquals());
    offerConditionCheck("smaller", new ConditionCheckSmaller());

    offeringTopic(getProperty<std::string>("DebugDrawerTopic").getValue());
}



void InertialMeasurementUnitObserver::onConnectObserver()
{
    debugDrawerPrx = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopic").getValue());
}


void InertialMeasurementUnitObserver::onExitObserver()
{
    debugDrawerPrx->removePoseVisu("IMU", "orientation");
    debugDrawerPrx->removeLineVisu("IMU", "acceleration");
}

void InertialMeasurementUnitObserver::reportSensorValues(const std::string& device, const std::string& name, const IMUData& values, const TimestampBasePtr& timestamp, const Ice::Current& c)
{
    ScopedLock lock(dataMutex);

    TimestampVariantPtr timestampPtr = TimestampVariantPtr::dynamicCast(timestamp);

    Vector3Ptr acceleration = new Vector3(values.acceleration.at(0), values.acceleration.at(1), values.acceleration.at(2));
    Vector3Ptr gyroscopeRotation = new Vector3(values.gyroscopeRotation.at(0), values.gyroscopeRotation.at(1), values.gyroscopeRotation.at(2));
    Vector3Ptr magneticRotation = new Vector3(values.magneticRotation.at(0), values.magneticRotation.at(1), values.magneticRotation.at(2));
    QuaternionPtr orientationQuaternion =  new Quaternion(values.orientationQuaternion.at(0), values.orientationQuaternion.at(1), values.orientationQuaternion.at(2), values.orientationQuaternion.at(3));

    if (!existsChannel(device))
    {
        offerChannel(device, "IMU data");
        \
        //todo remove
        offerDataFieldWithDefault(device, "name", Variant(name), "Name of the IMU sensor");
        offerDataFieldWithDefault(device, "acceleration", acceleration,  "acceleration values");
        offerDataFieldWithDefault(device, "gyroscopeRotation", gyroscopeRotation,  "gyroscope rotation values");
        offerDataFieldWithDefault(device, "magneticRotation", magneticRotation,  "magnetic rotation values");
        offerDataFieldWithDefault(device, "orientationQuaternion", orientationQuaternion,  "orientation quaternion values");
        offerDataFieldWithDefault(device, "timestamp", timestampPtr, "Timestamp");
    }
    else
    {
        setDataField(device, "name", Variant(name));
        setDataField(device, "acceleration", acceleration);
        setDataField(device, "gyroscopeRotation", gyroscopeRotation);
        setDataField(device, "magneticRotation", magneticRotation);
        setDataField(device, "orientationQuaternion", orientationQuaternion);
        setDataField(device, "timestamp", timestampPtr);
    }

    updateChannel(device);

    Eigen::Vector3f zero;
    zero.setZero();

    DrawColor color;
    color.r = 255;
    color.g = 255;

    Eigen::Vector3f ac = acceleration->toEigen();
    ac *= 10;

    debugDrawerPrx->setLineVisu("IMU", "acceleration", new Vector3(), new Vector3(ac), 2.0f, color);

    PosePtr posePtr = new Pose(orientationQuaternion->toEigen(), zero);
    debugDrawerPrx->setPoseVisu("IMU", "orientation", posePtr);
}


PropertyDefinitionsPtr InertialMeasurementUnitObserver::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new InertialMeasurementUnitObserverPropertyDefinitions(getConfigIdentifier()));
}


