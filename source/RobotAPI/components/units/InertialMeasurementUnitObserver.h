/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     David Schiebener <schiebener at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ROBOTAPI_IMU_OBSERVER_H
#define _ARMARX_ROBOTAPI_IMU_OBSERVER_H

#include <RobotAPI/interface/units/InertialMeasurementUnit.h>
#include <ArmarXCore/observers/Observer.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>


namespace armarx
{
    /**
     * \class InertialMeasurementUnitObserverPropertyDefinitions
     * \brief
     */
    class InertialMeasurementUnitObserverPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        InertialMeasurementUnitObserverPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("IMUTopicName", "IMUValues", "Name of the IMU Topic.");
            defineOptionalProperty<std::string>("DebugDrawerTopic", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
        }
    };


    /**
     * \class InertialMeasurementUnitObserver
     * \ingroup RobotAPI-SensorActorUnits-observers
     * \brief Observer monitoring @IMU sensor values
     *
     * The InertialMeasurementUnitObserver monitors @IMU sensor values published by InertialMeasurementUnit-implementations and offers condition checks on these values.
     * Available condition checks are: *updated*, *larger*, *equals* and *smaller*.
     */
    class InertialMeasurementUnitObserver :
        virtual public Observer,
        virtual public InertialMeasurementUnitObserverInterface
    {
    public:
        InertialMeasurementUnitObserver() {}

        virtual std::string getDefaultName() const
        {
            return "InertialMeasurementUnitObserver";
        }
        virtual void onInitObserver();
        virtual void onConnectObserver();
        virtual void onExitObserver();

        void reportSensorValues(const std::string& device, const std::string& name, const IMUData& values, const TimestampBasePtr& timestamp, const Ice::Current& c = ::Ice::Current());

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();


    private:
        Mutex dataMutex;
        DebugDrawerInterfacePrx debugDrawerPrx;


    };
}

#endif
