/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "KinematicUnit.h"

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/VirtualRobotException.h>
#include <VirtualRobot/RuntimeEnvironment.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

using namespace armarx;


void KinematicUnit::onInitComponent()
{
    // read names of kinematic chain elements belonging to this unit from XML and setup a map of all joints
    // the kinematic chain elements belonging to this unit are defined in a RobotNodeSet
    std::string robotFile = getProperty<std::string>("RobotFileName").getValue();
    robotNodeSetName = getProperty<std::string>("RobotNodeSetName").getValue();

    std::string project = getProperty<std::string>("RobotFileNameProject").getValue();
    StringList includePaths;

    if (!project.empty())
    {
        CMakePackageFinder finder(project);
        StringList projectIncludePaths;
        auto pathsString = finder.getDataDir();
        boost::split(projectIncludePaths,
                     pathsString,
                     boost::is_any_of(";,"),
                     boost::token_compress_on);
        includePaths.insert(includePaths.end(), projectIncludePaths.begin(), projectIncludePaths.end());
    }

    if (!ArmarXDataPath::getAbsolutePath(robotFile, robotFile, includePaths))
    {
        throw UserException("Could not find robot file " + robotFile);
    }

    // read the robot node set and initialize the joints of this kinematic unit
    try
    {
        robot = VirtualRobot::RobotIO::loadRobot(robotFile, VirtualRobot::RobotIO::eStructure);
    }
    catch (VirtualRobot::VirtualRobotException& e)
    {
        throw UserException(e.what());
    }

    if (robotNodeSetName == "")
    {
        throw UserException("RobotNodeSet not defined");
    }

    VirtualRobot::RobotNodeSetPtr robotNodeSetPtr = robot->getRobotNodeSet(robotNodeSetName);
    robotNodes = robotNodeSetPtr->getAllRobotNodes();

    // component dependencies
    listenerName = robotNodeSetName + "State";
    offeringTopic(listenerName);

    this->onInitKinematicUnit();
}


void KinematicUnit::onConnectComponent()
{
    ARMARX_INFO << "setting report topic to " << listenerName << flush;
    listenerPrx = getTopic<KinematicUnitListenerPrx>(listenerName);

    this->onStartKinematicUnit();
}


void KinematicUnit::onExitComponent()
{
    this->onExitKinematicUnit();
}


void KinematicUnit::switchControlMode(const NameControlModeMap& targetJointModes, const Ice::Current& c)
{
}

void KinematicUnit::setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current& c)
{
}

void KinematicUnit::setJointVelocities(const NameValueMap& targetJointVelocities, const Ice::Current& c)
{
}

void KinematicUnit::requestKinematicUnit(const StringSequence& nodes, const Ice::Current& c)
{
    SensorActorUnit::request(c);
}

void KinematicUnit::releaseKinematicUnit(const StringSequence& nodes, const Ice::Current& c)
{
    SensorActorUnit::release(c);
}

PropertyDefinitionsPtr KinematicUnit::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new KinematicUnitPropertyDefinitions(
                                      getConfigIdentifier()));
}
