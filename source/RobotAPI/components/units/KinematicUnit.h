/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_KINEMATIC_UNIT_H
#define _ARMARX_COMPONENT_KINEMATIC_UNIT_H

#include <RobotAPI/components/units/SensorActorUnit.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>

#include <VirtualRobot/Nodes/RobotNode.h>
#include <vector>

namespace armarx
{
    /**
     * \class KinematicUnitPropertyDefinitions
     * \brief
     */
    class KinematicUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        KinematicUnitPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("RobotNodeSetName", "Robot node set name as defined in robot xml file, e.g. 'LeftArm'");
            defineRequiredProperty<std::string>("RobotFileName", "Robot file name, e.g. robot_model.xml");
            defineOptionalProperty<std::string>("RobotFileNameProject", "", "Project in which the robot filename is located (if robot is loaded from an external project)");
        }
    };


    /**
     * \defgroup Component-KinematicUnit KinematicUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Base unit for kinematic sensors and actors.
     *
     * KinematicUnits are SensorActorUnits which provide sensory data in terms of joints angles, joint velocities and joint forces.
     * Further target joint angles and velocities can be controlled.
     *
     * The KinematicUnit retrieves its configuration from a VirtualRobot robot model. Within the model, the joints, the unit
     * controls are defined with a RobotNodeSet.
     */

    /**
     * @ingroup Component-KinematicUnit
     * @brief The KinematicUnit class
     */
    class KinematicUnit :
        virtual public KinematicUnitInterface,
        virtual public SensorActorUnit
    {
    public:
        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "KinematicUnit";
        }

        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onExitComponent();

        virtual void onInitKinematicUnit() = 0;
        virtual void onStartKinematicUnit() = 0;
        virtual void onExitKinematicUnit() = 0;

        // proxy implementation
        virtual void requestKinematicUnit(const StringSequence& nodes, const Ice::Current& c = ::Ice::Current());
        virtual void releaseKinematicUnit(const StringSequence& nodes, const Ice::Current& c = ::Ice::Current());
        virtual void switchControlMode(const NameControlModeMap& targetJointModes, const Ice::Current& c = ::Ice::Current());
        virtual void setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current& c = ::Ice::Current());
        virtual void setJointVelocities(const NameValueMap& targetJointVelocities, const Ice::Current& c = ::Ice::Current());


        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    protected:
        KinematicUnitListenerPrx listenerPrx;

        VirtualRobot::RobotPtr robot;
        std::string robotNodeSetName;
        std::string listenerName;
        std::vector< VirtualRobot::RobotNodePtr > robotNodes;
    };
}

#endif
