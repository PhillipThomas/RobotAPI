/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "KinematicUnitSimulation.h"

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/VirtualRobotException.h>
#include <VirtualRobot/RuntimeEnvironment.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <algorithm>

using namespace armarx;

void KinematicUnitSimulation::onInitKinematicUnit()
{

    for (std::vector<VirtualRobot::RobotNodePtr>::iterator it = robotNodes.begin(); it != robotNodes.end(); it++)
    {
        std::string jointName = (*it)->getName();
        jointInfos[jointName].limitLo = (*it)->getJointLimitLo();
        jointInfos[jointName].limitHi = (*it)->getJointLimitHi();

        if ((*it)->isRotationalJoint())
        {
            if (jointInfos[jointName].limitHi - jointInfos[jointName].limitLo >= float(M_PI * 2))
            {
                jointInfos[jointName].reset();
            }
        }

        ARMARX_VERBOSE << jointName << " with limits: lo: " << jointInfos[jointName].limitLo << " hi: " << jointInfos[jointName].limitHi;
    }

    {
        // duplicate the loop because of locking
        boost::mutex::scoped_lock lock(jointStatesMutex);

        // initialize JoinStates
        for (std::vector<VirtualRobot::RobotNodePtr>::iterator it = robotNodes.begin(); it != robotNodes.end(); it++)
        {
            std::string jointName = (*it)->getName();
            jointStates[jointName] = KinematicUnitSimulationJointState();
            jointStates[jointName].init();
        }
    }

    noise = getProperty<float>("Noise").getValue() / 180.f * M_PI;
    distribution = std::normal_distribution<double>(0, noise);
    intervalMs = getProperty<int>("IntervalMs").getValue();
    ARMARX_VERBOSE << "Starting kinematic unit simulation with " << intervalMs << " ms interval";
    simulationTask = new PeriodicTask<KinematicUnitSimulation>(this, &KinematicUnitSimulation::simulationFunction, intervalMs, false, "KinematicUnitSimulation");
}

void KinematicUnitSimulation::onStartKinematicUnit()
{
    lastExecutionTime = IceUtil::Time::now();
    simulationTask->start();
}


void KinematicUnitSimulation::onExitKinematicUnit()
{
    simulationTask->stop();
}


void KinematicUnitSimulation::simulationFunction()
{
    // the time it took until this method was called again
    double timeDeltaInSeconds = (IceUtil::Time::now() - lastExecutionTime).toMilliSecondsDouble() / 1000.0;
    lastExecutionTime = IceUtil::Time::now();
    // name value maps for reporting
    NameValueMap actualJointAngles;
    NameValueMap actualJointVelocities;

    bool aPosValueChanged = false;
    bool aVelValueChanged = false;

    {
        boost::mutex::scoped_lock lock(jointStatesMutex);

        for (KinematicUnitSimulationJointStates::iterator it = jointStates.begin(); it != jointStates.end(); it++)
        {

            float newAngle = 0.0f;

            // calculate joint positions if joint is in velocity control mode
            if (it->second.controlMode == eVelocityControl)
            {
                double change = (it->second.velocity * timeDeltaInSeconds);

                double randomNoiseValue = distribution(generator);
                change += randomNoiseValue;
                newAngle = it->second.angle + change;
            }
            else if (it->second.controlMode == ePositionControl)
            {
                newAngle = it->second.angle;
            }

            // constrain the angle
            KinematicUnitSimulationJointInfo& jointInfo = jointInfos[it->first];
            float maxAngle = (jointInfo.hasLimits()) ? jointInfo.limitHi : 2.0 * M_PI;
            float minAngle = (jointInfo.hasLimits()) ? jointInfo.limitLo : -2.0 * M_PI;
            newAngle = std::max<float>(newAngle, minAngle);
            newAngle = std::min<float>(newAngle, maxAngle);

            // Check if joint existed before
            KinematicUnitSimulationJointStates::iterator itPrev = previousJointStates.find(it->first);

            if (itPrev == previousJointStates.end())
            {
                aPosValueChanged = aVelValueChanged = true;
            }

            // check if the angle has changed
            if (fabs(previousJointStates[it->first].angle - newAngle) > 0.00000001)
            {
                aPosValueChanged = true;
                // set the new joint angle locally for the next simulation iteration
                it->second.angle = newAngle;
            }

            // check if velocities have changed
            if (fabs(previousJointStates[it->first].velocity - it->second.velocity) > 0.00000001)
            {
                aVelValueChanged = true;
            }


            actualJointAngles[it->first] = newAngle;
            actualJointVelocities[it->first] = it->second.velocity;
        }

        previousJointStates = jointStates;
    }

    listenerPrx->reportJointAngles(actualJointAngles, aPosValueChanged);
    listenerPrx->reportJointVelocities(actualJointVelocities, aVelValueChanged);
}

void KinematicUnitSimulation::switchControlMode(const NameControlModeMap& targetJointModes, const Ice::Current& c)
{
    bool aValueChanged = false;
    NameControlModeMap changedControlModes;
    {
        boost::mutex::scoped_lock lock(jointStatesMutex);

        for (NameControlModeMap::const_iterator it = targetJointModes.begin(); it != targetJointModes.end(); it++)
        {
            // target values
            std::string targetJointName = it->first;
            ControlMode targetControlMode = it->second;

            KinematicUnitSimulationJointStates::iterator iterJoints = jointStates.find(targetJointName);

            // check whether there is a joint with this name and set joint angle
            if (iterJoints != jointStates.end())
            {
                if (iterJoints->second.controlMode != targetControlMode)
                {
                    aValueChanged = true;
                }

                iterJoints->second.controlMode = targetControlMode;
                changedControlModes.insert(*it);
            }
            else
            {
                ARMARX_WARNING << "Could not find joint with name " << targetJointName;
            }
        }
    }
    // only report control modes which have been changed
    listenerPrx->reportControlModeChanged(changedControlModes, aValueChanged);
}


void KinematicUnitSimulation::setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current& c)
{

    boost::mutex::scoped_lock lock(jointStatesMutex);
    // name value maps for reporting
    NameValueMap actualJointAngles;
    bool aValueChanged = false;

    for (NameValueMap::const_iterator it = targetJointAngles.begin(); it != targetJointAngles.end(); it++)
    {
        // target values
        std::string targetJointName = it->first;
        float targetJointAngle = it->second;

        // check whether there is a joint with this name and set joint angle
        KinematicUnitSimulationJointStates::iterator iterJoints = jointStates.find(targetJointName);

        if (iterJoints != jointStates.end())
        {
            if (fabs(iterJoints->second.angle - targetJointAngle) > 0)
            {
                aValueChanged = true;
            }

            if (jointInfos[targetJointName].hasLimits())
            {
                targetJointAngle = std::max<float>(targetJointAngle, jointInfos[targetJointName].limitLo);
                targetJointAngle = std::min<float>(targetJointAngle, jointInfos[targetJointName].limitHi);
            }

            iterJoints->second.angle = targetJointAngle;
            actualJointAngles[targetJointName] = iterJoints->second.angle;
        }
        else
        {
            ARMARX_WARNING  << deactivateSpam(1) << "Joint '" << targetJointName << "' not found!";
        }
    }

    if (aValueChanged)
    {
        listenerPrx->reportJointAngles(actualJointAngles, aValueChanged);
    }
}

void KinematicUnitSimulation::setJointVelocities(const NameValueMap& targetJointVelocities, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(jointStatesMutex);
    NameValueMap actualJointVelocities;
    bool aValueChanged = false;

    for (NameValueMap::const_iterator it = targetJointVelocities.begin(); it != targetJointVelocities.end(); it++)
    {
        // target values
        std::string targetJointName = it->first;
        float targetJointVelocity = it->second;

        KinematicUnitSimulationJointStates::iterator iterJoints = jointStates.find(targetJointName);

        // check whether there is a joint with this name and set joint angle
        if (iterJoints != jointStates.end())
        {
            if (fabs(iterJoints->second.velocity - targetJointVelocity) > 0)
            {
                aValueChanged = true;
            }

            iterJoints->second.velocity = targetJointVelocity;
            actualJointVelocities[targetJointName] = iterJoints->second.velocity;
        }
    }

    if (aValueChanged)
    {
        listenerPrx->reportJointVelocities(actualJointVelocities, aValueChanged);
    }
}

void KinematicUnitSimulation::setJointTorques(const NameValueMap& targetJointTorques, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(jointStatesMutex);
    NameValueMap actualJointTorques;
    bool aValueChanged = false;

    for (NameValueMap::const_iterator it = targetJointTorques.begin(); it != targetJointTorques.end(); it++)
    {
        // target values
        std::string targetJointName = it->first;
        float targetJointTorque = it->second;

        KinematicUnitSimulationJointStates::iterator iterJoints = jointStates.find(targetJointName);

        // check whether there is a joint with this name and set joint angle
        if (iterJoints != jointStates.end())
        {
            if (fabs(iterJoints->second.torque - targetJointTorque) > 0)
            {
                aValueChanged = true;
            }

            iterJoints->second.torque = targetJointTorque;
            actualJointTorques[targetJointName] = iterJoints->second.torque;
        }
    }

    if (aValueChanged)
    {
        listenerPrx->reportJointTorques(actualJointTorques, aValueChanged);
    }
}

NameControlModeMap KinematicUnitSimulation::getControlModes(const Ice::Current& c)
{
    NameControlModeMap result;

    for (KinematicUnitSimulationJointStates::iterator it = jointStates.begin(); it != jointStates.end(); it++)
    {
        result[it->first] = it->second.controlMode;
    }

    return result;
}

void KinematicUnitSimulation::setJointAccelerations(const NameValueMap& targetJointAccelerations, const Ice::Current& c)
{
}

void KinematicUnitSimulation::setJointDecelerations(const NameValueMap& targetJointDecelerations, const Ice::Current& c)
{
}

void KinematicUnitSimulation::stop(const Ice::Current& c)
{

    boost::mutex::scoped_lock lock(jointStatesMutex);

    for (KinematicUnitSimulationJointStates::iterator it = jointStates.begin(); it != jointStates.end(); it++)
    {
        it->second.velocity = 0;
    }

    SensorActorUnit::stop(c);
}


PropertyDefinitionsPtr KinematicUnitSimulation::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(
               new KinematicUnitSimulationPropertyDefinitions(getConfigIdentifier()));
}


bool mapEntryEqualsString(std::pair<std::string, KinematicUnitSimulationJointState> iter, std::string compareString)
{
    return (iter.first == compareString);
}

void KinematicUnitSimulation::requestJoints(const StringSequence& joints, const Ice::Current& c)
{
    ARMARX_INFO << flush;
    // if one of the joints belongs to this unit, lock access to this unit for other components except for the requesting one
    KinematicUnitSimulationJointStates::const_iterator it = std::find_first_of(jointStates.begin(), jointStates.end(), joints.begin(), joints.end(), mapEntryEqualsString);

    if (jointStates.end() != it)
    {
        KinematicUnit::request(c);
    }
}

void KinematicUnitSimulation::releaseJoints(const StringSequence& joints, const Ice::Current& c)
{
    ARMARX_INFO << flush;
    // if one of the joints belongs to this unit, unlock access
    KinematicUnitSimulationJointStates::const_iterator it = std::find_first_of(jointStates.begin(), jointStates.end(), joints.begin(), joints.end(), mapEntryEqualsString);

    if (jointStates.end() != it)
    {
        KinematicUnit::release(c);
    }
}
