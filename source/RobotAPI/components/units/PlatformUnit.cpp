/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PlatformUnit.h"


using namespace armarx;


void PlatformUnit::onInitComponent()
{
    std::string platformName = getProperty<std::string>("PlatformName").getValue();

    // component dependencies
    listenerChannelName = platformName + "State";
    offeringTopic(listenerChannelName);

    this->onInitPlatformUnit();
}


void PlatformUnit::onConnectComponent()
{
    ARMARX_INFO << "setting report topic to " << listenerChannelName << flush;
    listenerPrx = getTopic<PlatformUnitListenerPrx>(listenerChannelName);

    this->onStartPlatformUnit();
}


void PlatformUnit::onDisconnectComponent()
{
    listenerPrx = NULL;

    this->onStopPlatformUnit();
}


void PlatformUnit::onExitComponent()
{
    this->onExitPlatformUnit();
}


void PlatformUnit::moveTo(Ice::Float targetPlatformPositionX, Ice::Float targetPlatformPositionY, Ice::Float targetPlatformRotation, Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c)
{
}


PropertyDefinitionsPtr PlatformUnit::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new PlatformUnitPropertyDefinitions(
                                      getConfigIdentifier()));
}
