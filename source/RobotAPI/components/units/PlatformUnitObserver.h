/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_KINEMATIC_UNIT_OBSERVER_H
#define _ARMARX_CORE_KINEMATIC_UNIT_OBSERVER_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/observers/PlatformUnitObserverInterface.h>
#include <ArmarXCore/interface/observers/VariantBase.h>

#include <ArmarXCore/observers/ConditionCheck.h>
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>

#include <string>

namespace armarx
{
    ARMARX_CREATE_CHECK(PlatformUnitObserver, equals)
    ARMARX_CREATE_CHECK(PlatformUnitObserver, larger)
    ARMARX_CREATE_CHECK(PlatformUnitObserver, inrange)

    /**
     * \class PlatformUnitObserver
     * \ingroup RobotAPI-SensorActorUnits-observers
     * \brief Observer monitoring platform-related sensor values.
     *
     * The PlatformUnitObserver allows the installation of conditions on all channel reported by the PlatformUnit.
     * These include current and target position.
     *
     * The PlatformUnitObserver retrieves its platform configuration via properties.
     * The name must exist in the used Simox robot model file.
     *
     * Available condition checks are: *valid*, *updated*, *equals*, *inrange*, *approx*, *larger* and *smaller*.
     */
    class ARMARXCORE_IMPORT_EXPORT PlatformUnitObserver :
        virtual public Observer,
        virtual public PlatformUnitObserverInterface
    {
    public:
        // framework hooks
        void onInitObserver();
        void onConnectObserver();

        // slice interface implementation
        void reportPlatformPose(::Ice::Float currentPlatformPositionX, ::Ice::Float currentPlatformPositionY, ::Ice::Float currentPlatformRotation, const Ice::Current& c = ::Ice::Current());
        void reportNewTargetPose(::Ice::Float newPlatformPositionX, ::Ice::Float newPlatformPositionY, ::Ice::Float newPlatformRotation, const Ice::Current& c = ::Ice::Current());
        void reportPlatformVelocity(::Ice::Float currentPlatformVelocityX, ::Ice::Float currentPlatformVelocityY, ::Ice::Float currentPlatformVelocityRotation, const Ice::Current& c = ::Ice::Current());

        virtual std::string getDefaultName() const
        {
            return "PlatformUnitObserver";
        }

        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    protected:
        void nameValueMapToDataFields(std::string channelName, ::Ice::Float platformPositionX, ::Ice::Float platformPositionY, ::Ice::Float platformRotation);

    private:
        std::string platformNodeName;
    };

    /**
     * \class PlatformUnitDatafieldCreator
     * \brief
     * \ingroup RobotAPI-SensorActorUnits-util
     */
    class PlatformUnitDatafieldCreator
    {
    public:
        PlatformUnitDatafieldCreator(const std::string& channelName) :
            channelName(channelName)
        {}

        /**
         * \brief Function to create a Channel Representation for a PlatformUnitObserver
         * \param platformUnitOberserverName Name of the PlatformUnitObserver
         * \param platformName Name of the platform of the robot like it is specified
         *        in the simox-robot-xml-file
         * \return returns a ChannelRepresentationPtr
         */
        DataFieldIdentifier getDatafield(const std::string& platformUnitObserverName, const std::string& platformName) const
        {
            if (platformUnitObserverName.empty())
            {
                throw LocalException("kinematicUnitObserverName must not be empty!");
            }

            if (platformName.empty())
            {
                throw LocalException("jointName must not be empty!");
            }

            return DataFieldIdentifier(platformUnitObserverName, channelName, platformName);
        }

    private:
        std::string channelName;
    };


    namespace channels
    {
        namespace PlatformUnitObserver
        {
            const PlatformUnitDatafieldCreator platformPose("platformPose");
            const PlatformUnitDatafieldCreator targetPose("targetPose");
            const PlatformUnitDatafieldCreator platformVelocity("platformVelocity");
        }
    }
}

#endif
