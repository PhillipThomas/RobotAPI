/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PlatformUnitSimulation.h"

#include <cmath>

using namespace armarx;

void PlatformUnitSimulation::onInitPlatformUnit()
{
    referenceFrame = getProperty<std::string>("ReferenceFrame").getValue();
    targetPositionX = currentPositionX = getProperty<float>("InitialPosition.x").getValue();
    targetPositionY = currentPositionY = getProperty<float>("InitialPosition.y").getValue();
    targetRotation = 0.0;
    targetRotation = currentRotation = getProperty<float>("InitialRotation").getValue();

    linearVelocity = getProperty<float>("LinearVelocity").getValue();
    angularVelocity = getProperty<float>("AngularVelocity").getValue();

    positionalAccuracy = 0.01;

    intervalMs = getProperty<int>("IntervalMs").getValue();
    ARMARX_VERBOSE << "Starting platform unit simulation with " << intervalMs << " ms interval";
    simulationTask = new PeriodicTask<PlatformUnitSimulation>(this, &PlatformUnitSimulation::simulationFunction, intervalMs, false, "PlatformUnitSimulation");
}

void PlatformUnitSimulation::onStartPlatformUnit()
{
    listenerPrx->reportPlatformPose(currentPositionX, currentPositionY, currentRotation);
    simulationTask->start();
}

void PlatformUnitSimulation::onStopPlatformUnit()
{
    if (simulationTask)
    {
        simulationTask->stop();
    }
}


void PlatformUnitSimulation::onExitPlatformUnit()
{
    if (simulationTask)
    {
        simulationTask->stop();
    }
}


void PlatformUnitSimulation::simulationFunction()
{
    // the time it took until this method was called again
    double timeDeltaInSeconds = (IceUtil::Time::now() - lastExecutionTime).toMilliSecondsDouble() / 1000.0;
    lastExecutionTime = IceUtil::Time::now();
    std::vector<float> vel(3, 0.0f);
    {
        ScopedLock lock(currentPoseMutex);
        float diff = fabs(targetPositionX - currentPositionX);

        if (diff > positionalAccuracy)
        {
            int sign = copysignf(1.0f, (targetPositionX - currentPositionX));
            currentPositionX += sign * std::min<float>(linearVelocity * timeDeltaInSeconds, diff);
            vel[0] = linearVelocity;
        }

        diff = fabs(targetPositionY - currentPositionY);

        if (diff > positionalAccuracy)
        {
            int sign = copysignf(1.0f, (targetPositionY - currentPositionY));
            currentPositionY +=  sign * std::min<float>(linearVelocity * timeDeltaInSeconds, diff);
            vel[1] = linearVelocity;
        }

        diff = fabs(targetRotation - currentRotation);

        if (diff > orientationalAccuracy)
        {
            int sign = copysignf(1.0f, (targetRotation - currentRotation));
            currentRotation += sign * std::min<float>(angularVelocity * timeDeltaInSeconds, diff);

            // stay between +/- M_2_PI
            if (currentRotation > 2 * M_PI)
            {
                currentRotation -= 2 * M_PI;
            }

            if (currentRotation < -2 * M_PI)
            {
                currentRotation += 2 * M_PI;
            }

            vel[2] = angularVelocity;

        }
    }
    listenerPrx->reportPlatformPose(currentPositionX, currentPositionY, currentRotation);
    listenerPrx->reportPlatformVelocity(vel[0], vel[1], vel[2]);
}

void PlatformUnitSimulation::moveTo(Ice::Float targetPlatformPositionX, Ice::Float targetPlatformPositionY, Ice::Float targetPlatformRotation, Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c)
{
    {
        ScopedLock lock(currentPoseMutex);
        targetPositionX = targetPlatformPositionX;
        targetPositionY = targetPlatformPositionY;
        targetRotation = targetPlatformRotation;
        this->positionalAccuracy = positionalAccuracy;
        this->orientationalAccuracy = orientationalAccuracy;
    }
    listenerPrx->reportNewTargetPose(targetPositionX, targetPositionY, targetRotation);
}


PropertyDefinitionsPtr PlatformUnitSimulation::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(
               new PlatformUnitSimulationPropertyDefinitions(getConfigIdentifier()));
}


void armarx::PlatformUnitSimulation::move(float targetPlatformVelocityX, float targetPlatformVelocityY, float targetPlatformVelocityRotation, const Ice::Current& c)
{
    throw LocalException("NYI");
}

void PlatformUnitSimulation::moveRelative(float targetPlatformOffsetX, float targetPlatformOffsetY, float targetPlatformOffsetRotation, float positionalAccuracy, float orientationalAccuracy, const Ice::Current& c)
{
    float targetPositionX, targetPositionY, targetRotation;
    {
        ScopedLock lock(currentPoseMutex);
        targetPositionX = currentPositionX + targetPlatformOffsetX;
        targetPositionY = currentPositionY + targetPlatformOffsetY;
        targetRotation = currentRotation + targetPlatformOffsetRotation;
    }
    moveTo(targetPositionX, targetPositionY, targetRotation, positionalAccuracy, orientationalAccuracy);
}

void PlatformUnitSimulation::setMaxVelocities(float positionalVelocity, float orientaionalVelocity, const Ice::Current& c)
{
    ScopedLock lock(currentPoseMutex);
    linearVelocity = positionalVelocity;
    angularVelocity = orientaionalVelocity;
}

