/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege dot at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_KINEMATIC_UNIT_SIMULATION_H
#define _ARMARX_COMPONENT_KINEMATIC_UNIT_SIMULATION_H

#include "PlatformUnit.h"

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <IceUtil/Time.h>

#include <string>

namespace armarx
{
    /**
     * \class PlatformUnitSimulationPropertyDefinitions
     * \brief
     */
    class PlatformUnitSimulationPropertyDefinitions :
        public PlatformUnitPropertyDefinitions
    {
    public:
        PlatformUnitSimulationPropertyDefinitions(std::string prefix) :
            PlatformUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("IntervalMs", 10, "The time in milliseconds between two calls to the simulation method.");
            defineOptionalProperty<float>("InitialRotation", 0, "Initial rotation of the platform.");
            defineOptionalProperty<float>("InitialPosition.x", 0, "Initial x position of the platform.");
            defineOptionalProperty<float>("InitialPosition.y", 0, "Initial y position of the platform.");
            defineOptionalProperty<std::string>("ReferenceFrame", "Platform", "Reference frame in which the platform position is reported.");
            defineOptionalProperty<float>("LinearVelocity", 1000.0, "Linear velocity of the platform (default: 1000 mm/sec).");
            defineOptionalProperty<float>("AngularVelocity", 1.5, "Angular velocity of the platform (default: 1.5 rad/sec).");
        }
    };

    /**
     * \class PlatformUnitSimulation
     * \brief Simulates a robot platform.
     * \ingroup RobotAPI-SensorActorUnits-simulation
     */
    class PlatformUnitSimulation :
        virtual public PlatformUnit
    {
    public:
        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "PlatformUnitSimulation";
        }

        virtual void onInitPlatformUnit();
        virtual void onStartPlatformUnit();
        void onStopPlatformUnit();
        virtual void onExitPlatformUnit();

        void simulationFunction();

        // proxy implementation
        void moveTo(Ice::Float targetPlatformPositionX, Ice::Float targetPlatformPositionY, Ice::Float targetPlatformRotation, Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c = ::Ice::Current());

        /**
         * \warning Not yet implemented!
         */
        void move(float targetPlatformVelocityX, float targetPlatformVelocityY, float targetPlatformVelocityRotation, const Ice::Current& c = Ice::Current());

        void moveRelative(float targetPlatformOffsetX, float targetPlatformOffsetY, float targetPlatformOffsetRotation, float positionalAccuracy, float orientationalAccuracy, const Ice::Current& c = Ice::Current());
        void setMaxVelocities(float positionalVelocity, float orientaionalVelocity, const Ice::Current& c = Ice::Current());

        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    protected:
        boost::mutex currentPoseMutex;
        IceUtil::Time lastExecutionTime;
        int intervalMs;

        ::Ice::Float targetPositionX;
        ::Ice::Float targetPositionY;
        ::Ice::Float currentPositionX;
        ::Ice::Float currentPositionY;
        ::Ice::Float targetRotation;
        ::Ice::Float currentRotation;

        ::Ice::Float linearVelocity;
        ::Ice::Float angularVelocity;

        ::Ice::Float positionalAccuracy;
        ::Ice::Float orientationalAccuracy;

        std::string referenceFrame;

        PeriodicTask<PlatformUnitSimulation>::pointer_type simulationTask;


    };
}

#endif
