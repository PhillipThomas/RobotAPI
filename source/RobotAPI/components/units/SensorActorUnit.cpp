/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::untis
* @author     Kai Welke (welke@kit.edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "SensorActorUnit.h"
#include <Ice/ObjectAdapter.h>

using namespace armarx;

#define CALLINFO //ARMARX_INFO <<__FILE__ << ":" << __PRETTY_FUNCTION__ << flush;

SensorActorUnit::SensorActorUnit()
{
    executionState = eUnitConstructed;
}

SensorActorUnit::~SensorActorUnit()
{

}

void SensorActorUnit::init(const Ice::Current& c)
{
    std::string currentName = c.adapter->getName();
    Ice::Identity currentId = c.adapter->getCommunicator()->stringToIdentity(currentName);

    if (ownerId == currentId)
    {
        CALLINFO

        if (executionState == eUnitConstructed)
        {
            executionState = eUnitInitialized;
            onInit();
        }
        else
        {
            ARMARX_LOG << armarx::eWARN << "Unit " << getName() << " is already initialized." << armarx::flush;
        }
    }
    else
    {
        ARMARX_LOG << armarx::eWARN << "Unit " << getName() << " can not be initialized while unrequested." << armarx::flush;
    }
}

void SensorActorUnit::start(const Ice::Current& c)
{
    std::string currentName = c.adapter->getName();
    Ice::Identity currentId = c.adapter->getCommunicator()->stringToIdentity(currentName);

    if (ownerId == currentId)
    {
        CALLINFO

        if ((executionState == eUnitInitialized) || (executionState == eUnitStopped))
        {
            executionState = eUnitStarted;
            onStart();
        }
        else
        {
            ARMARX_LOG << armarx::eWARN << "Unit " << getName() << " could not be started." << armarx::flush;
        }
    }
    else
    {
        ARMARX_LOG << armarx::eWARN << "Unit " << getName() << " can not be started while unrequested." << armarx::flush;
    }
}

void SensorActorUnit::stop(const Ice::Current& c)
{
    std::string currentName = c.adapter->getName();
    Ice::Identity currentId = c.adapter->getCommunicator()->stringToIdentity(currentName);

    if (ownerId == currentId)
    {
        CALLINFO

        if (executionState == eUnitStarted)
        {
            executionState = eUnitStopped;
            onStop();
        }
        else
        {
            ARMARX_LOG << armarx::eWARN << "Unit " << getName() << " could not be stopped." << armarx::flush;
        }
    }
    else
    {
        ARMARX_LOG << armarx::eWARN << "Unit " << getName() << " can not be stopped while unrequested. " << armarx::flush;
    }
}

UnitExecutionState SensorActorUnit::getExecutionState(const Ice::Current& c)
{
    CALLINFO
    return executionState;
}


void SensorActorUnit::request(const Ice::Current& c)
{
    CALLINFO

    // try to lock unit
    if (!unitMutex.try_lock())
    {
        ARMARX_LOG << armarx::eERROR << "Trying to request already owned unit " << getName() << "\n" << armarx::flush;
        throw ResourceUnavailableException("Trying to request already owned unit");
    }

    // retrieve owner id from current connection
    std::string ownerName = c.adapter->getName();
    ownerId = c.adapter->getCommunicator()->stringToIdentity(ownerName);

    ARMARX_INFO << "unit requested by " << ownerName << flush;
}

void SensorActorUnit::release(const Ice::Current& c)
{
    CALLINFO
    // retrieve owner id from current connection
    std::string callerName = c.adapter->getName();
    Ice::Identity callerId = c.adapter->getCommunicator()->stringToIdentity(callerName);

    if (!(ownerId == callerId))
    {
        ARMARX_LOG << armarx::eERROR << "Trying to release unit owned by another component" << armarx::flush;
        throw ResourceNotOwnedException("Trying to release unit owned by another component");
    }

    // unlock mutex
    ownerId = c.adapter->getCommunicator()->stringToIdentity(" ");
    unitMutex.unlock();

    ARMARX_INFO << "unit released" << flush;
}

void SensorActorUnit::onExitComponent()
{
    unitMutex.try_lock(); // try to lock, to ensure that it is locked on unlock call
    unitMutex.unlock();
}
