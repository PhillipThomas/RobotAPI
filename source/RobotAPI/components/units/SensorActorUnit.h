/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::utils
* @author     Kai Welke (welke@kit.edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef SENSOR_ACTOR_UNIT_H
#define SENSOR_ACTOR_UNIT_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/interface/units/UnitInterface.h>


namespace armarx
{
    /**
    * \class SensorActorUnit
    * \ingroup RobotAPI-SensorActorUnits
    * \brief Base Class for SensorActorUnits.
    *
    * SensorActorUnits are ArmarX component which provide an actor API and a sensor data stream.
    * The SensorActorUnit provides basic execution state handling and concurrent access handling.
    */
    class ARMARXCORE_IMPORT_EXPORT SensorActorUnit :
        virtual public SensorActorUnitInterface,
        virtual public Component
    {
    public:
        /**
        * Constructs a SensorActorUnit.
        */
        SensorActorUnit();
        /**
        * Destructor of SensorActorUnit.
        */
        virtual ~SensorActorUnit();

        /**
        * Set execution state to eInitialized.
        *
        * Assures that init is called only once and the calls subclass method onInit().
        *
        * \param c Ice context provided by the Ice framework
        */
        virtual void init(const Ice::Current& c = ::Ice::Current());

        /**
        * Set execution state to eStarted
        *
        * Start streaming of sensory data and acceptance of control data.
        *
        * Start can be called if the unit is initialized and not started yet (stopped).
        * Calls subclass method inStart().
        *
        * \param c Ice context provided by the Ice framework
        */
        virtual void start(const Ice::Current& c = ::Ice::Current());

        /**
        * Set execution state to eStopped
        *
        * Stop streaming of sensory data and acceptance of control data.
        *
        * Stop can be called if the unit is started.
        * Calls subclass method onStop()
        *
        * \param c Ice context provided by the Ice framework
        */
        virtual void stop(const Ice::Current& c = ::Ice::Current());

        /**
        * Retrieve current execution state
        *
        * \param c Ice context provided by the Ice framework
        * \return current execution state
        */
        UnitExecutionState getExecutionState(const Ice::Current& c = ::Ice::Current());

        /**
        * Request exclusive access to current unit. Throws ResourceUnavailableException on error.
        *
        * \param c Ice context provided by the Ice framework
        */
        virtual void request(const Ice::Current& c = ::Ice::Current());

        /**
        * Release exclusive access to current unit. Throws ResourceUnavailableException on error.
        *
        * \param c Ice context provided by the Ice framework
        */
        virtual void release(const Ice::Current& c = ::Ice::Current());

    protected:
        void onExitComponent();
        /**
        * callback onInit for subclass hook. See init().
        */
        virtual void onInit() {};

        /**
        * callback onStart for subclass hook. See start().
        */
        virtual void onStart() {};

        /**
        * callback onStop for subclass hook. See stop().
        */
        virtual void onStop() {};

        boost::mutex unitMutex;
        Ice::Identity ownerId;
        UnitExecutionState  executionState;
    };
}

#endif
