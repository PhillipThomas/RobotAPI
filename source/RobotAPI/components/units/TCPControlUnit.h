/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter (mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_TCPCONTROLUNIT_H
#define _ARMARX_TCPCONTROLUNIT_H

#include <RobotAPI/interface/units/TCPControlUnit.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/Component.h>

#include <VirtualRobot/IK/DifferentialIK.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

namespace armarx
{
    /**
     * \class TCPControlUnitPropertyDefinitions
     * \brief
     */
    class TCPControlUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        TCPControlUnitPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the KinematicUnit Proxy");
            defineOptionalProperty<std::string>("RobotStateTopicName", "RobotState", "Name of the RobotComponent State topic.");
            defineOptionalProperty<float>("MaxJointVelocity", 30.f / 180 * 3.141, "Maximal joint velocity in rad/sec");
            defineRequiredProperty<std::string>("RobotFileName", "Robot file name, e.g. robot_model.xml");
            defineOptionalProperty<int>("CycleTime", 30, "Cycle time of the tcp control in ms");
            defineOptionalProperty<float>("MaximumCommandDelay", 20000, "Delay after which the TCP Control unit releases itself if no new velocity have been set.");
            defineOptionalProperty<std::string>("TCPsToReport", "", "comma seperated list of nodesets' endeffectors, which poses and velocities that should be reported. * for all, empty for none");


        }
    };

    /**
     * \defgroup Component-TCPControlUnit TCPControlUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Unit for controlling a tool center point (TCP).
     *
     * This class implements the interface to control a node of a robot (e.g a TCP)
     * in cartesian coordinates in velocity control mode. It takes velocities in mm/s for
     * translations and rad/s for orientation. Several nodessets can be controlled simultaneously.
     *
     * Before the TCPControlUnit can be used, request() needs to be called. Since a cartesian velocity needs
     * is only correct for the current joint configuration, it needs to recalculated as fast as possible.
     * Thus, after request is called the TCPControlUnit recalculates the velocity in a given cycle time and
     * updates the joint velocities. To set another cycle time use setCycleTime().
     * To set the velocity for a node use setTCPVelocity. Calling setTCPVelocity again with another nodeset
     * will add this nodeset to the list of currently controlled TCPs.
     *
     * \note After usage release() **must** be called to stop the recalcuation and setting of joint velocities.
     */

    /**
     * @ingroup Component-TCPControlUnit
     * @brief The TCPControlUnit class
     */
    class TCPControlUnit :
        virtual public Component,
        virtual public TCPControlUnitInterface
    {
    public:
        TCPControlUnit();

        // TCPControlUnitInterface interface

        /**
         * \brief Sets the cycle time with which the joint velocities are recalculated.
         * \param milliseconds New cycle time.
         * \param c Ice Context, leave blank.
         */
        void setCycleTime(Ice::Int milliseconds, const Ice::Current& c = Ice::Current());

        /**
         * \brief Sets the cartesian velocity of a node in a nodeset for translation and/or orientation.
         * It is best to provide the data in global coordinates. Otherwise the coordinates frame transformation is done in place
         * on the current robot state, which might not be the same as when the command was given. Additionally, execution inaccurracies
         * might propagate if local coordinate frames are used.
         * \param nodeSetName Nodeset that should be used for moving the node, i.e. tcp
         * \param tcpName Name of the VirtualRobot node that should be moved
         * \param translationVelocity Target cartesian translation velocity in mm/s, but might not be reached. If NULL the translation is ommitted in the calculation.
         * Thus the translation behaviour is undefined und the node/tcp position might change.
         * \param orientationVelocityRPY Target cartesian orientation velocity in rad/s in roll-pitch-yaw, but might not be reached. If NULL the orientation is ommitted in the calculation.
         * Thus the orientation behaviour is undefined und the node/tcp orientation might change.
         * \param c Ice Context, leave blank.
         *
         * \see request(), release()
         */
        void setTCPVelocity(const std::string& nodeSetName, const std::string& tcpName, const::armarx::FramedDirectionBasePtr& translationVelocity, const::armarx::FramedDirectionBasePtr& orientationVelocityRPY, const Ice::Current& c = Ice::Current());

        // UnitExecutionManagementInterface interface
        /**
         * \brief Does not do anything at the moment.
         * \param c
         */
        void init(const Ice::Current& c = Ice::Current());
        /**
         * \brief Does not do anything at the moment.
         * \param c
         */
        void start(const Ice::Current& c = Ice::Current());
        /**
         * \brief Does not do anything at the moment.
         * \param c
         */
        void stop(const Ice::Current& c = Ice::Current());
        UnitExecutionState getExecutionState(const Ice::Current& c = Ice::Current());

        // UnitResourceManagementInterface interface
        /**
         * \brief Triggers the calculation loop for using cartesian velocity. Call once before/after setting a tcp velocity with SetTCPVelocity.
         * \param c Ice Context, leave blank.
         */
        void request(const Ice::Current& c = Ice::Current());

        /**
         * \brief Releases and stops the recalculation and updating of joint velocities.
         * Call always when finished with cartesian control. The target velocity values of
         * all node set will be deleted in this function.
         * \param c Ice Context, leave blank.
         */
        void release(const Ice::Current& c = Ice::Current());

    protected:

        void onInitComponent();
        void onConnectComponent();
        void onDisconnectComponent();
        void onExitComponent();
        std::string getDefaultName() const
        {
            return "TCPControlUnit";
        }

        // PropertyUser interface
        PropertyDefinitionsPtr createPropertyDefinitions();

        static Eigen::VectorXf CalcNullspaceJointDeltas(const Eigen::VectorXf& desiredJointDeltas, const Eigen::MatrixXf& jacobian, const Eigen::MatrixXf& jacobianInverse);
        static Eigen::VectorXf CalcJointLimitAvoidanceDeltas(VirtualRobot::RobotNodeSetPtr robotNodeSet, const Eigen::MatrixXf& jacobian, const Eigen::MatrixXf& jacobianInverse, Eigen::VectorXf desiredJointValues = Eigen::VectorXf());
        void calcAndSetVelocities();
    private:
        static void ContinuousAngles(const Eigen::AngleAxisf& oldAngle, Eigen::AngleAxisf& newAngle, double& offset);
        void periodicExec();
        void periodicReport();
        Eigen::VectorXf calcJointVelocities(VirtualRobot::RobotNodeSetPtr robotNodeSet, Eigen::VectorXf tcpDelta, VirtualRobot::RobotNodePtr refFrame, VirtualRobot::IKSolver::CartesianSelection mode);
        Eigen::VectorXf applyMaxJointVelocity(const Eigen::VectorXf& jointVelocity, float maxJointVelocity);

        float maxJointVelocity;
        int cycleTime;
        Eigen::Matrix4f lastTCPPose;

        struct TCPVelocityData
        {
            FramedDirectionPtr translationVelocity;
            FramedDirectionPtr orientationVelocity;
            std::string nodeSetName;
            std::string tcpName;
        };

        typedef std::map<std::string, TCPVelocityData> TCPVelocityDataMap;
        TCPVelocityDataMap tcpVelocitiesMap, localTcpVelocitiesMap;

        typedef std::map<std::string, VirtualRobot::DifferentialIKPtr> DIKMap;
        DIKMap dIKMap, localDIKMap;


        PeriodicTask<TCPControlUnit>::pointer_type execTask;
        PeriodicTask<TCPControlUnit>::pointer_type topicTask;

        RobotStateComponentInterfacePrx robotStateComponentPrx;
        KinematicUnitInterfacePrx kinematicUnitPrx;
        VirtualRobot::RobotPtr jointExistenceCheckRobot;
        VirtualRobot::RobotPtr localRobot;
        VirtualRobot::RobotPtr localReportRobot;
        VirtualRobot::RobotPtr localVelReportRobot;
        TCPControlUnitListenerPrx listener;

        DebugObserverInterfacePrx debugObs;

        bool requested;
        std::map<std::string, double> oriOffsets;


        std::vector<VirtualRobot::RobotNodePtr > nodesToReport;
        FramedPoseBaseMap lastTCPPoses;
        IceUtil::Time lastTopicReportTime;
        IceUtil::Time lastCommandTime;


        Mutex dataMutex;
        Mutex taskMutex;
        Mutex reportMutex;
        bool calculationRunning;
        double syncTimeDelta;

        std::string topicName;



        // KinematicUnitListener interface
    protected:
        void reportControlModeChanged(const NameControlModeMap&, bool, const Ice::Current&);
        void reportJointAngles(const NameValueMap& jointAngles, bool, const Ice::Current&);
        void reportJointVelocities(const NameValueMap& jointVel, bool, const Ice::Current&);
        void reportJointTorques(const NameValueMap&, bool, const Ice::Current&);
        void reportJointAccelerations(const NameValueMap& jointAccelerations, bool aValueChanged, const Ice::Current& c);
        void reportJointCurrents(const NameValueMap&, bool, const Ice::Current&);
        void reportJointMotorTemperatures(const NameValueMap&, bool, const Ice::Current&);
        void reportJointStatuses(const NameStatusMap&, bool, const Ice::Current&);

    };

    class EDifferentialIK : public VirtualRobot::DifferentialIK, virtual public Logging
    {
    public:
        typedef Eigen::VectorXf(EDifferentialIK::*ComputeFunction)(float);

        EDifferentialIK(VirtualRobot::RobotNodeSetPtr rns, VirtualRobot:: RobotNodePtr coordSystem = VirtualRobot::RobotNodePtr(), VirtualRobot::JacobiProvider::InverseJacobiMethod invJacMethod = eSVD);

        VirtualRobot::RobotNodePtr getRefFrame()
        {
            return coordSystem;
        }
        int getJacobianRows()
        {
            return nRows;
        }

        Eigen::MatrixXf calcFullJacobian();

        void clearGoals();
        void setRefFrame(VirtualRobot::RobotNodePtr coordSystem);

        void setGoal(const Eigen::Matrix4f& goal, VirtualRobot::RobotNodePtr tcp, VirtualRobot::IKSolver::CartesianSelection mode, float tolerancePosition, float toleranceRotation, Eigen::VectorXf tcpWeight);

        void setDOFWeights(Eigen::VectorXf dofWeights);
        //        void setTCPWeights(Eigen::VectorXf tcpWeights);
        bool computeSteps(float stepSize = 1.f, float mininumChange = 0.01f, int maxNStep = 50);
        bool computeSteps(Eigen::VectorXf& resultJointDelta, float stepSize = 1.f, float mininumChange = 0.01f, int maxNStep = 50, ComputeFunction computeFunction = &DifferentialIK::computeStep);
        Eigen::VectorXf computeStep(float stepSize);
        void applyDOFWeightsToJacobian(Eigen::MatrixXf& Jacobian);
        void applyTCPWeights(VirtualRobot::RobotNodePtr tcp, Eigen::MatrixXf& partJacobian);
        void applyTCPWeights(Eigen::MatrixXf& invJacobian);
        float getWeightedError();
        float getWeightedErrorPosition(VirtualRobot::SceneObjectPtr tcp);
        Eigen::VectorXf computeStepIndependently(float stepSize);
        bool solveIK(float stepSize = 1, float minChange = 0.01f, int maxSteps = 50, bool useAlternativeOnFail = false);
    protected:
        bool adjustDOFWeightsToJointLimits(const Eigen::VectorXf& plannedJointDeltas);

        Eigen::VectorXf dofWeights;
        std::map<VirtualRobot:: SceneObjectPtr, Eigen::VectorXf> tcpWeights;
        Eigen::VectorXf tcpWeightVec;
    };
    typedef boost::shared_ptr<EDifferentialIK> EDifferentialIKPtr;

}

#endif
