/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#ifndef _ARMARX_TCPCONTROLUNITOBSERVER_H
#define _ARMARX_TCPCONTROLUNITOBSERVER_H

#include <ArmarXCore/observers/Observer.h>
#include <RobotAPI/interface/units/TCPControlUnit.h>

namespace armarx
{
    /**
     * \class TCPControlUnitObserverPropertyDefinitions
     * \brief
     */
    class TCPControlUnitObserverPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        TCPControlUnitObserverPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("TCPControlUnitName", "Name of the TCPControlUnit");
        }
    };

    /**
     * \class TCPControlUnitObserver
     * \ingroup RobotAPI-SensorActorUnits-observers
     * \brief Observer monitoring TCP-related sensor values.
     *
     * The TCPControlUnitObserver allows the installation of conditions on all channel reported by the TCPControlUnit.
     * These include TCP pose and velocity.
     *
     * Available condition checks are: *updated*, *equals*, *approx*, *larger*, *smaller* and *magnitudelarger*.
     */
    class TCPControlUnitObserver :
        virtual public Observer,
        virtual public TCPControlUnitObserverInterface
    {
    public:
        TCPControlUnitObserver();

        // framework hooks
        virtual std::string getDefaultName() const
        {
            return "TCPControlUnitObserver";
        }
        void onInitObserver();
        void onConnectObserver();

        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    public:
        // TCPControlUnitListener interface
        void reportTCPPose(const FramedPoseBaseMap& poseMap, const Ice::Current& c = Ice::Current());
        void reportTCPVelocities(const FramedDirectionMap& tcpTranslationVelocities, const FramedDirectionMap& tcpOrientationVelocities, const Ice::Current& c = Ice::Current());

        Mutex dataMutex;
    };

}

#endif
