/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "HandUnitConfigDialog.h"
#include "ui_HandUnitConfigDialog.h"

#include <RobotAPI/components/units/HandUnit.h>
#include <IceUtil/UUID.h>

armarx::HandUnitConfigDialog::HandUnitConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::HandUnitConfigDialog),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);
    setName(getDefaultName()); // @@@ This is necessary for more than 1 widget or even reopening the widget.


    proxyFinder = new IceProxyFinder<HandUnitInterfacePrx>(this);
    proxyFinder->setSearchMask("*Unit");
    ui->proxyFinderContainer->addWidget(proxyFinder, 0, 0, 1, 1);
}

armarx::HandUnitConfigDialog::~HandUnitConfigDialog()
{
    delete ui;
}



void armarx::HandUnitConfigDialog::onInitComponent()
{
    proxyFinder->setIceManager(getIceManager());
}

void armarx::HandUnitConfigDialog::onConnectComponent()
{
}
