#include "HandUnitGuiPlugin.h"
#include "HandUnitConfigDialog.h"
#include "ui_HandUnitConfigDialog.h"
#include <ArmarXCore/core/system/ArmarXDataPath.h>

// Qt headers
#include <Qt>
#include <QtGlobal>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>

#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>

using namespace armarx;


HandUnitGuiPlugin::HandUnitGuiPlugin()
{
    addWidget<HandUnitWidget>();
}


HandUnitWidget::HandUnitWidget() :
    handName("NOT SET YET"),
    handUnitProxyName(""),
    setJointAnglesFlag(false)
{
    // init gui
    ui.setupUi(getWidget());

    jointAngleUpdateTask = new PeriodicTask<HandUnitWidget>(this, &HandUnitWidget::setJointAngles, 50);
}


void HandUnitWidget::onInitComponent()
{
    usingProxy(handUnitProxyName);
    //usingTopic(handName + "State");
    //ARMARX_WARNING << "Listening on Topic: " << handName + "State";
}

void HandUnitWidget::onConnectComponent()
{
    connectSlots();
    jointAngleUpdateTask->start();

    handUnitProxy = getProxy<HandUnitInterfacePrx>(handUnitProxyName);
    handName = handUnitProxy->getHandName();

    // @@@ In simulation hand is called 'Hand L'/'Hand R'. On 3b Hand is called 'TCP R'
    if (handName != "Hand L" && handName != "Hand R" && handName != "TCP L" && handName != "TCP R")
    {
        //QMessageBox::warning(NULL, "Hand not supported", QString("Hand with name \"") + QString::fromStdString(handName) + " \" is not suppored.");
        ARMARX_WARNING << "Hand with name \"" << handName << "\" is not supported.";
    }

    ui.labelInfo->setText(QString::fromStdString(handUnitProxyName + " :: " + handName));

    SingleTypeVariantListPtr preshapeStrings = SingleTypeVariantListPtr::dynamicCast(handUnitProxy->getShapeNames());
    QStringList list;
    int preshapeCount = preshapeStrings->getSize();

    for (int i = 0; i < preshapeCount; ++i)
    {
        std::string shape = ((preshapeStrings->getVariant(i))->get<std::string>());
        list << QString::fromStdString(shape);
    }

    ui.comboPreshapes->clear();
    ui.comboPreshapes->addItems(list);
}

void HandUnitWidget::onDisconnectComponent()
{
    jointAngleUpdateTask->stop();
}

void HandUnitWidget::onExitComponent()
{
}

QPointer<QDialog> HandUnitWidget::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new HandUnitConfigDialog(parent);
        //dialog->ui->editHandUnitName->setText(QString::fromStdString(handUnitProxyName));
        //dialog->ui->editHandName->setText(QString::fromStdString(handName));
    }

    return qobject_cast<HandUnitConfigDialog*>(dialog);
}

void HandUnitWidget::configured()
{
    handUnitProxyName = dialog->proxyFinder->getSelectedProxyName().toStdString();
}

void HandUnitWidget::preshapeHand()
{
    setPreshape(ui.comboPreshapes->currentText().toUtf8().data());
}

void HandUnitWidget::setJointAngles()
{
    if (!handUnitProxy)
    {
        ARMARX_WARNING << "invalid proxy";
        return;
    }

    if (!setJointAnglesFlag)
    {
        return;
    }

    setJointAnglesFlag = false;

    NameValueMap ja;

    if (handName == "Hand L" || handName == "TCP L")
    {
        ja["Hand Palm 2 L"] = ui.horizontalSliderPalm->value() * M_PI / 180;
        ja["Index L J0"] = ui.horizontalSliderIndexJ0->value() * M_PI / 180;
        ja["Index L J1"] = ui.horizontalSliderIndexJ1->value() * M_PI / 180;
        ja["Middle L J0"] = ui.horizontalSliderMiddleJ0->value() * M_PI / 180;
        ja["Middle L J1"] = ui.horizontalSliderMiddleJ1->value() * M_PI / 180;
        ja["Thumb L J0"] = ui.horizontalSliderThumbJ0->value() * M_PI / 180;
        ja["Thumb L J1"] = ui.horizontalSliderThumbJ1->value() * M_PI / 180;
        float rinkyValue = ui.horizontalSliderRinky->value() * M_PI / 180;
        ja["Ring L J0"] = rinkyValue;
        ja["Ring L J1"] = rinkyValue;
        ja["Pinky L J0"] = rinkyValue;
        ja["Pinky L J1"] = rinkyValue;
    }
    else if (handName == "Hand R" || handName == "TCP R")
    {
        ja["Hand Palm 2 R"] = ui.horizontalSliderPalm->value() * M_PI / 180;
        ja["Index R J0"] = ui.horizontalSliderIndexJ0->value() * M_PI / 180;
        ja["Index R J1"] = ui.horizontalSliderIndexJ1->value() * M_PI / 180;
        ja["Middle R J0"] = ui.horizontalSliderMiddleJ0->value() * M_PI / 180;
        ja["Middle R J1"] = ui.horizontalSliderMiddleJ1->value() * M_PI / 180;
        ja["Thumb R J0"] = ui.horizontalSliderThumbJ0->value() * M_PI / 180;
        ja["Thumb R J1"] = ui.horizontalSliderThumbJ1->value() * M_PI / 180;
        float rinkyValue = ui.horizontalSliderRinky->value() * M_PI / 180;
        ja["Ring R J0"] = rinkyValue;
        ja["Ring R J1"] = rinkyValue;
        ja["Pinky R J0"] = rinkyValue;
        ja["Pinky R J1"] = rinkyValue;
    }
    else
    {
        ARMARX_WARNING << "Hand with name \"" << handName << "\" is not supported.";
    }

    handUnitProxy->setJointAngles(ja);
}

void HandUnitWidget::requestSetJointAngles()
{
    setJointAnglesFlag = true;
}

void HandUnitWidget::openHand()
{
    setPreshape("Open");
}

void HandUnitWidget::closeHand()
{
    setPreshape("Close");
}

void HandUnitWidget::closeThumb()
{
    setPreshape("Thumb");
}

void HandUnitWidget::relaxHand()
{
    setPreshape("Relax");
}

void HandUnitWidget::setPreshape(std::string preshape)
{
    ARMARX_INFO << "Setting new hand shape: " << preshape;
    handUnitProxy->setShape(preshape);
}

void HandUnitWidget::loadSettings(QSettings* settings)
{
    handUnitProxyName = settings->value("handUnitProxyName", QString::fromStdString(handUnitProxyName)).toString().toStdString();
    handName = settings->value("handName", QString::fromStdString(handName)).toString().toStdString();
}

void HandUnitWidget::saveSettings(QSettings* settings)
{
    settings->setValue("handUnitProxyName", QString::fromStdString(handUnitProxyName));
    settings->setValue("handName", QString::fromStdString(handName));
}


void HandUnitWidget::connectSlots()
{
    connect(ui.buttonPreshape, SIGNAL(clicked()), this, SLOT(preshapeHand()));
    connect(ui.buttonOpenHand, SIGNAL(clicked()), this, SLOT(openHand()));
    connect(ui.buttonCloseHand, SIGNAL(clicked()), this, SLOT(closeHand()));
    connect(ui.buttonCloseThumb, SIGNAL(clicked()), this, SLOT(closeThumb()));
    connect(ui.buttonRelaxHand, SIGNAL(clicked()), this, SLOT(relaxHand()));
    //connect(ui.comboPreshapes, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(selectPreshape(const QString&)));
    connect(ui.buttonSetJointAngles, SIGNAL(clicked()), this, SLOT(requestSetJointAngles()));
    connect(ui.horizontalSliderIndexJ0, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()));
    connect(ui.horizontalSliderIndexJ1, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()));
    connect(ui.horizontalSliderMiddleJ0, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()));
    connect(ui.horizontalSliderMiddleJ1, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()));
    connect(ui.horizontalSliderRinky, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()));
    connect(ui.horizontalSliderPalm, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()));
    connect(ui.horizontalSliderThumbJ0, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()));
    connect(ui.horizontalSliderThumbJ1, SIGNAL(sliderMoved(int)), this, SLOT(requestSetJointAngles()));
}





void armarx::HandUnitWidget::reportHandShaped(const std::string& handName, const std::string& handShapeName, const Ice::Current&)
{
    ARMARX_IMPORTANT << handName << ": " << handShapeName;
}

void armarx::HandUnitWidget::reportNewHandShapeName(const std::string& handName, const std::string& handShapeName, const Ice::Current&)
{
    ARMARX_IMPORTANT << handName << ": " << handShapeName;
}



void armarx::HandUnitWidget::reportJointAngles(const armarx::NameValueMap& actualJointAngles, const Ice::Current& c)
{

}



void armarx::HandUnitWidget::reportJointPressures(const armarx::NameValueMap& actualJointPressures, const Ice::Current& c)
{

}

Q_EXPORT_PLUGIN2(robotapi_gui_HandUnitGuiPlugin, HandUnitGuiPlugin)
