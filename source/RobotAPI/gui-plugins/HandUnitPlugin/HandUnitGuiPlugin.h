/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::ObjectExaminerGuiPlugin
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @copyright  2012
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#ifndef _ROBOTAPI_ARMARXGUI_PLUGINS_HANDUNITWIDGET_H
#define _ROBOTAPI_ARMARXGUI_PLUGINS_HANDUNITWIDGET_H

/* ArmarX headers */
#include "ui_HandUnitGuiPlugin.h"
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/interface/units/HandUnitInterface.h>

/* Qt headers */
#include <QtGui/QMainWindow>

#include <string>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>


namespace armarx
{
    class HandUnitConfigDialog;

    /**
      \class HandUnitGuiPlugin
      \brief This plugin offers a widget witch which the HandUnit can be controlled.
      \see HandUnitWidget
      */
    class HandUnitGuiPlugin :
        public ArmarXGuiPlugin
    {
    public:
        HandUnitGuiPlugin();
        QString getPluginName()
        {
            return "HandUnitGuiPlugin";
        }
    };

    /*!
     * \page RobotAPI-GuiPlugins-HandUnitWidget HandUnitGuiPlugin
     * \brief With this widget the HandUnit can be controlled.
     * \image html HandUnitGUI.png
     * You can either select a preshape from the drop-down-menu on top or set each
     * joint individually.
     * When you add the widget to the Gui, you need to specify the following parameters:
     *
     * Parameter Name   | Example Value     | Required?     | Description
     *  :----------------  | :-------------:   | :-------------- |:--------------------
     * Proxy     | LeftHandUnit   | Yes | The hand unit you want to control.
     */
    class HandUnitWidget :
        public ArmarXComponentWidgetController,
        public HandUnitListener
    {
        Q_OBJECT
    public:
        HandUnitWidget();
        ~HandUnitWidget()
        {}

        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onDisconnectComponent();
        virtual void onExitComponent();

        // HandUnitListener interface
        void reportHandShaped(const std::string&, const std::string&, const Ice::Current&);
        void reportNewHandShapeName(const std::string&, const std::string&, const Ice::Current&);


        // inherited of ArmarXWidget
        virtual QString getWidgetName() const
        {
            return "RobotControl.HandUnitGUI";
        }
        virtual QIcon getWidgetIcon() const
        {
            return QIcon("://icons/hand.svg");
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0);
        virtual void loadSettings(QSettings* settings);
        virtual void saveSettings(QSettings* settings);
        void configured();

    public slots:

        void preshapeHand();
        void setJointAngles();
        void requestSetJointAngles();
        void openHand();
        void closeHand();
        void closeThumb();
        void relaxHand();

    private:
        void setPreshape(std::string preshape);

    protected:
        void connectSlots();

        Ui::HandUnitGuiPlugin ui;

    private:
        std::string handName;
        //std::string preshapeName;

        std::string handUnitProxyName;
        HandUnitInterfacePrx handUnitProxy;

        //QPointer<QWidget> __widget;
        QPointer<HandUnitConfigDialog> dialog;

        PeriodicTask<HandUnitWidget>::pointer_type jointAngleUpdateTask;
        bool setJointAnglesFlag;


        // HandUnitListener interface
    public:
        void reportJointAngles(const::armarx::NameValueMap& actualJointAngles, const Ice::Current&);
        void reportJointPressures(const::armarx::NameValueMap& actualJointPressures, const Ice::Current&);
    };
    typedef boost::shared_ptr<HandUnitWidget> HandUnitGuiPluginPtr;
}

#endif
