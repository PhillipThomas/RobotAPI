#include "HapticUnitGuiPlugin.h"
#include "HapticUnitConfigDialog.h"
#include "ui_HapticUnitConfigDialog.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/util/variants/eigen3/MatrixVariant.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <IceUtil/Time.h>


// Qt headers
#include <QInputDialog>
#include <Qt>
#include <QtGlobal>
#include <QtGui/QPushButton>
#include <QtGui/QComboBox>
#include <QtGui/QMenu>

using namespace armarx;


HapticUnitGuiPlugin::HapticUnitGuiPlugin()
{
    addWidget<HapticUnitWidget>();
}


HapticUnitWidget::HapticUnitWidget()
{
    // init gui
    ui.setupUi(getWidget());
    //ui.stateOpen->setChecked(true);
    hapticObserverProxyName = "HapticUnitObserver";
    hapticUnitProxyName = "WeissHapticUnit";

    updateTimer = new QTimer(this);




}


void HapticUnitWidget::onInitComponent()
{
    usingProxy(hapticObserverProxyName);
    usingProxy(hapticUnitProxyName);

}


void HapticUnitWidget::onConnectComponent()
{
    hapticObserverProxy = getProxy<ObserverInterfacePrx>(hapticObserverProxyName);
    weissHapticUnit = getProxy<WeissHapticUnitInterfacePrx>(hapticUnitProxyName);

    connectSlots();
    createMatrixWidgets();
    updateTimer->start(25); // 50 Hz

}


void HapticUnitWidget::onExitComponent()
{
    updateTimer->stop();
}

void HapticUnitWidget::onDisconnectComponent()
{
    updateTimer->stop();
}


QPointer<QDialog> HapticUnitWidget::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new HapticUnitConfigDialog(parent);
    }

    return qobject_cast<HapticUnitConfigDialog*>(dialog);
}


void HapticUnitWidget::configured()
{

}

void HapticUnitWidget::updateData()
{

    for (std::pair<std::string, MatrixDatafieldDisplayWidget*> pair : matrixDisplays)
    {
        //MatrixFloatPtr matrix = VariantPtr::dynamicCast(hapticObserverProxy->getDataField(new DataFieldIdentifier(hapticObserverProxyName, pair.first, "matrix")))->get<MatrixFloat>();
        std::string name = hapticObserverProxy->getDataField(new DataFieldIdentifier(hapticObserverProxyName, pair.first, "name"))->getString();
        std::string deviceName = hapticObserverProxy->getDataField(new DataFieldIdentifier(hapticObserverProxyName, pair.first, "device"))->getString();
        //float rate = hapticObserverProxy->getDataField(new DataFieldIdentifier(hapticObserverProxyName, pair.first, "rate"))->getFloat();
        TimestampVariantPtr timestamp = VariantPtr::dynamicCast(hapticObserverProxy->getDataField(new DataFieldIdentifier(hapticObserverProxyName, pair.first, "timestamp")))->get<TimestampVariant>();
        MatrixDatafieldDisplayWidget* matrixDisplay = pair.second;
        matrixDisplay->setInfoOverlay(QString::fromStdString(deviceName) + ": " + QString::fromStdString(name) + "\n" + QString::fromStdString(timestamp->toTime().toDateTime()));
        matrixDisplay->invokeUpdate();
    }

}

void HapticUnitWidget::onContextMenu(QPoint point)
{
    QMenu* contextMenu = new QMenu(getWidget());
    MatrixDatafieldDisplayWidget* matrixDisplay = qobject_cast<MatrixDatafieldDisplayWidget*>(sender());

    if (!matrixDisplay)
    {
        return;
    }

    QAction* setDeviceTag = contextMenu->addAction(tr("Set Device Tag"));

    QAction* action = contextMenu->exec(matrixDisplay->mapToGlobal(point));

    std::string channelName = channelNameReverseMap.at(matrixDisplay);
    std::string deviceName = deviceNameReverseMap.at(matrixDisplay);

    if (action == setDeviceTag)
    {
        std::string tag = hapticObserverProxy->getDataField(new DataFieldIdentifier(hapticObserverProxyName, channelName, "name"))->getString();

        bool ok;
        QString newTag = QInputDialog::getText(getWidget(), tr("Set Device Tag"),
                                               QString::fromStdString("New Tag for Device '" + deviceName + "'"), QLineEdit::Normal,
                                               QString::fromStdString(tag), &ok);

        if (ok && !newTag.isEmpty())
        {
            ARMARX_IMPORTANT_S << "requesting to set new device tag for " << deviceName << ": " << newTag.toStdString();
            weissHapticUnit->setDeviceTag(deviceName, newTag.toStdString());
        }
    }

    delete contextMenu;
}


void HapticUnitWidget::loadSettings(QSettings* settings)
{
}


void HapticUnitWidget::saveSettings(QSettings* settings)
{
}


void HapticUnitWidget::connectSlots()
{
    connect(this, SIGNAL(doUpdateDisplayWidgets()), SLOT(updateDisplayWidgets()), Qt::QueuedConnection);
    connect(updateTimer, SIGNAL(timeout()), this, SLOT(updateData()));
    connect(ui.checkBoxOffsetFilter, SIGNAL(stateChanged(int)), this, SLOT(onCheckBoxOffsetFilterStateChanged(int)));
}

void HapticUnitWidget::createMatrixWidgets()
{
    //ARMARX_LOG << "HapticUnitWidget::createMatrixWidgets()";
    emit doUpdateDisplayWidgets();
}

void HapticUnitWidget::updateDisplayWidgets()
{
    QLayoutItem* child;

    while ((child = ui.gridLayoutDisplay->takeAt(0)) != 0)
    {
        delete child;
    }

    int i = 0;
    ChannelRegistry channels = hapticObserverProxy->getAvailableChannels(false);

    for (std::pair<std::string, ChannelRegistryEntry> pair : channels)
    {
        std::string channelName = pair.first;
        MatrixDatafieldDisplayWidget* matrixDisplay = new MatrixDatafieldDisplayWidget(new DatafieldRef(hapticObserverProxy, channelName, "matrix"), hapticObserverProxy, getWidget());
        matrixDisplay->setRange(0, 4095);
        matrixDisplay->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(matrixDisplay, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onContextMenu(QPoint)));
        matrixDisplays.insert(std::make_pair(pair.first, matrixDisplay));
        std::string deviceName = hapticObserverProxy->getDataField(new DataFieldIdentifier(hapticObserverProxyName, channelName, "device"))->getString();
        channelNameReverseMap.insert(std::make_pair(matrixDisplay, channelName));
        deviceNameReverseMap.insert(std::make_pair(matrixDisplay, deviceName));
        ui.gridLayoutDisplay->addWidget(matrixDisplay, 0, i);
        i++;
    }
}

void HapticUnitWidget::onCheckBoxOffsetFilterStateChanged(int state)
{
    //ARMARX_IMPORTANT << "onCheckBoxOffsetFilterToggled: " << state;
    for (std::pair<std::string, MatrixDatafieldDisplayWidget*> pair : matrixDisplays)
    {
        pair.second->enableOffsetFilter(ui.checkBoxOffsetFilter->isChecked());
    }
}


Q_EXPORT_PLUGIN2(robotapi_gui_HapticUnitGuiPlugin, HapticUnitGuiPlugin)
