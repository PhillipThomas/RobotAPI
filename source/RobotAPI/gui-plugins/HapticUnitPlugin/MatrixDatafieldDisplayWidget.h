#ifndef MATRIXDISPLAYWIDGET_H
#define MATRIXDISPLAYWIDGET_H

#include <QWidget>
#include <QMutex>
#include <eigen3/Eigen/Dense>
#include <valarray>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>


using Eigen::MatrixXf;

namespace Ui
{
    class MatrixDatafieldDisplayWidget;
}

namespace armarx
{
    class MatrixDatafieldDisplayWidget : public QWidget
    {
        Q_OBJECT

    signals:
        void doUpdate();

    private slots:
        void updateRequested();

    public:
        explicit MatrixDatafieldDisplayWidget(DatafieldRefBasePtr matrixDatafield, ObserverInterfacePrx observer, QWidget* parent = 0);
        ~MatrixDatafieldDisplayWidget();
        void paintEvent(QPaintEvent*);


        void setRange(float min, float max)
        {
            this->min = min;
            this->max = max;
        }
        void enableOffsetFilter(bool enabled);
        QColor getColor(float value, float min, float max);

        void invokeUpdate()
        {
            emit doUpdate();
        }
        void setInfoOverlay(QString infoOverlay)
        {
            mtx.lock();
            this->infoOverlay = infoOverlay;
            mtx.unlock();
        }

    private:
        void drawMatrix(const QRect& target, QPainter& painter);
        void drawPercentiles(const QRect& target, QPainter& painter);

    private:
        Ui::MatrixDatafieldDisplayWidget* ui;
        MatrixXf data;
        std::vector<float> percentiles;
        float min, max;
        std::valarray<QColor> colors;
        QMutex mtx;
        QString infoOverlay;
        DatafieldRefPtr matrixDatafield;
        DatafieldRefPtr matrixDatafieldOffsetFiltered;
        DatafieldRefPtr percentilesDatafield;
        ObserverInterfacePrx observer;
    };
}

#endif // MATRIXDISPLAYWIDGET_H
