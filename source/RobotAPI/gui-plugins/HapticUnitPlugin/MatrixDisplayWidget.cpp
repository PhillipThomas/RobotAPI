#include "MatrixDisplayWidget.h"
#include "ui_MatrixDisplayWidget.h"

#include <QPainter>
#include <pthread.h>
#include <iostream>

using namespace std;
using namespace armarx;

MatrixDisplayWidget::MatrixDisplayWidget(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::MatrixDisplayWidget)
{
    ui->setupUi(this);
    this->min = 0;
    this->max = 1;
    this->data = MatrixXf(1, 1);
    this->data(0, 0) = 0;
    QColor c[] = {QColor::fromHsv(0, 0, 0), QColor::fromHsv(240, 255, 255), QColor::fromHsv(270, 255, 255), QColor::fromHsv(300, 255, 255),
                  QColor::fromHsv(0, 255, 255), QColor::fromHsv(30, 255, 255), QColor::fromHsv(60, 255, 255)
                 };
    this->colors = std::valarray<QColor>(c, sizeof c / sizeof c[0]);

    //connect(this, SIGNAL(updateData(MatrixXf)), SLOT(setData(MatrixXf)), Qt::QueuedConnection);
    connect(this, SIGNAL(doUpdate()), SLOT(update()), Qt::QueuedConnection);
}

MatrixDisplayWidget::~MatrixDisplayWidget()
{
    delete ui;
}

void MatrixDisplayWidget::paintEvent(QPaintEvent*)
{
    mtx.lock();
    MatrixXf data = this->data;

    //cout << "[" << pthread_self() << "] MatrixDisplayWidget::paintEvent" << endl;

    int pixelSize = std::min(width() / data.cols(), height() / data.rows());
    int dx = (width() - pixelSize * data.cols()) / 2;
    int dy = (height() - pixelSize * data.rows()) / 2;
    QPainter painter(this);
    painter.fillRect(rect(), QColor::fromRgb(0, 0, 0));
    painter.setFont(QFont("Arial", 8));

    for (int x = 0; x < data.cols(); x++)
    {
        for (int y = 0; y < data.rows(); y++)
        {
            QRect target = QRect(dx + x * pixelSize, dy + y * pixelSize, pixelSize, pixelSize);
            painter.fillRect(target, getColor(data(y, x), min, max));
            painter.drawText(target, Qt::AlignCenter, QString::number(data(y, x)));
        }
    }

    painter.setFont(QFont("Arial", 12));
    painter.drawText(rect(), Qt::AlignBottom | Qt::AlignRight, infoOverlay);

    mtx.unlock();
}

QColor MatrixDisplayWidget::getColor(float value, float min, float max)
{
    value = (value - min) / (max - min) * (colors.size() - 1);

    if (value < 0)
    {
        return colors[0];
    }

    if (value >= colors.size() - 1)
    {
        return colors[colors.size() - 1];
    }

    int i = (int)value;
    float f2 = value - i;
    float f1 = 1 - f2;
    QColor c1 = colors[i];
    QColor c2 = colors[i + 1];
    return QColor((int)(c1.red() * f1 + c2.red() * f2), (int)(c1.green() * f1 + c2.green() * f2), (int)(c1.blue() * f1 + c2.blue() * f2));
}
