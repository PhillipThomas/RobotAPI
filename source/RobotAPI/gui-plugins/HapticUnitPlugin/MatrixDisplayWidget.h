#ifndef MATRIXDISPLAYWIDGET_H
#define MATRIXDISPLAYWIDGET_H

#include <QWidget>
#include <QMutex>
#include <eigen3/Eigen/Dense>
#include <valarray>

using Eigen::MatrixXf;

namespace Ui
{
    class MatrixDisplayWidget;
}

namespace armarx
{
    class MatrixDisplayWidget : public QWidget
    {
        Q_OBJECT

    signals:
        void doUpdate();

    public slots:
        void setData(MatrixXf data)
        {
            mtx.lock();
            this->data = data;
            mtx.unlock();
            //this->update();
        }

    public:
        explicit MatrixDisplayWidget(QWidget* parent = 0);
        ~MatrixDisplayWidget();
        void paintEvent(QPaintEvent*);



        void setRange(float min, float max)
        {
            this->min = min;
            this->max = max;
        }
        QColor getColor(float value, float min, float max);

        void invokeUpdate()
        {
            emit doUpdate();
        }
        void setInfoOverlay(QString infoOverlay)
        {
            mtx.lock();
            this->infoOverlay = infoOverlay;
            mtx.unlock();
        }

    private:
        Ui::MatrixDisplayWidget* ui;
        MatrixXf data;
        float min, max;
        std::valarray<QColor> colors;
        QMutex mtx;
        QString infoOverlay;
    };
}

#endif // MATRIXDISPLAYWIDGET_H
