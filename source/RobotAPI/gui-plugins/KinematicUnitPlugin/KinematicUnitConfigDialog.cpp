/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "KinematicUnitConfigDialog.h"
#include "ui_KinematicUnitConfigDialog.h"

#include <QTimer>
#include <QPushButton>
#include <QMessageBox>


// ArmarX
#include <RobotAPI/interface/units/KinematicUnitInterface.h>

#include <IceUtil/UUID.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

using namespace armarx;

KinematicUnitConfigDialog::KinematicUnitConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::KinematicUnitConfigDialog),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);
    fileDialog = new QFileDialog(parent);
    fileDialog->setModal(true);
    fileDialog->setFileMode(QFileDialog::ExistingFiles);
    QStringList fileTypes;
    fileTypes << tr("XML (*.xml)")
              << tr("All Files (*.*)");
    fileDialog->setNameFilters(fileTypes);
    connect(ui->btnSelectModelFile, SIGNAL(clicked()), fileDialog, SLOT(show()));
    connect(fileDialog, SIGNAL(accepted()), this, SLOT(modelFileSelected()));
    connect(this->ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfig()));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
    proxyFinder = new IceProxyFinder<KinematicUnitInterfacePrx>(this);
    proxyFinder->setSearchMask("*");
    ui->gridLayout->addWidget(proxyFinder, 2, 1, 1, 2);
    topicFinder = new IceTopicFinder(this);
    topicFinder->setSearchMask("*RobotState");
    ui->gridLayout->addWidget(topicFinder, 3, 1, 1, 2);

}

KinematicUnitConfigDialog::~KinematicUnitConfigDialog()
{
    delete ui;
}

void KinematicUnitConfigDialog::onInitComponent()
{
    proxyFinder->setIceManager(getIceManager());
    topicFinder->setIceManager(getIceManager());
}

void KinematicUnitConfigDialog::onConnectComponent()
{

}

void KinematicUnitConfigDialog::onExitComponent()
{
    QObject::disconnect();

}



void KinematicUnitConfigDialog::modelFileSelected()
{
    ui->editRobotFilepath->setText(fileDialog->selectedFiles()[0]);
}

void KinematicUnitConfigDialog::verifyConfig()
{
    if (proxyFinder->getSelectedProxyName().trimmed().length() == 0)
    {
        QMessageBox::critical(this, "Invalid Configuration", "The proxy name must not be empty");
    }
    else if (topicFinder->getSelectedProxyName().trimmed().length() == 0)
    {
        QMessageBox::critical(this, "Invalid Configuration", "The topic name must not be empty");
    }
    else
    {
        this->accept();
    }
}



