/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef KINEMATICUNITCONFIGDIALOG_H
#define KINEMATICUNITCONFIGDIALOG_H

#include <QDialog>
#include <QFileDialog>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

namespace Ui
{
    class KinematicUnitConfigDialog;
}

namespace armarx
{
    class KinematicUnitConfigDialog :
        public QDialog,
        virtual public ManagedIceObject
    {
        Q_OBJECT

    public:
        explicit KinematicUnitConfigDialog(QWidget* parent = 0);
        ~KinematicUnitConfigDialog();

        // inherited from ManagedIceObject
        std::string getDefaultName() const
        {
            return "KinematicUnitConfigDialog" + uuid;
        }
        void onInitComponent();
        void onConnectComponent();
        void onExitComponent();

        void updateKinematicUnitList();
    signals:

    public slots:
        void modelFileSelected();
        void verifyConfig();
    private:

        IceProxyFinderBase* proxyFinder;
        IceProxyFinderBase* topicFinder;
        Ui::KinematicUnitConfigDialog* ui;
        QFileDialog* fileDialog;
        std::string uuid;
        friend class KinematicUnitWidgetController;
    };
}

#endif
