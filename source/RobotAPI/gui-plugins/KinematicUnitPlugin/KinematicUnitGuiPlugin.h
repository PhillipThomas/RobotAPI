/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::KinematicUnitGuiPlugin
* @author     Christian Boege <boege at kit dot edu>
* @copyright  2011 Christian Böge
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#ifndef ARMARX_COMPONENT_GUIHANDLERKINEMATICUNIT_GUI_H
#define ARMARX_COMPONENT_GUIHANDLERKINEMATICUNIT_GUI_H

/* ArmarX headers */
#include <RobotAPI/gui-plugins/KinematicUnitPlugin/ui_kinematicunitguiplugin.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/Component.h>

/* Qt headers */
#include <QtGui/QMainWindow>


#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Visualization/VisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/nodes/SoNode.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/Qt/SoQt.h>
#include <QStyledItemDelegate>

#include <boost/shared_ptr.hpp>
#include <boost/cstdint.hpp>

namespace armarx
{
    typedef boost::shared_ptr<VirtualRobot::CoinVisualization> CoinVisualizationPtr;

    class KinematicUnitConfigDialog;

    /*!
     \class KinematicUnitGuiPlugin
     \brief This plugin provides a generic widget showing position and velocity of all joints. Optionally a 3d robot model can be visualized.
     \see KinematicUnitWidget
    */
    class KinematicUnitGuiPlugin :
        public ArmarXGuiPlugin
    {
    public:
        KinematicUnitGuiPlugin();

        QString getPluginName()
        {
            return "KinematicUnitGuiPlugin";
        }
    };

    class RangeValueDelegate :
        public QStyledItemDelegate
    {
        void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    };

    /*!
      \page RobotAPI-GuiPlugins-KinematicUnitPlugin KinematicUnitPlugin

      \brief This widget shows position and velocity of all joints. Optionally a 3d robot model can be visualized.

      \image html KinematicUnitGUI.png
      When you add the widget to the Gui, you need to specify the following parameters:

      Parameter Name   | Example Value     | Required?     | Description
      :----------------  | :-------------:   | :-------------- |:--------------------
      Robot model filepath     | $ArmarX_Core/Armar3/data/Armar3/robotmodel/ArmarIII.xml  | Yes | The robot model to use. This needs to be the same model the kinematic unit is using.
      Robot nodeset name     | Robot          | Yes | ?
      Kinematic unit name - Proxy | Armar3KinematicUnit | Yes | The kinematic unit you wish to observe/control.
      RobotState Topic Name | RobotState | ? | ?

      This widget allows you to both observe and control a kinematic unit. All joints are listed in
      the table in the center of the widget. The 3D viewer shows the current state of the robot.

      On the top you can select the joint you wish to control and the control mode. You can control
      a joint with the slider below.
     */
    class KinematicUnitWidgetController :
        public ArmarXComponentWidgetController,
        public KinematicUnitListener
    {
        Q_OBJECT
    public:
        /**
         * @brief Holds the column index for the joint tabel.
         * Used to avoid magic numbers.
         */
        enum JointTabelColumnIndex : int
        {
            eTabelColumnName = 0,
            eTabelColumnControlMode,
            eTabelColumnAngleProgressbar,
            eTabelColumnVelocity,
            eTabelColumnTorque,
            eTabelColumnCurrent,
            eTabelColumnTemperature,
            eTabelColumnOperation,
            eTabelColumnError,
            eTabelColumnEnabled,
            eTabelColumnEmergencyStop
        };

        enum Roles
        {
            eJointAngleRole = Qt::UserRole + 1,
            eJointHiRole,
            eJointLoRole
        };

        KinematicUnitWidgetController();
        virtual ~KinematicUnitWidgetController() {}

        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onExitComponent();

        // inherited of ArmarXWidget
        virtual QString getWidgetName() const
        {
            return "RobotControl.KinematicUnitGUI";
        }
        virtual QIcon getWidgetIcon() const
        {
            return QIcon("://icons/kinematic_icon.svg");
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0);
        virtual void loadSettings(QSettings* settings);
        virtual void saveSettings(QSettings* settings);
        void configured();

        SoNode* getScene();

        bool kinematicUnitSetupViewer();

        void modelUpdateCB();

        void updateGuiElements();

        // overwrite setMutex, so that we can inform the debugdrawer
        virtual void setMutex3D(boost::shared_ptr<boost::recursive_mutex> mutex3D);

    signals:

        void jointAnglesReported();
        void jointVelocitiesReported();
        void jointControlModesReported();
        void jointTorquesReported();
        void jointCurrentsReported();
        void jointMotorTemperaturesReported();
        void jointStatusesReported();


    public slots:

        // KinematicUnit
        void kinematicUnitZeroPosition();
        void setControlModePosition();
        void setControlModeVelocity();
        void setControlModeTorque();
        void selectJoint(int i);
        void selectJointFromTableWidget(int row, int column);
        void sliderValueChanged(int i);

        /**
         * @brief Sets the Slider ui.horizontalSliderKinematicUnitPos to 0 if
         * this->selectedControlMode is eVelocityControl.
         */
        void resetSliderInVelocityControl();

        void updateJointAnglesTable();
        void updateJointVelocitiesTable();
        void updateJointTorquesTable();
        void updateJointCurrentsTable();
        void updateJointMotorTemperaturesTable();
        void updateJointStatusesTable();
        void updateControlModesTable();
        void updateKinematicUnitListInDialog() {}


    protected:
        void connectSlots();
        void initializeUi();

        QString translateStatus(OperationStatus status);
        QString translateStatus(ErrorStatus status);

        Ui::KinematicUnitGuiPlugin ui;

        // ice proxies
        KinematicUnitInterfacePrx kinematicUnitInterfacePrx;// send commands to kinematic unit
        KinematicUnitListenerPrx kinematicUnitListenerPrx; // receive reports from kinematic unit

        bool verbose;

        std::string kinematicUnitFile;
        std::string kinematicUnitFileDefault;
        std::string kinematicUnitName;
        std::string topicName;
        std::string robotNodeSetName;

        VirtualRobot::RobotPtr robot;
        VirtualRobot::RobotNodeSetPtr robotNodeSet;
        VirtualRobot::RobotNodePtr currentNode;

        CoinVisualizationPtr kinematicUnitVisualization;
        SoNode* kinematicUnitNode;
        SoSeparator* rootVisu;

        SoSeparator* debugLayerVisu;
        armarx::DebugDrawerComponentPtr debugDrawer;

        // interface implementation
        void reportControlModeChanged(const NameControlModeMap& jointModes, bool aValueChanged, const Ice::Current& c);
        void reportJointAngles(const NameValueMap& jointAngles, bool aValueChanged, const Ice::Current& c);
        void reportJointVelocities(const NameValueMap& jointVelocities, bool aValueChanged, const Ice::Current& c);
        void reportJointTorques(const NameValueMap& jointTorques, bool aValueChanged, const Ice::Current& c);
        void reportJointAccelerations(const NameValueMap& jointAccelerations, bool aValueChanged, const Ice::Current& c);
        void reportJointCurrents(const NameValueMap& jointCurrents, bool aValueChanged, const Ice::Current& c);
        void reportJointMotorTemperatures(const NameValueMap& jointMotorTemperatures, bool aValueChanged, const Ice::Current& c);
        void reportJointStatuses(const NameStatusMap& jointStatuses, bool aValueChanged, const Ice::Current&);


        void updateModel();

    protected slots:
        void showVisuLayers(bool show);
    private:

        boost::recursive_mutex mutexNodeSet;
        // init stuff
        VirtualRobot::RobotPtr loadRobotFile(std::string fileName);
        CoinVisualizationPtr getCoinVisualization(VirtualRobot::RobotPtr robot);
        VirtualRobot::RobotNodeSetPtr getRobotNodeSet(VirtualRobot::RobotPtr robot, std::string nodeSetName);
        bool initGUIComboBox(VirtualRobot::RobotNodeSetPtr robotNodeSet);
        bool initGUIJointListTable(VirtualRobot::RobotNodeSetPtr robotNodeSet);

        NameValueMap reportedJointAngles;
        NameValueMap reportedJointVelocities;
        NameControlModeMap reportedJointControlModes;
        NameValueMap reportedJointTorques;
        NameValueMap reportedJointCurrents;
        NameValueMap reportedJointMotorTemperatures;
        NameStatusMap reportedJointStatuses;

        std::vector<float> dirty_squaresum_old;

        QPointer<QWidget> __widget;
        QPointer<KinematicUnitConfigDialog> dialog;
        ControlMode selectedControlMode;

        RangeValueDelegate delegate;
        /**
         * @brief The zero position of the slider
         */
        static const int SLIDER_ZERO_POSITION = 0;

        /**
         * @brief Returns values in
         * (-ui.jitterThresholdSpinBox->value(),ui.jitterThresholdSpinBox->value())
         * as 0. (Other values remain unchanged)
         * @param value The value with jitter.
         * @return The value without jitter.
         */
        float cutJitter(float value);
    };
    typedef ::std::vector< ::Ice::Float> FloatVector;
    typedef boost::shared_ptr<KinematicUnitWidgetController> KinematicUnitGuiPluginPtr;


}

#endif
