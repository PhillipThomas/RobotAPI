/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    ArmarX::Component::ObjectExaminerGuiPlugin
* \author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* \copyright  2012
* \license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#ifndef _ARMARXGUI_PLUGINS_PLATFORMUNITWIDGET_H
#define _ARMARXGUI_PLUGINS_PLATFORMUNITWIDGET_H

/* ArmarX headers */
#include "ui_PlatformUnitGuiPlugin.h"
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/JoystickControlWidget.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/interface/units/PlatformUnitInterface.h>

/* Qt headers */
#include <QtGui/QMainWindow>
#include <QTimer>

#include <string>


namespace armarx
{
    class PlatformUnitConfigDialog;

    /**
      \class PlatformUnitGuiPlugin
      \brief This plugin provides a widget with which the PlatformUnit can be controlled.
      \see PlatformUnitWidget
      */
    class PlatformUnitGuiPlugin :
        public ArmarXGuiPlugin
    {
    public:
        PlatformUnitGuiPlugin();
        QString getPluginName()
        {
            return "PlatformUnitGuiPlugin";
        }
    };

    /*!
      \page RobotAPI-GuiPlugins-PlatformUnitPlugin PlatformUnitPlugin
      \brief With this widget the PlatformUnit can be controlled.

      \image html PlatformUnitGUI.png "The plugin's ui." width=300px
            -# The current position and rotation, fields to enter a new target and a button to set the platform in motion.
            -# A joystick like control widget to move the platform. The platform does not rotate to move in a direction. Up moves the platform forward.
            -# A joystick like control widget to rotate the platform.
            -# Be careful to set a maximum velocity before using the joysticks.

       When you add the widget to the Gui, you need to specify the following parameters:

       Parameter Name   | Example Value     | Required?     | Description
       :----------------  | :-------------:   | :-------------- |:--------------------
       PlatformUnit - Proxy     | PlatformUnit   | Yes | The name of the platform unit.
       Platform | Platform | Yes | The name of the platform.
      */
    class PlatformUnitWidget :
        public ArmarXComponentWidgetController,
        public PlatformUnitListener
    {
        Q_OBJECT
    public:
        PlatformUnitWidget();
        ~PlatformUnitWidget()
        {}

        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onDisconnectComponent();
        virtual void onExitComponent();

        // slice interface implementation
        void reportPlatformPose(::Ice::Float currentPlatformPositionX, ::Ice::Float currentPlatformPositionY, ::Ice::Float currentPlatformRotation, const Ice::Current& c = ::Ice::Current());
        void reportNewTargetPose(::Ice::Float newPlatformPositionX, ::Ice::Float newPlatformPositionY, ::Ice::Float newPlatformRotation, const Ice::Current& c = ::Ice::Current());
        void reportPlatformVelocity(::Ice::Float currentPlatformVelocityX, ::Ice::Float currentPlatformVelocityY, ::Ice::Float currentPlatformVelocityRotation, const Ice::Current& c = ::Ice::Current());

        // inherited of ArmarXWidget
        virtual QString getWidgetName() const
        {
            return "RobotControl.PlatformUnitGUI";
        }
        virtual QIcon getWidgetIcon() const
        {
            return QIcon("://icons/retro_joystick2.svg");
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0);
        virtual void loadSettings(QSettings* settings);
        virtual void saveSettings(QSettings* settings);
        void configured();

    public slots:

        void moveTo();

        void setNewPlatformPoseLabels(float x, float y, float alpha);
        void setNewTargetPoseLabels(float x, float y, float alpha);

        void startControlTimer();
        void stopControlTimer();

    protected:
        void connectSlots();

        Ui::PlatformUnitGuiPlugin ui;

    private slots:
        /**
          \brief Checks the joystick contol widgets speedCtrl and rotaCtrl and performs a move if necessary.
            Activated when ctrlEvaluationTimer times out.
         */
        void controlTimerTick();

        /**
              \brief Stops the platform
              */
        void stopPlatform();

    private:
        std::string platformUnitProxyName;
        std::string platformName;

        PlatformUnitInterfacePrx platformUnitProxy;

        QPointer<QWidget> __widget;
        QPointer<PlatformUnitConfigDialog> dialog;

        /**
         * \brief A Joystick control for the platform speed.
         * (currently the velocity control is nyi is the simulator so this is emulated by
         * adding a delta to the target positions)
         */
        QPointer<JoystickControlWidget> speedCtrl;

        /**
         * \brief A Joystick control for the platform rotation.
         */
        QPointer<JoystickControlWidget> rotaCtrl;

        /**
         * \brief A timer to evaluate the speedCtrl and rotaCtrl
         */
        QTimer ctrlEvaluationTimer;

        /**
         * \brief Holds the last reported platform rotation. (required to emulate the speed control)
         */
        ::Ice::Float platformRotation;

        /**
         * \brief Whether the platform is moved with speedCtrl.
         */
        bool platformMoves;

        /**
         * \brief Whether the platform is moved with speedCtrl
         */
        ::Ice::Float platformRotationAtMoveStart;

        /**
         * \brief The tick rate (in ms) for the ctrlEvaluationTimer.
         */
        static const int CONTROL_TICK_RATE = 50;
    };
    typedef boost::shared_ptr<PlatformUnitWidget> PlatformUnitGuiPluginPtr;
}

#endif
