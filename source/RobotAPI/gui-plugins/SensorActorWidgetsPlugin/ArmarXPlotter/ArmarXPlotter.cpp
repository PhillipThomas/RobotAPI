/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXPlotter.h"
#include "ArmarXPlotterDialog.h"

#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/exceptions/local/InvalidChannelException.h>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_canvas.h>
#include <qwt_legend.h>
#include <qwt_legend_item.h>
#include <qwt_series_data.h>

//QT
#include <QTimer>
#include <QTime>
#include <QSettings>
#include <QDir>

#include <boost/date_time/posix_time/posix_time.hpp>

#include <IceUtil/IceUtil.h>

#include <sstream>


using namespace std;

namespace armarx
{

    ArmarXPlotter::ArmarXPlotter() :
        ArmarXComponentWidgetController(),
        updateInterval(100),
        shownInterval(60),
        startUpTime(QDateTime::currentDateTime())


    {
        setTag("Plotter");
        ui.setupUi(getWidget());
        timer = new QTimer(getWidget());

        ////////////////
        //  Setup Plotter
        ///////////////
        // panning with the left mouse button
        (void) new QwtPlotPanner(ui.qwtPlot->canvas());

        // zoom in/out with the wheel
        QwtPlotMagnifier* magnifier = new QwtPlotMagnifier(ui.qwtPlot->canvas());
        magnifier->setAxisEnabled(QwtPlot::xBottom, false);

        ui.qwtPlot->canvas()->setPaintAttribute(QwtPlotCanvas::BackingStore, false); //increases performance for incremental drawing

        QwtLegend* legend = new QwtLegend;
        legend->setItemMode(QwtLegend::CheckableItem);
        ui.qwtPlot->insertLegend(legend, QwtPlot::BottomLegend);


        ui.qwtPlot->setAxisTitle(QwtPlot::xBottom, "Time (in sec)");
        //        ui.qwtPlot->setAutoReplot();

        dialog = new ArmarXPlotterDialog(getWidget(), NULL);
        loggingDir = (QDir::currentPath());
        dialog->ui.editLoggingDirectory->setText(loggingDir);
    }

    ArmarXPlotter::~ArmarXPlotter()
    {
        //        if(dialog && this->getState() == eManagedIceObjectInitialized)
        //            getArmarXManager()->removeObjectNonBlocking(dialog);
        //        delete dialog;
    }







    void ArmarXPlotter::onInitComponent()
    {
        json = new JSONObject(getIceManager()->getCommunicator());

    }

    void ArmarXPlotter::onConnectComponent()
    {
        ARMARX_VERBOSE << "ArmarXPlotter started" << flush;
        dialog->setIceManager(getIceManager());

        connect(dialog, SIGNAL(accepted()), this, SLOT(configDone()));
        connect(ui.BTNEdit, SIGNAL(clicked()), this, SLOT(configDialog()));
        connect(timer, SIGNAL(timeout()), this, SLOT(updateGraph()));
        connect(ui.BTNPlotterStatus, SIGNAL(toggled(bool)), this, SLOT(plottingPaused(bool)));

        connect(ui.BTNAutoScale, SIGNAL(toggled(bool)), this, SLOT(autoScale(bool)));
        connect(ui.qwtPlot, SIGNAL(legendChecked(QwtPlotItem*, bool)),
                SLOT(showCurve(QwtPlotItem*, bool)));

        connect(ui.btnLogToFile, SIGNAL(toggled(bool)), this, SLOT(toggleLogging(bool)));

        if (!QMetaObject::invokeMethod(this, "setupCurves"))
        {
            ARMARX_WARNING << "Failed to invoke enable";
        }


    }

    void ArmarXPlotter::onExitComponent()
    {
        if (pollingTask)
        {
            pollingTask->stop();
        }
    }

    void ArmarXPlotter::onCloseWidget(QCloseEvent* event)
    {
        ARMARX_VERBOSE << "closing" << flush;
        timer->stop();

        if (logstream.is_open())
        {
            logstream.close();
        }
    }

    void ArmarXPlotter::ButtonAddSensorChannelClicked()
    {
    }

    void ArmarXPlotter::configDialog()
    {
        if (!dialog)
        {
            return;
        }

        dialog->ui.spinBoxUpdateInterval->setValue(updateInterval);
        dialog->ui.spinBoxShownInterval->setValue(shownInterval);
        dialog->ui.listWidget->clear();
        dialog->ui.listWidget->addItems(selectedChannels);
        //        if(dialog->exec())
        //            configDone();
        dialog->setModal(true);
        dialog->show();
        //        dialog->raise();
        //        dialog->activateWindow();
        //        dialog->setParent(0);
    }

    void ArmarXPlotter::configDone()
    {
        {
            ScopedLock lock(dataMutex);
            updateInterval = dialog->ui.spinBoxUpdateInterval->value();
            shownInterval = dialog->ui.spinBoxShownInterval->value();
            pollingInterval = dialog->ui.spinBoxPollingInterval->value();
            selectedChannels = dialog->getSelectedChannels();
            loggingDir = dialog->ui.editLoggingDirectory->text();
            ui.btnLogToFile->setChecked(false);
            curves.clear();
            ui.qwtPlot->detachItems();
        }

        setupCurves();
    }

    void ArmarXPlotter::toggleLogging(bool toggled)
    {
        if (toggled)
        {

            std::string filename = "datalog-" + IceUtil::Time::now().toDateTime() + ".csv";
            boost::replace_all(filename, "/", "-");
            filename = loggingDir.toStdString() + "/" + filename;
            ARMARX_INFO << "Logging to " << filename;
            logstream.open(filename);
            logStartTime = IceUtil::Time::now();

            if (!logstream.is_open())
            {
                ARMARX_ERROR << "Could not open file for logging: " << filename;
                ui.btnLogToFile->setChecked(false);
            }
            else
            {
                logstream << "Timestamp";

                for (auto& channel : selectedChannels)
                {
                    logstream <<  "," << channel.toStdString();
                }

                logstream << std::endl;
            }
        }
        else
        {
            logstream.close();
        }
    }

    void ArmarXPlotter::pollingExec()
    {
        ScopedLock lock(dataMutex);
        auto newData = getData(selectedChannels, dataMap);

        if (ui.btnLogToFile->isChecked())
        {
            logToFile(IceUtil::Time::now(),  newData);
        }
    }



    void ArmarXPlotter::updateGraph()
    {
        ScopedLock lock(dataMutex);
        QPointF p;

        //        int size = shownInterval*1000/updateInterval;
        GraphDataMap::iterator it = dataMap.begin();
        IceUtil::Time curTime = IceUtil::Time::now();

        for (; it != dataMap.end(); ++it)
        {

            QVector<QPointF> pointList;
            pointList.clear();
            std::vector<TimeData>& dataVec = it->second;
            //            int newSize = min(size,(int)dataVec.size());
            pointList.resize(dataVec.size());

            try
            {
                int j = 0;

                for (int i = dataVec.size() - 1; i >= 0 ; --i)
                {
                    TimeData& data = dataVec[i];
                    IceUtil::Time age = (data.time - curTime);

                    if (age.toMilliSecondsDouble() <= shownInterval * 1000)
                    {
                        VariantPtr var = VariantPtr::dynamicCast(data.data);

                        if (var->getInitialized())
                        {

                            if ((signed int)j < pointList.size())
                            {
                                if (var->getType() == VariantType::Float)
                                {
                                    pointList[j].setY(var->getFloat());
                                }
                                else if (var->getType() == VariantType::Double)
                                {
                                    pointList[j].setY(var->getDouble());
                                }
                                else if (var->getType() == VariantType::Int)
                                {
                                    pointList[j].setY(var->getInt());
                                }
                                else
                                {
                                    continue;
                                }

                                pointList[j].setX(0.001f * age.toMilliSecondsDouble());
                            }
                            else
                            {
                                if (var->getType() == VariantType::Float)
                                {
                                    p.setY(var->getFloat());
                                }
                                else if (var->getType() == VariantType::Double)
                                {
                                    p.setY(var->getDouble());
                                }
                                else if (var->getType() == VariantType::Int)
                                {
                                    p.setY(var->getInt());
                                }
                                else
                                {
                                    continue;
                                }

                                p.setX(0.001 * age.toMilliSecondsDouble());
                                pointList.push_back(p);
                            }

                            j++;
                        }
                        else
                        {
                            ARMARX_WARNING << "uninitialized field: " << it->first;
                        }

                    }
                    else
                    {
                        break;    // data too old from now
                    }
                }
            }
            catch (...)
            {
                handleExceptions();
            }

            QwtSeriesData<QPointF>* pointSeries = new  QwtPointSeriesData(pointList);

            if (curves.find(it->first) != curves.end())
            {
                QwtPlotCurve* curve = curves[it->first];
                curve->setData(pointSeries);
            }
        }


        ui.qwtPlot->setAxisScale(QwtPlot::xBottom, shownInterval * -1, 0.f);

        if (ui.BTNAutoScale->isChecked())
        {
            ui.qwtPlot->setAxisAutoScale(QwtPlot::yLeft, true);
        }

        //        ui.qwtPlot->setAxisScale( QwtPlot::yLeft, -1, 1);

        ui.qwtPlot->replot();
    }

    void ArmarXPlotter::showCurve(QwtPlotItem* item, bool on)
    {
        item->setVisible(on);

        QwtLegendItem* legendItem =
            qobject_cast<QwtLegendItem*>(ui.qwtPlot->legend()->find(item));

        if (legendItem)
        {
            legendItem->setChecked(on);
        }

        ui.qwtPlot->replot();
    }

    void ArmarXPlotter::autoScale(bool toggled)
    {
        ARMARX_VERBOSE << "clicked autoscale" << flush;

        ui.qwtPlot->setAxisAutoScale(QwtPlot::yLeft, toggled);
        ui.qwtPlot->replot();


    }

    void ArmarXPlotter::plottingPaused(bool toggled)
    {
        ScopedLock lock(dataMutex);
        __plottingPaused = toggled;

        if (pollingTask)
        {
            pollingTask->stop();
        }

        if (__plottingPaused)
        {
            timer->stop();
        }
        else
        {
            pollingTask = new PeriodicTask<ArmarXPlotter>(this, & ArmarXPlotter::pollingExec, pollingInterval, false, "DataPollingTask");
            pollingTask->start();
            timer->start();
        }
    }



    void ArmarXPlotter::saveSettings(QSettings* settings)
    {
        ScopedLock lock(dataMutex);
        settings->setValue("updateInterval", updateInterval);
        settings->setValue("shownInterval", shownInterval);
        settings->setValue("pollingInterval", pollingInterval);
        settings->setValue("selectedChannels", selectedChannels);
        settings->setValue("autoScaleActive", ui.BTNAutoScale->isChecked());
    }

    void ArmarXPlotter::loadSettings(QSettings* settings)
    {
        ScopedLock lock(dataMutex);
        updateInterval = settings->value("updateInterval", 100).toInt();
        shownInterval = settings->value("shownInterval", 60).toInt();
        pollingInterval = settings->value("pollingInterval", 33).toInt();
        selectedChannels = settings->value("selectedChannels", QStringList()).toStringList();
        ui.BTNAutoScale->setChecked(settings->value("autoScaleActive", true).toBool());

        ARMARX_VERBOSE << "Settings loaded" << flush;
    }

    QwtPlotCurve* ArmarXPlotter::createCurve(const QString& label)
    {
        QwtPlotCurve* curve = new QwtPlotCurve(label);

        curve->setPen(QColor(Qt::GlobalColor((curves.size() + 7) % 15)));
        curve->setStyle(QwtPlotCurve::Lines);

        curve->attach(ui.qwtPlot);
        showCurve(curve, true);

        return curve;
    }

    void ArmarXPlotter::setupCurves()
    {
        {
            ScopedLock lock(dataMutex);

            for (int i = 0; i < selectedChannels.size(); i++)
            {
                ARMARX_VERBOSE << "Channel: " << selectedChannels.at(i).toStdString() << flush;
                DataFieldIdentifierPtr identifier = new DataFieldIdentifier(selectedChannels.at(i).toStdString());
                auto prx = getProxy<ObserverInterfacePrx>(identifier->observerName);

                VariantPtr var = VariantPtr::dynamicCast(prx->getDataField(identifier));

                if (VariantType::IsBasicType(var->getType()))
                {
                    QwtPlotCurve* curve = createCurve(selectedChannels.at(i));
                    curves[selectedChannels.at(i).toStdString()] = curve;
                }
                else
                {
                    auto id = identifier->getIdentifierStr();
                    ARMARX_IMPORTANT << id;
                    auto dict = JSONObject::ConvertToBasicVariantMap(json, var);

                    for (auto e : dict)
                    {
                        VariantTypeId type = e.second->getType();

                        if (type == VariantType::Double
                            || type == VariantType::Float
                            || type == VariantType::Int)
                        {
                            std::string key = id + "." + e.first;
                            ARMARX_INFO << key << ": " << *VariantPtr::dynamicCast(e.second);
                            QwtPlotCurve* curve = createCurve(QString::fromStdString(key));
                            curves[key] = curve;
                        }
                    }
                }
            }

            ui.qwtPlot->replot();
        }
        timer->start(updateInterval);

        if (pollingTask)
        {
            pollingTask->stop();
        }

        pollingTask = new PeriodicTask<ArmarXPlotter>(this, & ArmarXPlotter::pollingExec, pollingInterval, false, "DataPollingTask");
        pollingTask->start();
    }

    std::map<string, VariantPtr> ArmarXPlotter::getData(const QStringList& channels, GraphDataMap& dataMaptoAppend)
    {
        map< std::string, DataFieldIdentifierBaseList > channelsSplittedByObserver;
        foreach(QString channel, channels)
        {
            DataFieldIdentifierPtr identifier = new DataFieldIdentifier(channel.toStdString());
            channelsSplittedByObserver[identifier->getObserverName()].push_back(identifier);
            //            ARMARX_INFO << identifier;
        }
        std::map<std::string, VariantPtr> newData;

        // first clear to old entries
        auto now = IceUtil::Time::now();
        GraphDataMap::iterator itmap = dataMaptoAppend.begin();

        for (; itmap != dataMaptoAppend.end(); ++itmap)
        {
            std::vector<TimeData>& dataVec = itmap->second;
            int stepSize = dataVec.size() * 0.01;
            int thresholdIndex = -1;

            for (unsigned int i = 0; dataVec.size(); i += stepSize)
            {
                // only delete if entries are older than 2*showninterval
                // and delete then all entries that are older than showninterval.
                // otherwise it would delete items on every call, which would be very slow

                if ((now - dataVec[i].time).toMilliSecondsDouble() > shownInterval * 2 * 1000
                    || (thresholdIndex != -1 && (now - dataVec[i].time).toMilliSecondsDouble() > shownInterval * 1000)
                   )
                {
                    thresholdIndex = i;
                }
                else
                {
                    break;
                }
            }

            if (thresholdIndex != -1)
            {
                unsigned int offset = std::min((int)dataVec.size(), thresholdIndex);

                //                ARMARX_IMPORTANT << "Erasing " << offset << " fields";
                if (offset > dataVec.size())
                {
                    dataVec.clear();
                }
                else
                {
                    dataVec.erase(dataVec.begin(), dataVec.begin() + offset);
                }
            }

            //            ARMARX_IMPORTANT << deactivateSpam(5) << "size: " << dataVec.size();
        }

        // now get new data
        IceUtil::Time time = IceUtil::Time::now();
        map<string, DataFieldIdentifierBaseList >::iterator it = channelsSplittedByObserver.begin();

        try
        {
            for (; it != channelsSplittedByObserver.end(); ++it)
            {
                std::string observerName = it->first;

                if (proxyMap.find(observerName) == proxyMap.end())
                {
                    proxyMap[observerName] = getProxy<ObserverInterfacePrx>(observerName);
                }

                //            QDateTime time(QDateTime::currentDateTime());
                VariantBaseList variants = proxyMap[observerName]->getDataFields(it->second);

                //                ARMARX_IMPORTANT << "data from observer: " << observerName;
                for (unsigned int i = 0; i < variants.size(); ++i)
                {
                    //                    ARMARX_IMPORTANT << "Variant: " << VariantPtr::dynamicCast(variants[i]);
                    VariantPtr var = VariantPtr::dynamicCast(variants[i]);

                    if (VariantType::IsBasicType(var->getType()))
                    {
                        dataMaptoAppend[DataFieldIdentifierPtr::dynamicCast(it->second[i])->getIdentifierStr()].push_back(TimeData(time, var));
                        newData[DataFieldIdentifierPtr::dynamicCast(it->second[i])->getIdentifierStr()] = var;
                    }
                    else
                    {
                        auto id = DataFieldIdentifierPtr::dynamicCast(it->second[i])->getIdentifierStr();
                        //                        ARMARX_IMPORTANT << id;
                        auto dict = JSONObject::ConvertToBasicVariantMap(json, var);

                        for (const auto& e : dict)
                        {
                            std::string key = id + "." + e.first;
                            //                            ARMARX_INFO << key << ": " << *VariantPtr::dynamicCast(e.second);
                            dataMaptoAppend[key].push_back(TimeData(time, VariantPtr::dynamicCast(e.second)));
                            newData[key] = VariantPtr::dynamicCast(e.second);
                        }
                    }

                }

            }
        }
        catch (Ice::NotRegisteredException& e)
        {
            ARMARX_WARNING << deactivateSpam(3) << "Caught Ice::NotRegisteredException: " << e.what();
        }
        catch (exceptions::local::InvalidDataFieldException& e)
        {
            ARMARX_WARNING << deactivateSpam(5) << "Caught InvalidDataFieldException: " << e.what();
        }
        catch (exceptions::local::InvalidChannelException& e)
        {
            ARMARX_WARNING << deactivateSpam(5) << "Caught InvalidChannelException: " << e.what();
        }
        catch (...)
        {
            handleExceptions();
        }

        return newData;
    }

    void ArmarXPlotter::logToFile(const IceUtil::Time& time, const std::map<std::string, VariantPtr>& dataMaptoAppend)
    {

        if (logstream.is_open() && dataMaptoAppend.size() > 0)
        {
            logstream << (time - logStartTime).toMilliSecondsDouble();

            for (const auto& elem : dataMaptoAppend)
            {
                logstream  << "," /*<< elem.first << ","*/ << elem.second->getOutputValueOnly();
            }

            logstream  << std::endl;
        }
    }


}
