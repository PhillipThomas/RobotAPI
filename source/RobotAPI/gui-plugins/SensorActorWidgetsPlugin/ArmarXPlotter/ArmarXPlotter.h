/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef ARMARXPLOTTER_H
#define ARMARXPLOTTER_H

#include "ui_ArmarXPlotter.h"

// ArmarX
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/gui-plugins/ObserverPropertiesPlugin/ObserverItemModel.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/core/system/AbstractFactoryMethod.h>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

// QT
#include <QWidget>
#include <QDialog>
#include <QDateTime>

#include <vector>
#include <fstream>

//forward declarations
class QwtPlotCurve;

namespace armarx
{
    struct TimeData
    {
        TimeData(const IceUtil::Time& time, const VariantPtr& data): time(time), data(data) {}

        IceUtil::Time time;
        VariantPtr data;
    };

    typedef std::map< std::string, ObserverInterfacePrx> ProxyMap;
    typedef std::map< std::string, std::vector<TimeData> > GraphDataMap;

    class ArmarXPlotterDialog;
    /*!
     * \page RobotAPI-GuiPlugins-ArmarXPlotter ArmarXPlotter
     * \brief The plotter widget allows the user to plot any sensor channel.
     * \image html PlotterGUI.png
     * A sensor channel can be selected for plotting by clicking the wrench button at the bottom.
     * \ingroup RobotAPI-ArmarXGuiPlugins ArmarXGuiPlugins
      */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ArmarXPlotter:
        public ArmarXComponentWidgetController
    {
        Q_OBJECT
    public:


        std::map<std::string, QwtPlotCurve*> curves;
        Ui::ArmarXPlotter ui;
        QPointer<ArmarXPlotterDialog> dialog;
        QTimer* timer;
        ConditionHandlerInterfacePrx handler;

        // Configuration Parameters
        int updateInterval;
        int shownInterval;
        QStringList selectedChannels;
        QString loggingDir;

        explicit ArmarXPlotter();
        ~ArmarXPlotter();
        //inherited from ArmarXWidget
        virtual QString getWidgetName() const
        {
            return "Observers.Plotter";
        }
        void loadSettings(QSettings* settings);
        void saveSettings(QSettings* settings);
        //for AbstractFactoryMethod class


        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onExitComponent();



        /**
        * emits the closeRequest signal
        */
        void onCloseWidget(QCloseEvent* event);
        QwtPlotCurve* createCurve(const QString& label);
    signals:

    public slots:
        void ButtonAddSensorChannelClicked();
        void configDialog();
        void updateGraph();
        void showCurve(QwtPlotItem* item, bool on);
        void autoScale(bool toggled);
        void plottingPaused(bool toggled);
        void configDone();
        void toggleLogging(bool toggled);

    private slots:
        void setupCurves();
    protected:
        boost::shared_ptr<QSettings> settings;
    private:

        void pollingExec();
        QDateTime startUpTime;
        GraphDataMap dataMap;
        std::map< std::string, ObserverInterfacePrx> proxyMap;
        bool __plottingPaused;
        JSONObjectPtr json;


        std::map<std::string, VariantPtr> getData(const QStringList& channels, GraphDataMap& dataMaptoAppend);
        void logToFile(const IceUtil::Time& time, const std::map<std::string, VariantPtr>& dataMaptoAppend);

        Mutex dataMutex;
        PeriodicTask<ArmarXPlotter>::pointer_type pollingTask;
        int pollingInterval;
        std::ofstream logstream;
        IceUtil::Time logStartTime;
    };

}

#endif
