/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef ARMARXGUI_TCPMOVER_H
#define ARMARXGUI_TCPMOVER_H

/** ArmarX headers **/
#include "TCPMoverConfigDialog.h"
#include "ui_TCPMover.h"

// ArmarX includes
#include <RobotAPI/interface/units/TCPMoverUnitInterface.h>
#include <RobotAPI/interface/units/TCPControlUnit.h>
/** VirtualRobot headers **/
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/IK/DifferentialIK.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

// Qt includes
#include <QDialog>

#define KINEMATIC_UNIT_FILE_DEFAULT armarx::ArmarXDataPath::getHomePath()+std::string("Armar4/ARMAR_IV_simox.xml")
#define KINEMATIC_UNIT_NAME_DEFAULT "RobotKinematicUnit"


class QDialogButtonBox;

namespace armarx
{

    /*class TCPMoverConfigDialog : public QDialog
    {
        Q_OBJECT

    public:
        TCPMoverConfigDialog(QWidget *parent = 0);

        friend class TCPMover;
    protected:
        QGridLayout *layout;
        //QLabel *label;
        //QLineEdit *editTCPMoverUnitName;
        IceProxyFinderBase* proxyFinder;
        QDialogButtonBox *buttonBox;

        void setupUI(QWidget *parent);
    };*/


    /**
     * \page RobotAPI-GuiPlugins-TCPMover TCPMoverPlugin
     * @brief The TCPMover widget allows direct control of a TCP.
     *
     * \image html TCPMoverGUI.png
     */
    class TCPMover :
        public ArmarXComponentWidgetController
    {
        Q_OBJECT

    public:
        TCPMover();
        ~TCPMover();
        //inherited from ArmarXMdiWidget
        void loadSettings(QSettings* settings);
        void saveSettings(QSettings* settings);
        virtual QString getWidgetName() const
        {
            return "RobotControl.TCPMover";
        }
        virtual QIcon getWidgetIcon() const
        {
            return QIcon("://icons/games.ico");
        }
        virtual QIcon getWidgetCategoryIcon() const
        {
            return QIcon("://icons/games.ico");
        }
        QPointer<QDialog> getConfigDialog(QWidget* parent = 0);
        void configured();

        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onExitComponent();


    public slots:
        void moveUp();
        void moveDown();
        void moveZUp();
        void moveZDown();
        void moveLeft();
        void moveRight();
        void increaseAlpha();
        void decreaseAlpha();
        void increaseBeta();
        void decreaseBeta();
        void increaseGamma();
        void decreaseGamma();
        void stopMoving();
        void reset();
        void robotControl(bool request);
        void selectHand(int index);
    signals:

    protected:
        void execMove();

        void moveRelative(float x, float y, float z);
        std::map<std::string, std::vector<float> > tcpData;
        std::string selectedTCP;
        std::string refFrame;
        //        float velocities[2][3];
        //        float orientation[2][3];
        NameValueMap velocityMap;
        bool robotRequested;
        VirtualRobot::RobotNodeSetPtr tcpNodeSet;
        std::string tcpNodeSetName;
        VirtualRobot::DifferentialIKPtr ik;
        Ui::TCPMover ui;
        std::string kinematicUnitName;
        std::string kinematicUnitFile;
        std::string tcpMoverUnitName;
        VirtualRobot::RobotPtr robot;
        TCPControlUnitInterfacePrx tcpMoverUnitPrx;
        RobotStateComponentInterfacePrx robotStateComponentPrx;
        SharedRobotInterfacePrx robotPrx;
        QPointer<TCPMoverConfigDialog> configDialog;
    };
}

#endif
