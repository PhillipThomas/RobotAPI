/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARXGUI_PLUGINS_HANDUNITCONFIGDIALOG_H
#define _ARMARXGUI_PLUGINS_HANDUNITCONFIGDIALOG_H

#include <QDialog>

#include <ArmarXCore/core/IceManager.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

namespace Ui
{
    class TCPMoverConfigDialog;
}

namespace armarx
{
    class TCPMoverConfigDialog :
        public QDialog,
        virtual public ManagedIceObject
    {
        Q_OBJECT

    public:
        explicit TCPMoverConfigDialog(QWidget* parent = 0);
        ~TCPMoverConfigDialog();

    protected:
        // ManagedIceObject interface
        std::string getDefaultName() const
        {
            return "TCPMoverConfigDialog" + uuid;
        }
        void onInitComponent();
        void onConnectComponent();

    private:
        Ui::TCPMoverConfigDialog* ui;

        IceProxyFinderBase* proxyFinder;
        std::string uuid;

        friend class TCPMover;

    };
}

#endif
