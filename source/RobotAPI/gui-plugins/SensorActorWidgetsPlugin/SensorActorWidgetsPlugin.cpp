/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Gui::SensorActorWidgetsPlugin
* @author     ( at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "SensorActorWidgetsPlugin.h"

#include "ArmarXPlotter/ArmarXPlotter.h"
#include "ArmarXTCPMover/TCPMover.h"

namespace armarx
{

    SensorActorPlugin::SensorActorPlugin()
    {
        addWidget<TCPMover>();
        addWidget<ArmarXPlotter>();
    }

    Q_EXPORT_PLUGIN2(armarX_gui_SensorActorPlugin, SensorActorPlugin)
}
