/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RobotViewerConfigDialog.h"
#include "ui_RobotViewerConfigDialog.h"

#include <QTimer>
#include <QPushButton>
#include <QMessageBox>


#include <IceUtil/UUID.h>

#include <Gui/ArmarXGuiLib/utility/IceProxyFinder.h>

#include <RobotAPI/interface/core/RobotState.h>

using namespace armarx;

RobotViewerConfigDialog::RobotViewerConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::RobotViewerConfigDialog),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);

    connect(this->ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfig()));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
    proxyFinder = new IceProxyFinder<RobotStateComponentInterfacePrx>(this);
    proxyFinder->setSearchMask("RobotState*");
    ui->gridLayout->addWidget(proxyFinder, 2, 1, 1, 2);
}

RobotViewerConfigDialog::~RobotViewerConfigDialog()
{
    delete ui;
}

void RobotViewerConfigDialog::onInitComponent()
{
    proxyFinder->setIceManager(getIceManager());
}

void RobotViewerConfigDialog::onConnectComponent()
{

}

void RobotViewerConfigDialog::onExitComponent()
{
    QObject::disconnect();

}



void RobotViewerConfigDialog::verifyConfig()
{
    if (proxyFinder->getSelectedProxyName().trimmed().length() == 0)
    {
        QMessageBox::critical(this, "Invalid Configuration", "The proxy name must not be empty");
    }
    else
    {
        this->accept();
    }
}



