/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef RobotViewerCONFIGDIALOG_H
#define RobotViewerCONFIGDIALOG_H

#include <QDialog>
#include <QFileDialog>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <Gui/ArmarXGuiLib/utility/IceProxyFinder.h>

namespace Ui
{
    class RobotViewerConfigDialog;
}

namespace armarx
{
    class RobotViewerConfigDialog :
        public QDialog,
        virtual public ManagedIceObject
    {
        Q_OBJECT

    public:
        explicit RobotViewerConfigDialog(QWidget* parent = 0);
        ~RobotViewerConfigDialog();

        // inherited from ManagedIceObject
        std::string getDefaultName() const
        {
            return "RobotViewerConfigDialog" + uuid;
        }
        void onInitComponent();
        void onConnectComponent();
        void onExitComponent();

        void updateRobotViewerList();
    signals:

    public slots:
        void verifyConfig();
    private:

        IceProxyFinderBase* proxyFinder;
        Ui::RobotViewerConfigDialog* ui;
        std::string uuid;
        friend class RobotViewerWidgetController;
    };
}

#endif
