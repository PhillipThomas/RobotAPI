/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::RobotViewerGuiPlugin
* @author     Nikolaus Vahrenkamp
* @copyright  2015 KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#ifndef ARMARX_COMPONENT_ROBOTVIEWER_GUI_H
#define ARMARX_COMPONENT_ROBOTVIEWER_GUI_H

/* ArmarX headers */
#include <RobotAPI/gui-plugins/RobotViewerPlugin/ui_RobotViewerGuiPlugin.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/interface/core/RobotIK.h>

#include <Gui/ArmarXGuiLib/ArmarXGuiPlugin.h>
#include <Gui/ArmarXGuiLib/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/Component.h>

/* Qt headers */
#include <QtGui/QMainWindow>


#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Visualization/VisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <boost/shared_ptr.hpp>

#include <Inventor/sensors/SoTimerSensor.h>

namespace armarx
{
    typedef boost::shared_ptr<VirtualRobot::CoinVisualization> CoinVisualizationPtr;

    class RobotViewerConfigDialog;

    /**
      \class RobotViewerGuiPlugin
      \brief This plugin provides a generic widget showing a 3D model of the robot. The robot is articulated and moved according to the corresponding RemoteRobot.
      Further, DebugDrawer methods are available.
      \see RobotViewerWidget
      */
    class RobotViewerGuiPlugin :
        public ArmarXGuiPlugin
    {
    public:
        RobotViewerGuiPlugin();

        QString getPluginName()
        {
            return "RobotViewerGuiPlugin";
        }
    };

    /**
     \class RobotViewerWidget
     \brief This widget shows a 3D model of the robot. The robot is articulated and moved according to the corresponding RemoteRobot.
     Further, DebugDrawer methods are available.
     \image html RobotViewerGUI.png
     When you add the widget to the Gui, you need to specify the following parameters:

     Parameter Name   | Example Value     | Required?     | Description
     :----------------  | :-------------:   | :-------------- |:--------------------
     Proxy     | RobotStateComponent   | Yes | The name of the robot state component.

     \ingroup RobotAPI-ArmarXGuiPlugins ArmarXGuiPlugins
     \see RobotViewerGuiPlugin
     */
    class RobotViewerWidgetController :
        public ArmarXComponentWidgetController
    {
        Q_OBJECT
    public:

        RobotViewerWidgetController();
        virtual ~RobotViewerWidgetController() {}

        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onDisconnectComponent();
        virtual void onExitComponent();

        // inherited of ArmarXWidget
        virtual QString getWidgetName() const
        {
            return "RobotControl.RobotIK";
        }
        QPointer<QDialog> getConfigDialog(QWidget* parent = 0);
        virtual void loadSettings(QSettings* settings);
        virtual void saveSettings(QSettings* settings);
        void configured();

        SoNode* getScene();

        // overwrite setMutex, so that we can inform the debugdrawer
        virtual void setMutex3D(boost::shared_ptr<boost::recursive_mutex> mutex3D);

    signals:

        void robotStatusUpdated();

    public slots:

        void updateRobotVisu();
        void solveIK();



    protected:
        void connectSlots();


        Ui::RobotViewerGuiPlugin ui;

        bool verbose;


        RobotStateComponentInterfacePrx robotStateComponentPrx;
        KinematicUnitInterfacePrx kinematicUnitInterfacePrx;
        RobotIKInterfacePrx robotIKPrx;

        VirtualRobot::RobotPtr robot;

        SoSeparator* robotVisu;
        SoSeparator* rootVisu;

        std::string robotStateComponentName;
        std::string kinematicUnitComponentName;
        std::string robotIKName;

        SoTimerSensor* timerSensor;

        SoSeparator* debugLayerVisu;
        armarx::DebugDrawerComponentPtr debugDrawer;

        static void timerCB(void* data, SoSensor* sensor);
    protected slots:
        void showVisuLayers(bool show);
        void showRoot(bool show);
        void showRobot(bool show);
    private:

        // init stuff
        VirtualRobot::RobotPtr loadRobotFile(std::string fileName);

        CoinVisualizationPtr getCoinVisualization(VirtualRobot::RobotPtr robot);


        QPointer<QWidget> __widget;
        QPointer<RobotViewerConfigDialog> dialog;

    };
    typedef boost::shared_ptr<RobotViewerWidgetController> RobotViewerGuiPluginPtr;
}

#endif
