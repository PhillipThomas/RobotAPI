armarx_set_target("RobotIKGuiPlugin")

find_package(Qt4 COMPONENTS QtCore QtGui QtOpenGL QUIET)

find_package(Eigen3 QUIET)

# VirtualRobot (adds dependencies to Coin3D and SoQt)
find_package(Simox ${ArmarX_Simox_VERSION} QUIET)
#find_package(ArmarXGui QUIET)

armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

armarx_build_if(QT_FOUND "Qt not available")
armarx_build_if(QT_QTOPENGL_FOUND "QtOpenGL not available")
armarx_build_if(Eigen3_FOUND "Eigen3 not available")
armarx_build_if(Simox_FOUND "VirtualRobot not available")

include(${QT_USE_FILE})

include_directories(${Eigen3_INCLUDE_DIR})
include_directories(${Simox_INCLUDE_DIRS})


set(SOURCES
    RobotIKGuiPlugin.cpp
    RobotIKConfigDialog.cpp
    RobotViewer.cpp
    RobotViewerWidget.cpp
)
set(HEADERS
    RobotIKGuiPlugin.h
    RobotIKConfigDialog.h
    RobotViewer.h
    RobotViewerWidget.h
)

set(GUI_MOC_HDRS
    RobotIKGuiPlugin.h
    RobotIKConfigDialog.h
    RobotViewer.h
    RobotViewerWidget.h
)

set(GUI_UIS
    RobotIKGuiPlugin.ui
    RobotIKConfigDialog.ui
)

set(COMPONENT_LIBS RobotAPIUnits DebugDrawer ${Simox_LIBRARIES})




if (ArmarXGui_FOUND)
	armarx_gui_library(RobotIKGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}" )
endif()
