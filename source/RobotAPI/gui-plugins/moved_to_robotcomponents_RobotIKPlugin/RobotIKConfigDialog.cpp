/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Philipp Schmidt
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

//Configuration dialog
#include "RobotIKConfigDialog.h"
#include "ui_RobotIKConfigDialog.h"

//RobotAPI includes for different Components
#include <RobotAPI/interface/core/RobotIK.h>
#include <RobotAPI/interface/core/RobotState.h>

//Qt includes
#include <QPushButton>
#include <QMessageBox>

//Ice includes
#include <IceUtil/UUID.h>

armarx::RobotIKConfigDialog::RobotIKConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::RobotIKConfigDialog),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);

    //Connect OK Button to the event checking the input parameters
    connect(this->ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfiguration()));

    //Set OK button as default for convenience
    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);

    //Initialize proxyFinders for every component
    robotStateComponentProxyFinder = new IceProxyFinder<RobotStateComponentInterfacePrx>(this);
    robotStateComponentProxyFinder->setSearchMask("RobotState*");
    ui->gridLayout->addWidget(robotStateComponentProxyFinder, 1, 1);

    kinematicUnitComponentProxyFinder = new IceProxyFinder<KinematicUnitInterfacePrx>(this);
    kinematicUnitComponentProxyFinder->setSearchMask("Armar3Kinematic*");
    ui->gridLayout->addWidget(kinematicUnitComponentProxyFinder, 2, 1);

    robotIKComponentProxyFinder = new IceProxyFinder<RobotIKInterfacePrx>(this);
    robotIKComponentProxyFinder->setSearchMask("RobotIK*");
    ui->gridLayout->addWidget(robotIKComponentProxyFinder, 3, 1);
}

armarx::RobotIKConfigDialog::~RobotIKConfigDialog()
{
    delete ui;
}

void armarx::RobotIKConfigDialog::onInitComponent()
{
    robotStateComponentProxyFinder->setIceManager(this->getIceManager());
    kinematicUnitComponentProxyFinder->setIceManager(this->getIceManager());
    robotIKComponentProxyFinder->setIceManager(this->getIceManager());
}

void armarx::RobotIKConfigDialog::onConnectComponent()
{

}

void armarx::RobotIKConfigDialog::onExitComponent()
{
    QObject::disconnect();
}

void armarx::RobotIKConfigDialog::verifyConfiguration()
{
    if (robotStateComponentProxyFinder->getSelectedProxyName().trimmed().length() == 0
        || kinematicUnitComponentProxyFinder->getSelectedProxyName().trimmed().length() == 0
        || robotIKComponentProxyFinder->getSelectedProxyName().trimmed().length() == 0)
    {
        QMessageBox::critical(this, "Invalid Configuration", "The proxy name must not be empty");
    }
    else
    {
        this->accept();
    }
}
