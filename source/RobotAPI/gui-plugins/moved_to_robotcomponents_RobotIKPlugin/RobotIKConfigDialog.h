/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Philipp Schmidt
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef RobotIKCONFIGDIALOG_H
#define RobotIKCONFIGDIALOG_H

//ArmarX includes
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

//Qt includes
#include <QDialog>

namespace Ui
{
    class RobotIKConfigDialog;
}

namespace armarx
{
    class RobotIKConfigDialog : public QDialog, virtual public ManagedIceObject
    {
        Q_OBJECT
        friend class RobotIKWidgetController;

    public:
        explicit RobotIKConfigDialog(QWidget* parent = 0);
        ~RobotIKConfigDialog();

        // inherited from ManagedIceObject
        std::string getDefaultName() const
        {
            return "RobotIKConfigDialog" + uuid;
        }
        void onInitComponent();
        void onConnectComponent();
        void onExitComponent();

    public slots:
        void verifyConfiguration();

    private:
        IceProxyFinderBase* robotStateComponentProxyFinder;
        IceProxyFinderBase* kinematicUnitComponentProxyFinder;
        IceProxyFinderBase* robotIKComponentProxyFinder;

        Ui::RobotIKConfigDialog* ui;
        std::string uuid;
    };
}

#endif
