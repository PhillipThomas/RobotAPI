/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Philipp Schmidt
* @date       2015
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#ifndef RobotIKGUIPLUGIN_H
#define RobotIKGUIPLUGIN_H

//Gui
#include "RobotIKConfigDialog.h"
#include "ui_RobotIKGuiPlugin.h"

//ArmarX includes
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <RobotAPI/interface/core/RobotIK.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

//Qt includes
#include <QObject>

//VirtualRobot includes
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

//Inventor includes
#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/manips/SoTransformerManip.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoMaterial.h>

namespace armarx
{
    class RobotIKConfigDialog;

    class RobotIKGuiPlugin : public ArmarXGuiPlugin
    {
    public:
        RobotIKGuiPlugin();

        QString getPluginName()
        {
            return "RobotIKGuiPlugin";
        }
    };

    class RobotIKWidgetController : public ArmarXComponentWidgetController
    {
        Q_OBJECT

    public:
        RobotIKWidgetController();
        virtual ~RobotIKWidgetController() {}

        // inherited from Component
        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onDisconnectComponent();
        virtual void onExitComponent();

        // inherited of ArmarXWidget
        virtual QString getWidgetName() const
        {
            return "RobotControl.RobotIK";
        }
        QPointer<QDialog> getConfigDialog(QWidget* parent = 0);
        virtual void loadSettings(QSettings* settings);
        virtual void saveSettings(QSettings* settings);
        void configured();

    public slots:
        void solveIK();
        void kinematicChainChanged(const QString& arg1);
        void caertesianSelectionChanged(const QString& arg1);
        void resetManip();

    protected:
        RobotStateComponentInterfacePrx robotStateComponentPrx;
        KinematicUnitInterfacePrx kinematicUnitInterfacePrx;
        RobotIKInterfacePrx robotIKPrx;

        Ui::RobotIKGuiPlugin ui;

    private:
        void connectSlots();
        std::string robotStateComponentName;
        std::string kinematicUnitComponentName;
        std::string robotIKComponentName;

        QPointer<RobotIKConfigDialog> dialog;

        VirtualRobot::RobotPtr robot;
        StringList getIncludePaths();
        VirtualRobot::RobotPtr loadRobot(StringList includePaths);

        SoSeparator* manipSeparator;
        SoSeparator* currentSeparator;

        SoTransformerManip* tcpManip;
        static void manipFinishCallback(void* data, SoDragger* manip);
        SoTransform* tcpManipTransform;
        SoMaterial* tcpManipColor;
        SoSphere* tcpManipSphere;

        SoTransform* tcpCurrentTransform;
        SoMaterial* tcpCurrentColor;
        SoSphere* tcpCurrentSphere;

        SoTimerSensor* robotUpdateSensor;
        static void robotUpdateTimerCB(void* data, SoSensor* sensor);

        SoTimerSensor* textFieldUpdateSensor;
        static void textFieldUpdateTimerCB(void* data, SoSensor* sensor);

        ExtendedIKResult getIKSolution();

        CartesianSelection convertOption(std::string option);

        bool startUpCameraPositioningFlag;

    };
    typedef boost::shared_ptr<RobotIKWidgetController> RobotIKGuiPluginPtr;
    typedef boost::shared_ptr<VirtualRobot::CoinVisualization> CoinVisualizationPtr;
}

#endif
