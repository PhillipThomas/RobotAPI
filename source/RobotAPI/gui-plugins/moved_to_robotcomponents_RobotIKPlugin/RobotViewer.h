#ifndef __RobotViewer_h__
#define __RobotViewer_h__

/* Coin headers */
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

namespace armarx
{
    class RobotViewer : public SoQtExaminerViewer
    {
    public:
        RobotViewer(QWidget* widget);
        ~RobotViewer();
        SoSeparator* getRootNode();
        void cameraViewAll();
    private:
        virtual SbBool processSoEvent(const SoEvent* const event);
        SoSeparator* sceneRootNode;
        SoSeparator* contentRootNode;
        SoPerspectiveCamera* camera;
    };
}

#endif
