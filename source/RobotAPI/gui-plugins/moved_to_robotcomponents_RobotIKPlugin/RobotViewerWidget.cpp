#include "RobotViewerWidget.h"

#include <QGridLayout>

armarx::RobotViewerWidget::RobotViewerWidget(QWidget* parent) : QWidget(parent)
{
    this->setContentsMargins(1, 1, 1, 1);

    QGridLayout* grid = new QGridLayout();
    grid->setContentsMargins(0, 0, 0, 0);
    this->setLayout(grid);
    this->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    QWidget* view1 = new QWidget(this);
    view1->setMinimumSize(100, 100);
    view1->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    viewer.reset(new RobotViewer(view1));
    viewer->show();

    grid->addWidget(view1, 0, 0, 1, 2);
}


armarx::RobotViewerWidget::~RobotViewerWidget()
{

}

RobotViewerPtr armarx::RobotViewerWidget::getRobotViewer()
{
    return viewer;
}
