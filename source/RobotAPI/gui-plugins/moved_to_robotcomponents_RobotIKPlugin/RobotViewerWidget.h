#ifndef RobotViewerWidget_h
#define RobotViewerWidget_h

/* Qt headers */
#include <QWidget>

/* boost headers */
#include <boost/smart_ptr/shared_ptr.hpp>

#include "RobotViewer.h"

typedef boost::shared_ptr<armarx::RobotViewer> RobotViewerPtr;

namespace armarx
{
    class RobotViewerWidget : public QWidget
    {
        Q_OBJECT

    public:
        /**
        * Constructor.
        * Constructs a robot viewer widget.
        * Expects a controller::ControllerPtr.
        *
        * @param    control     shared pointer to controller::controller
        * @param    parent      parent widget
        *
        */
        explicit RobotViewerWidget(QWidget* parent = 0);

        /**
        * Destructor.
        *
        */
        ~RobotViewerWidget();

        RobotViewerPtr getRobotViewer();

    private:
        RobotViewerPtr viewer;
    };
}

#endif
