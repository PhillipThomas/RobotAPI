###
### CMakeLists.txt file for ArmarX Interfaces
###

set(ROBOTAPI_INTERFACE_DEPEND ArmarXCore)

set(SLICE_FILES
    hardware/BusInspectionInterface.ice

    observers/KinematicUnitObserverInterface.ice
    observers/PlatformUnitObserverInterface.ice
    observers/ObserverFilters.ice

    core/PoseBase.ice
    core/LinkedPoseBase.ice
    core/FramedPoseBase.ice
    core/RobotState.ice
    core/RobotStateObserverInterface.ice

    selflocalisation/SelfLocalisationProcess.ice

    speech/SpeechInterface.ice

    units/ForceTorqueUnit.ice
    units/InertialMeasurementUnit.ice
    units/HandUnitInterface.ice
    units/HapticUnit.ice
    units/WeissHapticUnit.ice
    units/HeadIKUnit.ice
    units/KinematicUnitInterface.ice
    units/PlatformUnitInterface.ice
    units/TCPControlUnit.ice
    units/TCPMoverUnitInterface.ice
    units/UnitInterface.ice
    units/ATINetFTUnit.ice

    visualization/DebugDrawerInterface.ice
)
    #core/RobotIK.ice

# generate the interface library
armarx_interfaces_generate_library(RobotAPI "${ROBOTAPI_INTERFACE_DEPEND}")
