/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Stefan Ulbrich (ulbrich at kit dot edu), Nikolaus Vahrenkamp
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_ROBOTAPI_ROBOTSTATE_FRAMED_POSE_SLICE_
#define _ARMARX_ROBOTAPI_ROBOTSTATE_FRAMED_POSE_SLICE_

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Filters.ice>
#include <RobotAPI/interface/core/PoseBase.ice>

module armarx
{
	/**
	* [FramedDirectionBase] defines a vector with regard to an agent and a frame within the agent.
	* If agent is empty and frame is set to armarx::GlobalFrame, vector is defined in global frame.
	* @param frame Name of frame.
	* @param frame Name of agent.
	*/
    ["cpp:virtual"]
    class FramedDirectionBase extends Vector3Base
    {
        string frame;
        string agent;
    };

	/**
	* [FramedPoseBase] defines a pose with regard to an agent and a frame within the agent.
	* If agent is empty and frame is set to armarx::GlobalFrame, pose is defined in global frame.
	* @param frame Name of frame.
	* @param frame Name of agent.
	*/
    ["cpp:virtual"]
    class FramedPoseBase extends PoseBase
    {
        string frame;
        string agent;
    };
    
	/**
	* [FramedPositionBase] defines a position with regard to an agent and a frame within the agent.
	* If agent is empty and frame is set to armarx::GlobalFrame, position is defined in global frame.
	* @param frame Name of frame.
	* @param frame Name of agent.
	*/
    ["cpp:virtual"]
    class FramedPositionBase extends Vector3Base
    {
        string frame;
        string agent;
    };

	/**
	* [FramedOrientationBase] defines an orientation in quaternion with regard to an agent and a frame within the agent.
	* If agent is empty and frame is set to armarx::GlobalFrame, orientation in quaternion is defined in global frame.
	* @param frame Name of frame.
	* @param frame Name of agent.
	*/
   ["cpp:virtual"]
    class FramedOrientationBase extends QuaternionBase
    {
        string frame;
        string agent;
    };
	/**
	* FramedDirectionMap is a map of FramedDirectionBases and corresponding agent nodes identified by name.
	* @see FramedDirectionBase
	*/
    dictionary<string, FramedDirectionBase> FramedDirectionMap;
    /**
	* FramedPositionBase is a map of FramedPositionBases and corresponding agent nodes identified by name.
	* @see FramedPositionBase
	*/
    dictionary<string, FramedPositionBase> FramedPositionMap;
    
	/**
	* PoseMedianFilterBase filters poses with a median filter.
	*/
    ["cpp:virtual"]
    class PoseMedianFilterBase extends MedianFilterBase
    {

    };

};

#endif
