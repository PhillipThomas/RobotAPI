/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
* @copyright  2013 H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_ROBOTAPI_ROBOTSTATE_LINKEDPOSE_SLICE_
#define _ARMARX_ROBOTAPI_ROBOTSTATE_LINKEDPOSE_SLICE_

#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <RobotAPI/interface/core/RobotState.ice>

module armarx
{
	/**
	* [LinkedPoseBase] 
	*/
    ["cpp:virtual"]
    class LinkedPoseBase extends FramedPoseBase
    {
        SharedRobotInterface* referenceRobot;
        /**
        * changeFrame describes pose on new frame.
        * @param newFrame Name of new frame node.
        */
        void changeFrame(string newFrame);
    };
    
	/**
	* [LinkedDirectionBase] 
	*/
    ["cpp:virtual"]
    class LinkedDirectionBase extends FramedDirectionBase
    {
        SharedRobotInterface* referenceRobot;
        /**
        * changeFrame describes direction on new frame.
        * @param newFrame Name of new frame node.
        */
        void changeFrame(string newFrame);
    };
};

#endif
