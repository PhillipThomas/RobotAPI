#ifndef ARMARX_ROBOTSTATE_ROBOTSTATE_SLICE
#define ARMARX_ROBOTSTATE_ROBOTSTATE_SLICE

#include <RobotAPI/interface/units/KinematicUnitInterface.ice>
#include <RobotAPI/interface/units/PlatformUnitInterface.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <ArmarXCore/interface/observers/Timestamp.ice>

module armarx
{
    /**
     * Available VirtualRobot joint types.
     */
    enum JointType
    {
        ePrismatic,
        eFixed,
        eRevolute
    };

    /**
     * Distributed reference counting interface for the SharedRobotInterace
     * instances.
     */
    interface SharedObjectInterface
    {
        void ref();
        void unref();
    };

    sequence<string> NameList;

    /**
     * The data stored in a VirtualRobot::RobotNodeSet but accessable over Ice.
     */
    class RobotNodeSetInfo
    {
        NameList names;
        string name;
        string tcpName;
        string rootName;
    };

    /**
     * The SharedRobotNodeInterface provides access to a limited amount of
     * VirtualRobot::RobotNode methods over the Ice network.
     */
    interface SharedRobotNodeInterface extends SharedObjectInterface
    {

        ["cpp:const"] idempotent
        float getJointValue();
        ["cpp:const"] idempotent
        string getName();

		["cpp:const"] idempotent
        PoseBase getLocalTransformation();
        
        ["cpp:const"] idempotent
        FramedPoseBase getGlobalPose();

        ["cpp:const"] idempotent
        FramedPoseBase getPoseInRootFrame();

        ["cpp:const"] idempotent
        JointType getType();
        ["cpp:const"] idempotent
        Vector3Base getJointTranslationDirection();
        ["cpp:const"] idempotent
        Vector3Base getJointRotationAxis();

        ["cpp:const"] idempotent
        bool hasChild(string name, bool recursive);
        ["cpp:const"] idempotent
        string getParent();
        ["cpp:const"] idempotent
        NameList getChildren();
        ["cpp:const"] idempotent
        NameList getAllParents(string name);

        ["cpp:const"] idempotent
        float getJointValueOffest();
        ["cpp:const"] idempotent
        float getJointLimitHigh();
        ["cpp:const"] idempotent
        float getJointLimitLow();

    };


    /**
     * The SharedRobotInterface provides access to a limited amount of
     * VirtualRobot::Robot methods over the Ice network.
     */
    interface SharedRobotInterface extends SharedObjectInterface
    {
        SharedRobotNodeInterface* getRobotNode(string name);
        SharedRobotNodeInterface* getRootNode();
        bool hasRobotNode(string name);
        NameList getRobotNodes();
        /**
          * @return The timestamp of the last joint value update (not the global pose update).
          */
        ["cpp:const"] idempotent
        TimestampBase getTimestamp();

        RobotNodeSetInfo getRobotNodeSet(string name);
        NameList getRobotNodeSets();
        bool hasRobotNodeSet(string name);

        string getName();
        string getType();
        void setGlobalPose(PoseBase globalPose);
        PoseBase getGlobalPose();
        NameValueMap getConfig();
    };


    /**
     * The interface used by the RobotStateComponent which allows creating
     * snapshots of a robot configuration or retrieving a proxy to a constantly
     * updating synchronized robot.
     */
    interface RobotStateComponentInterface extends KinematicUnitListener
    {
        /**
         * @return proxy to the shared robot which constantly updates all joint values
         */
        ["cpp:const"]
        idempotent
        SharedRobotInterface* getSynchronizedRobot() throws NotInitializedException;

        /**
         * @return proxy to a copy of the shared robot with non updating joint values
         */
        SharedRobotInterface* getRobotSnapshot(string time) throws NotInitializedException;
        
        /**
         * @return the robot xml filename as specified in the configuration
         */
        ["cpp:const"]
        idempotent
        string getRobotFilename();

        /**
         * @return All dependent packages, which might contain a robot file.
         */
        ["cpp:const"]
        idempotent
        StringList getArmarXPackages();

         /**
          * @return The name of the robot represented by this component. Same as
          * getSynchronizedRobot()->getName()
          *
          */
        ["cpp:const"]
        idempotent string getRobotName() throws NotInitializedException;
    };
};

#endif
