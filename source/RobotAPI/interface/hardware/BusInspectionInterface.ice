/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Core
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @author     Matthias Hadlich (matthias dot hadlich at student dot kit dot edu)
 * @copyright  2014 Peter Kaiser 
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_BUSINSPECTIONINTERFACE_SLICE_
#define _ARMARX_CORE_BUSINSPECTIONINTERFACE_SLICE_

#include <RobotAPI/interface/units/KinematicUnitInterface.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
    exception UnknownBusException extends UserException
    {
        string busName;
    };

    exception UnknownDeviceException extends UserException
    {
        string deviceName;
    };
	
    exception UnknownCommandException extends UserException
    {
        string commandName;
    };


    struct DeviceStatus
    {
        OperationStatus operation;
        ErrorStatus error;

        bool enabled;
        bool emergencyStop;
        string msg;
    };

    sequence<string> BusNames;
    sequence<string> DeviceNames;

    struct DeviceInformation
    {
        int id;
        string type;
        string node;
        int heartBeatErrorCounter;
	string firmwareBuildDate;

        DeviceStatus status;
    };

    struct BusInformation
    {
        int baudrate;
        int amountOfErrors;
    };

    enum BasicOperation { eStart, eStop, eReset };
	

    interface BusInspectionInterface
    {
        BusNames getConfiguredBusses();
        BusInformation getBusInformation(string bus) throws UnknownBusException;
        DeviceNames getDevicesOnBus(string bus) throws UnknownBusException;
        DeviceInformation getDeviceInformation(string device) throws UnknownDeviceException;
        int performDeviceOperation(BasicOperation operation, int device);
	int performBusOperation(BasicOperation operation, string bus);
	bool isInRealTimeMode();
        string sendCommand(string command, int device) throws UnknownCommandException;	
    };

    interface BusInspectionListener
    {

    };
};

#endif
