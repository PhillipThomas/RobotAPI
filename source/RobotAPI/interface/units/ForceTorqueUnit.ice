/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Mirko Waechter <waechter at kit dot edu>
 * @copyright  2013 Mirko Waechter
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 **/

#ifndef _ARMARX_ROBOTAPI_UNITS_FORCETORQUE_SLICE_
#define _ARMARX_ROBOTAPI_UNITS_FORCETORQUE_SLICE_

#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/core/RobotState.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>

module armarx
{    
     /**
     * Implements an interface to a ForceTorqueUnit.
     **/
    interface ForceTorqueUnitInterface extends armarx::SensorActorUnitInterface
    {
	/** 
	 * setOffset allows to define offset forces and torques which are subtracted from the measured values.
	 * @param forceOffsets Offset forces in x, y, and z.
	 * @param torqueOffsets Offset torques in x, y, and z.
	 **/
        void setOffset(FramedDirectionBase forceOffsets, FramedDirectionBase torqueOffsets);
     /** 
	 * setToNull allows to reset the ForceTorqueUnit by setting the measured values to zero.
	 **/
        void setToNull();
    };
	/**
     * Implements an interface to a ForceTorqueUnitListener
     **/
    interface ForceTorqueUnitListener
    {
		/**
			* reportSensorValues reports measured forces and torques from the ForceTorqueUnit to ForceTorqueObserver.
			* @param sensorNodeName Name of force/torque sensor.
			* @param forces Measured forces.
			* @param torques Measured torques.
		**/
        void reportSensorValues(string sensorNodeName, FramedDirectionBase forces, FramedDirectionBase torques);
    };
	/**
     * Implements an interface to a ForceTorqueUnitObserver. Provides topics on measured forces and torques which can be subscribed.
     **/
    interface ForceTorqueUnitObserverInterface extends ObserverInterface, ForceTorqueUnitListener
    {
		/**
			* getForceDatafield returns a reference on the force topic.
			* @param nodeName Name of force sensor topic.
		**/
        DatafieldRefBase getForceDatafield(string nodeName) throws UserException;
        /** 
			* getTorqueDatafield returns a reference on the torque topic.
			* @param nodeName Name of torque sensor topic.
		**/
        DatafieldRefBase getTorqueDatafield(string nodeName) throws UserException;
		/**
			* createNulledDatafield resets the force/torque topic.
			* @param forceTorqueDatafieldRef Reference of torque/torque sensor topic.
		**/
        DatafieldRefBase createNulledDatafield(DatafieldRefBase forceTorqueDatafieldRef);
    };
};

#endif
