/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Core
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @copyright  2013 Manfred Kroehnert
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_UNITS_HANDUNIT_SLICE_
#define _ARMARX_CORE_UNITS_HANDUNIT_SLICE_

#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/units/KinematicUnitInterface.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/VariantContainers.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>


module armarx
{
	/**
     * Implements an interface to a HandUnit.
     */
    interface HandUnitInterface extends SensorActorUnitInterface
    {
		/**
         * setShape sets the name of a known hand shape for the hand unit.
         * @param shapeName Name of the hand shape.
         */
        void setShape(string shapeName);
        /**
         * getShapeNames returns a list of names of hand shapes known to the hand unit.
         * @return List of known hand shape names.
         */
        SingleTypeVariantListBase getShapeNames();
        NameValueMap getShapeJointValues(string shapeName);
        NameValueMap getCurrentJointValues();
        string getHandName();


        /**
         * setObjectGrasped informs the hand unit that an object has been successfully grasped.
         * @param objectName Name of the object
         * E.g. a state that verifies if a grasp was successful can call this routine.
         */
        void setObjectGrasped(string objectName);

        /**
         * setObjectReleased informs the hand unit that an object has been released.
         * @param objectName Name of the object
         * E.g. a state that verifies if a grasp was successful can call this routine.
         */
        void setObjectReleased(string objectName);
		/**
         * setJointAngles controls the joints of the hand unit.
         * @param targetJointAngles Map of joint names and corresponding joint angle values.
         */
        void setJointAngles(NameValueMap targetJointAngles);
    };
	/**
     * Implements an interface to a HandUnitListener.
     */
    interface HandUnitListener
    {
		/**
		* reportNewHandShapeName reports the current hand shape name.
		* @param handName Name of the hand to be controlled.
		* @param handName Name of the current hand shape name.
		**/
        void reportHandShaped(string handName, string handShapeName);
        /**
		* reportNewHandShapeName reports the target hand shape name.
		* @param handName Name of the hand to be controlled.
		* @param handName Name of the target hand shape name.
		**/
        void reportNewHandShapeName(string handName, string handShapeName);
        /**
		* reportJointAngles reports sensed joint angle values in the hand joints.
		* @param actualJointAngles Map of joint names and corresponding sensed joint angle values.
		**/
        void reportJointAngles(NameValueMap actualJointAngles);
        /**
		* reportJointPressures reports sensed pressure values in the hand joints.
		* @param actualJointPressures Map of joint names and corresponding sensed pressure values.
		**/
        void reportJointPressures(NameValueMap actualJointPressures);

    };
	/**
     * Implements an interface to a HandUnitObserver.
     */
    interface HandUnitObserverInterface extends ObserverInterface, HandUnitListener
    {
    };

};

#endif
