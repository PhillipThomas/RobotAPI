/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     David Schiebener <schiebener at kit dot edu>
 * @copyright  2014 David Schiebener
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ROBOTAPI_UNITS_HEADIK_SLICE_
#define _ARMARX_ROBOTAPI_UNITS_HEADIK_SLICE_

#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/core/RobotState.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>

module armarx
{
	/**
	* Implements an interface to a HeadIKUnit.
	**/
    interface HeadIKUnitInterface extends armarx::SensorActorUnitInterface
    {
		/**
		* setCycleTime allows to set the cycle time with which the head IK is solved and the head is controlled within a periodic task.
		* @param milliseconds Cycle time in milliseconds.
		**/
        void setCycleTime(int milliseconds);
        /**
		* setHeadTarget allows to set a new target position for the head.
		* @param robotNodeSetName Name of kinematic chain/nodeset with head as TCP.
		* @param targetPosition in x, y, and z.
		**/
        void setHeadTarget(string robotNodeSetName, FramedPositionBase targetPosition);
    };

};

#endif
