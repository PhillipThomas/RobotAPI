/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Core
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @copyright  2013 Manfred Kroehnert
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_UNITS_PLATFORMUNIT_SLICE_
#define _ARMARX_CORE_UNITS_PLATFORMUNIT_SLICE_

#include <RobotAPI/interface/units/UnitInterface.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{	
	/**
	* Implements an interface to an PlatformUnit.
	**/
    interface PlatformUnitInterface extends SensorActorUnitInterface
    {
		/**
		* moveTo moves the platform to a global pose specified by:
		* @param targetPlatformPositionX Global x-coordinate of target position. 
		* @param targetPlatformPositionY Global y-coordinate of target position.
		* @param targetPlatformRotation Global orientation of platform, mostly yaw angle (rotation around z-axis). 
		* @param postionalAccuracy Platform stops translating if distance to target position gets lower than this threshhold.
		* @param orientationalAccuracy Platform stops rotating if distance from current to target orientation gets lower than this threshhold.
		**/
        void moveTo(float targetPlatformPositionX, float targetPlatformPositionY, float targetPlatformRotation, float positionalAccuracy, float orientationalAccuracy);
        /**
		* move moves the platform with given velocities.
		* @param targetPlatformVelocityX x-coordinate of target velocity defined in platform root. 
		* @param targetPlatformVelocityY y-coordinate of target velocity defined in platform root. 
		* @param targetPlatformVelocityRotation Target orientational velocity defined in platform root. 
		**/
        void move(float targetPlatformVelocityX, float targetPlatformVelocityY, float targetPlatformVelocityRotation);
        /**
		* moveRelative moves to a pose defined in platform coordinates.
		* @param targetPlatformOffsetX x-coordinate of target position defined in platform root. 
		* @param targetPlatformOffsetY y-coordinate of target position defined in platform root.
		* @param targetPlatformOffsetRotation Target orientation of platform defined in platform root. 
		* @param postionalAccuracy Platform stops translating if distance to target position gets lower than this threshhold.
		* @param orientationalAccuracy Platform stops rotating if distance from current to target orientation gets lower than this threshhold.
		**/
        void moveRelative(float targetPlatformOffsetX, float targetPlatformOffsetY, float targetPlatformOffsetRotation, float positionalAccuracy, float orientationalAccuracy);
        /**
		* setMaxVelocities allows to specify max velocities in translation and orientation.
		* @param positionalVelocity Max translation velocity. 
		* @param orientationalVelocity Max orientational velocity. 
		**/
        void setMaxVelocities(float positionalVelocity, float orientationalVelocity);
        /**
		* stopPlatform stops the movements of the platform.
		**/
        void stopPlatform();
    };
	/**
	* Implements an interface to an PlatformUnitListener.
	**/
    interface PlatformUnitListener
    {
		/**
		* reportPlatformPose reports current platform pose.
		* @param currentPlatformPositionX Global x-coordinate of current platform position.
		* @param currentPlatformPositionY Global y-coordinate of current platform position.
		* @param currentPlatformRotation Current global orientation of platform.
		**/
        void reportPlatformPose(float currentPlatformPositionX, float currentPlatformPositionY, float currentPlatformRotation);
        /**
		* reportNewTargetPose reports target platform pose.
		* @param newPlatformPositionX Global x-coordinate of target platform position.
		* @param newPlatformPositionY Global y-coordinate of target platform position.
		* @param newPlatformRotation Target global orientation of platform.
		**/
        void reportNewTargetPose(float newPlatformPositionX, float newPlatformPositionY, float newPlatformRotation);
        /**
		* reportPlatformVelocity reports current platform velocities.
		* @param currentPlatformVelocityX x-coordinate of current velocity defined in platform root. 
		* @param currentPlatformVelocityY y-coordinate of current velocity defined in platform root. 
		* @param currentPlatformVelocityRotation Current orientational velocity defined in platform root. 
		**/
        void reportPlatformVelocity(float currentPlatformVelocityX, float currentPlatformVelocityY, float currentPlatformVelocityRotation);
    };

};

#endif
