/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Peter Kaiser <peter dot kaiser at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ROBOTAPI_UNITS_WEISSHAPTIC_SLICE_
#define _ARMARX_ROBOTAPI_UNITS_WEISSHAPTIC_SLICE_

#include <RobotAPI/interface/units/HapticUnit.ice>

module armarx
{
	/**
	* Implements an interface to a HapticUnit for the tactile sensors from Weiss Robotics.
	**/
    interface WeissHapticUnitInterface extends armarx::HapticUnitInterface
    {
		/**
		* setDeviceTag allows to rename device.
		* @param deviceName Original name of tactile sensor device.
		* @param tag New name of tactile sensor device.
		**/
        void setDeviceTag(string deviceName, string tag);
        /**
		* startLogging starts logging when devices are initialized and started.
		**/
        void startLogging();
        /**
		* stopLogging stop logging when devices are initialized and started.
		**/
        void stopLogging();
    };

};

#endif
