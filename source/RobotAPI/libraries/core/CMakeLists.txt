armarx_set_target("RobotAPI Core Library: RobotAPICore")


find_package(Eigen3 QUIET)
find_package(Simox ${ArmarX_Simox_VERSION} QUIET)

armarx_build_if(Eigen3_FOUND "Eigen3 not available")
armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")

if (Eigen3_FOUND AND Simox_FOUND)
    include_directories(
        ${Eigen3_INCLUDE_DIR}
        ${Simox_INCLUDE_DIRS})
endif()

set(LIB_NAME       RobotAPICore)



set(LIBS RobotAPIInterfaces ArmarXCoreObservers ArmarXCoreStatechart ArmarXCoreEigen3Variants ${Simox_LIBRARIES})


set(LIB_FILES
    PIDController.cpp
    Pose.cpp
    FramedPose.cpp
    LinkedPose.cpp
    RobotStatechartContext.cpp
    checks/ConditionCheckMagnitudeChecks.cpp
    RobotAPIObjectFactories.cpp
    remoterobot/RobotStateObserver.cpp
    remoterobot/RemoteRobot.cpp
    remoterobot/RemoteRobotNode.cpp
)

set(LIB_HEADERS
    PIDController.h
    Pose.h
    FramedPose.h
    LinkedPose.h
    RobotStatechartContext.h
    observerfilters/PoseMedianFilter.h
    observerfilters/OffsetFilter.h
    observerfilters/MatrixFilters.h
    checks/ConditionCheckEqualsPose.h
    checks/ConditionCheckEqualsPoseWithTolerance.h
    checks/ConditionCheckMagnitudeChecks.h
    RobotAPIObjectFactories.h
    remoterobot/RobotStateObserver.h
    remoterobot/RemoteRobot.h
    math/SlidingWindowVectorMedian.h
    math/MathUtils.h
    math/Trigonometry.h
    math/SVD.h
    math/StatUtils.h
    math/MatrixHelpers.h
)


armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")
