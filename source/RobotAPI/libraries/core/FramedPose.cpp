#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

// boost includes
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>



#include <VirtualRobot/Robot.h>
#include <VirtualRobot/LinkedCoordinate.h>
#include <VirtualRobot/LinkedCoordinate.h>
#include <VirtualRobot/VirtualRobot.h>

using namespace Eigen;
using namespace std;

namespace armarx
{


    FramedDirection::FramedDirection()
    {
    }

    FramedDirection::FramedDirection(const FramedDirection& source) :
        IceUtil::Shared(source),
        Vector3Base(source),
        FramedDirectionBase(source),
        Vector3(source)
    {
    }

    FramedDirection::FramedDirection(const Eigen::Vector3f& vec, const string& frame, const string& agent) :
        Vector3(vec)
    {
        this->frame = frame;
        this->agent = agent;

    }

    FramedDirection::FramedDirection(Ice::Float x, ::Ice::Float y, ::Ice::Float z, const std::string& frame) :
        Vector3(x, y, z)
    {
        this->frame = frame;
    }

    string FramedDirection::getFrame() const
    {
        return frame;
    }

    FramedDirectionPtr FramedDirection::ChangeFrame(const VirtualRobot::RobotPtr robot, const FramedDirection& framedVec, const string& newFrame)
    {
        if (framedVec.getFrame() == newFrame)
        {
            return FramedDirectionPtr::dynamicCast(framedVec.clone());
        }

        if (!robot->hasRobotNode(newFrame))
        {
            throw LocalException() << "The requested frame '" << newFrame << "' does not exists in the robot " << robot->getName();
        }

        Eigen::Matrix4f rotToNewFrame = __GetRotationBetweenFrames(framedVec.frame, newFrame, robot);

        Eigen::Vector3f vecOldFrame = framedVec.Vector3::toEigen();

        Eigen::Vector3f newVec = rotToNewFrame.block(0, 0, 3, 3).inverse() * vecOldFrame;

        FramedDirectionPtr result = new FramedDirection();
        result->x = newVec(0);
        result->y = newVec(1);
        result->z = newVec(2);
        result->frame = newFrame;
        return result;
    }

    void FramedDirection::changeFrame(const VirtualRobot::RobotPtr robot, const string& newFrame)
    {
        if (getFrame() == newFrame)
        {
            return;
        }

        if (newFrame == GlobalFrame)
        {
            changeToGlobal(robot);
            return;
        }

        if (!robot->hasRobotNode(newFrame))
        {
            throw LocalException() << "The requested frame '" << newFrame << "' does not exists in the robot " << robot->getName();
        }

        Eigen::Matrix4f rotToNewFrame = __GetRotationBetweenFrames(frame, newFrame, robot);

        Eigen::Vector3f vecOldFrame = Vector3::toEigen();

        Eigen::Vector3f newVec = rotToNewFrame.block(0, 0, 3, 3).inverse() * vecOldFrame;


        x = newVec(0);
        y = newVec(1);
        z = newVec(2);
        frame = newFrame;
    }

    void FramedDirection::changeToGlobal(const SharedRobotInterfacePrx& referenceRobot)
    {
        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        changeToGlobal(sharedRobot);
    }

    void FramedDirection::changeToGlobal(const VirtualRobot::RobotPtr& referenceRobot)
    {
        if (frame == GlobalFrame)
        {
            return;
        }

        changeFrame(referenceRobot, referenceRobot->getRootNode()->getName());
        Eigen::Vector3f pos = referenceRobot->getRootNode()->getGlobalPose().block<3, 3>(0, 0) * toEigen();
        x = pos[0];
        y = pos[1];
        z = pos[2];
        frame = GlobalFrame;
        agent = "";
    }

    FramedDirectionPtr FramedDirection::toGlobal(const SharedRobotInterfacePrx& referenceRobot) const
    {
        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        return toGlobal(sharedRobot);
    }

    FramedDirectionPtr FramedDirection::toGlobal(const VirtualRobot::RobotPtr& referenceRobot) const
    {
        FramedDirectionPtr newPos = FramedDirectionPtr::dynamicCast(this->clone());
        newPos->changeToGlobal(referenceRobot);
        return newPos;
    }

    string FramedDirection::output(const Ice::Current& c) const
    {
        std::stringstream s;
        s << toEigen() << std::endl << "frame: " << getFrame() << (agent.empty() ? "" : (" agent: " + agent));
        return s.str();
    }

    Eigen::Matrix4f FramedDirection::__GetRotationBetweenFrames(const std::string& oldFrame, const std::string& newFrame, VirtualRobot::RobotPtr robotState)
    {
        Eigen::Matrix4f toNewFrame;

        if (oldFrame.compare(GlobalFrame) == 0)
        {
            toNewFrame = robotState->getRobotNode(newFrame)->getGlobalPose();
        }
        else
        {
            toNewFrame = robotState->getRobotNode(oldFrame)->getTransformationTo(robotState->getRobotNode(newFrame));
        }

        toNewFrame(0, 3) = 0;
        toNewFrame(1, 3) = 0;
        toNewFrame(2, 3) = 0;

        return toNewFrame;
    }

    int FramedDirection::readFromXML(const string& xmlData, const Ice::Current& c)
    {
        int result = Vector3::readFromXML(xmlData);

        boost::property_tree::ptree pt = Variant::GetPropertyTree(xmlData);

        frame = (pt.get<std::string>("frame"));

        return result;
    }

    string FramedDirection::writeAsXML(const Ice::Current& c)
    {
        using namespace boost::property_tree;
        ptree pt = Variant::GetPropertyTree(Vector3::writeAsXML());
        pt.add("frame", frame);

#if BOOST_VERSION >= 105600
        boost::property_tree::xml_parser::xml_writer_settings<std::string> settings('\t', 1);
#else
        boost::property_tree::xml_parser::xml_writer_settings<char> settings('\t', 1);
#endif

        std::stringstream stream;
        xml_parser::write_xml(stream, pt, settings);

        return stream.str();
    }

    void FramedDirection::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        Vector3::serialize(serializer);
        obj->setString("frame", frame);
        obj->setString("agent", agent);

    }

    void FramedDirection::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        Vector3::deserialize(serializer);
        frame = obj->getString("frame");

        if (obj->hasElement("agent"))
        {
            agent = obj->getString("agent");
        }
    }



    FramedPose::FramedPose() :
        Pose()
    {
        frame = "";
    }

    FramedPose::FramedPose(const FramedPose& pose) :
        IceUtil::Shared(pose),
        PoseBase(pose),
        FramedPoseBase(pose),
        Pose(pose)
    {

    }

    FramedPose::FramedPose(const Eigen::Matrix3f& m, const Eigen::Vector3f& v, const std::string& s, const string& agent) :
        Pose(m, v)
    {
        frame = s;
        this->agent = agent;
    }

    FramedPose::FramedPose(const Eigen::Matrix4f& m, const std::string& s, const string& agent) :
        Pose(m)
    {
        frame = s;
        this->agent = agent;
    }

    FramedPose::FramedPose(const armarx::Vector3BasePtr pos, const armarx::QuaternionBasePtr ori, const std::string& frame, const string& agent) :
        Pose(pos, ori)
    {
        this->frame = frame;
        this->agent = agent;
    }

    std::string FramedPose::getFrame() const
    {
        return frame;
    }

    string FramedPose::output(const Ice::Current& c) const
    {
        std::stringstream s;
        s << toEigen() << std::endl << "frame: " << getFrame() << (agent.empty() ? "" : (" agent: " + agent));
        return s.str();
    }

    void FramedPose::changeFrame(const SharedRobotInterfacePrx& referenceRobot, const string& newFrame)
    {
        if (newFrame == frame)
        {
            return;
        }

        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        changeFrame(sharedRobot, newFrame);
    }

    void FramedPose::changeFrame(const VirtualRobot::RobotPtr& referenceRobot, const string& newFrame)
    {
        if (newFrame == frame)
        {
            return;
        }

        if (newFrame == GlobalFrame)
        {
            changeToGlobal(referenceRobot);
            return;
        }

        Eigen::Matrix4f oldFrameTransformation = Eigen::Matrix4f::Identity();

        if (!referenceRobot->hasRobotNode(newFrame))
        {
            throw LocalException() << "The requested frame '" << newFrame << "' does not exists in the robot " << referenceRobot->getName();
        }

        if (frame != GlobalFrame)
        {
            oldFrameTransformation = referenceRobot->getRobotNode(frame)->getPoseInRootFrame();
        }
        else
        {
            oldFrameTransformation = referenceRobot->getRootNode()->getGlobalPose().inverse();
        }

        Eigen::Matrix4f newPose =
            (referenceRobot->getRobotNode(newFrame)->getPoseInRootFrame().inverse() * oldFrameTransformation) * toEigen();

        orientation = new Quaternion(newPose);
        Eigen::Vector3f pos = newPose.block<3, 1>(0, 3);
        position = new Vector3(pos);
        frame = newFrame;
        init();
    }

    void FramedPose::changeToGlobal(const SharedRobotInterfacePrx& referenceRobot)
    {
        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        changeToGlobal(sharedRobot);

    }

    void FramedPose::changeToGlobal(const VirtualRobot::RobotPtr& referenceRobot)
    {
        if (frame == GlobalFrame)
        {
            return;
        }

        changeFrame(referenceRobot, referenceRobot->getRootNode()->getName());
        Eigen::Matrix4f global = referenceRobot->getRootNode()->getGlobalPose();
        const Eigen::Matrix4f pose = global * toEigen();
        position->x = pose(0, 3);
        position->y = pose(1, 3);
        position->z = pose(2, 3);
        Eigen::Quaternionf q(pose.block<3, 3>(0, 0));
        orientation->qw = q.w();
        orientation->qx = q.x();
        orientation->qy = q.y();
        orientation->qz = q.z();
        frame = GlobalFrame;
        agent = "";
    }

    FramedPosePtr FramedPose::toGlobal(const SharedRobotInterfacePrx& referenceRobot) const
    {
        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        return toGlobal(sharedRobot);
    }

    FramedPosePtr FramedPose::toGlobal(const VirtualRobot::RobotPtr& referenceRobot) const
    {
        FramedPosePtr newPose = FramedPosePtr::dynamicCast(this->clone());
        newPose->changeToGlobal(referenceRobot);
        return newPose;
    }

    int FramedPose::readFromXML(const string& xmlData, const Ice::Current& c)
    {
        boost::property_tree::ptree pt = Variant::GetPropertyTree(xmlData);


        Pose::readFromXML(xmlData);
        frame = (pt.get<std::string>("frame"));

        return 1;
    }

    string FramedPose::writeAsXML(const Ice::Current&)
    {
        using namespace boost::property_tree;
        std::stringstream stream;
        stream << Pose::writeAsXML()
               << "<frame>" << frame << "</frame>";
        return stream.str();
    }

    FramedPositionPtr FramedPose::getPosition() const
    {
        FramedPositionPtr result = new FramedPosition(Vector3Ptr::dynamicCast(position)->toEigen(), frame, agent);
        return result;
    }

    FramedOrientationPtr FramedPose::getOrientation() const
    {
        FramedOrientationPtr result = new FramedOrientation(QuaternionPtr::dynamicCast(this->orientation)->toEigen(), frame, agent);
        return result;
    }

    void FramedPose::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        Pose::serialize(obj, c);
        obj->setString("frame", frame);
        obj->setString("agent", agent);
    }

    void FramedPose::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        Pose::deserialize(obj);
        frame = obj->getString("frame");

        if (obj->hasElement("agent"))
        {
            agent = obj->getString("agent");
        }
    }

    FramedPosition::FramedPosition() :
        Vector3()
    {
        frame = "";
    }

    FramedPosition::FramedPosition(const Eigen::Vector3f& v, const std::string& s, const string& agent) :
        Vector3(v)
    {
        frame = s;
        this->agent = agent;
    }

    FramedPosition::FramedPosition(const Eigen::Matrix4f& m, const std::string& s, const string& agent) :
        Vector3(m)
    {
        frame = s;
        this->agent = agent;
    }

    // this doesnt work for unknown reasons
    //    FramedPosition::FramedPosition(const Vector3BasePtr pos, const std::string &frame )
    //    {
    //        this->x = pos->x;
    //        this->y = pos->y;
    //        this->z = pos->z;
    //        this->frame = frame;
    //    }

    std::string FramedPosition::getFrame() const
    {
        return this->frame;
    }

    void FramedPosition::changeFrame(const SharedRobotInterfacePrx& referenceRobot, const string& newFrame)
    {
        if (newFrame == frame)
        {
            return;
        }

        if (!referenceRobot->hasRobotNode(newFrame))
        {
            throw LocalException() << "The requested frame '" << newFrame << "' does not exists in the robot " << referenceRobot->getName();
        }

        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        changeFrame(sharedRobot, newFrame);
    }

    void FramedPosition::changeFrame(const VirtualRobot::RobotPtr& referenceRobot, const string& newFrame)
    {
        if (newFrame == frame)
        {
            return;
        }

        if (newFrame == GlobalFrame)
        {
            changeToGlobal(referenceRobot);
            return;
        }

        Eigen::Matrix4f oldFrameTransformation = Eigen::Matrix4f::Identity();

        if (!referenceRobot->hasRobotNode(newFrame))
        {
            throw LocalException() << "The requested frame '" << newFrame << "' does not exists in the robot " << referenceRobot->getName();
        }

        if (frame != GlobalFrame)
        {
            oldFrameTransformation = referenceRobot->getRobotNode(frame)->getPoseInRootFrame();
        }
        else
        {
            oldFrameTransformation = referenceRobot->getRootNode()->getGlobalPose().inverse();
        }

        Eigen::Matrix4f oldPose = Eigen::Matrix4f::Identity();
        oldPose.block<3, 1>(0, 3) = toEigen();
        Eigen::Matrix4f newPose =
            (referenceRobot->getRobotNode(newFrame)->getPoseInRootFrame().inverse() * oldFrameTransformation) * oldPose;

        Eigen::Vector3f pos = newPose.block<3, 1>(0, 3);
        x = pos[0];
        y = pos[1];
        z = pos[2];
        frame = newFrame;
    }

    void FramedPosition::changeToGlobal(const SharedRobotInterfacePrx& referenceRobot)
    {
        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        changeToGlobal(sharedRobot);
    }

    void FramedPosition::changeToGlobal(const VirtualRobot::RobotPtr& referenceRobot)
    {
        if (frame == GlobalFrame)
        {
            return;
        }

        changeFrame(referenceRobot, referenceRobot->getRootNode()->getName());
        Eigen::Vector3f pos = referenceRobot->getRootNode()->getGlobalPose().block<3, 3>(0, 0) * toEigen()
                              + referenceRobot->getRootNode()->getGlobalPose().block<3, 1>(0, 3);
        x = pos[0];
        y = pos[1];
        z = pos[2];
        frame = GlobalFrame;
        agent = "";
    }

    FramedPositionPtr FramedPosition::toGlobal(const SharedRobotInterfacePrx& referenceRobot) const
    {
        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        return toGlobal(sharedRobot);
    }

    FramedPositionPtr FramedPosition::toGlobal(const VirtualRobot::RobotPtr& referenceRobot) const
    {
        FramedPositionPtr newPos = FramedPositionPtr::dynamicCast(this->clone());
        newPos->changeToGlobal(referenceRobot);
        return newPos;
    }

    string FramedPosition::output(const Ice::Current& c) const
    {
        std::stringstream s;
        s << toEigen() << std::endl << "frame: " << getFrame() << (agent.empty() ? "" : (" agent: " + agent));
        return s.str();
    }

    int FramedPosition::readFromXML(const string& xmlData, const Ice::Current& c)
    {
        using namespace boost::property_tree;
        ptree pt;
        std::stringstream stream;
        stream << xmlData;
        xml_parser::read_xml(stream, pt);

        Vector3::readFromXML(xmlData);
        frame = (pt.get<std::string>("frame"));
        agent = (pt.get<std::string>("agent"));

        return 1;
    }

    string FramedPosition::writeAsXML(const Ice::Current&)
    {
        using namespace boost::property_tree;
        std::stringstream stream;
        stream << Vector3::writeAsXML() <<
               "<frame>" << frame << "</frame>" <<
               "<agent>" << agent << "</agent>";
        return stream.str();
    }

    void FramedPosition::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        Vector3::serialize(obj, c);
        obj->setString("frame", frame);
        obj->setString("agent", agent);
    }

    void FramedPosition::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        Vector3::deserialize(obj);
        frame = obj->getString("frame");

        if (obj->hasElement("agent"))
        {
            agent = obj->getString("agent");
        }
    }


    FramedOrientation::FramedOrientation() :
        Quaternion()
    {
        frame = "";
    }

    FramedOrientation::FramedOrientation(const Eigen::Matrix3f& m, const std::string& s, const string& agent) :
        Quaternion(m)
    {
        frame = s;
        this->agent = agent;
    }

    FramedOrientation::FramedOrientation(const Eigen::Matrix4f& m, const std::string& s, const std::string& agent) :
        Quaternion(m)
    {
        frame = s;
        this->agent = agent;
    }

    // this doesnt work for an unknown reason
    //    FramedOrientation::FramedOrientation(const QuaternionBasePtr ori, const std::string &frame )
    //    {
    //        Matrix3f rot;
    //        rot = Quaternionf(ori->qw, ori->qx, ori->qy, ori->qz);
    //        FramedOrientation(rot, frame);
    //    }

    std::string FramedOrientation::getFrame() const
    {
        return this->frame;
    }

    string FramedOrientation::output(const Ice::Current& c) const
    {
        std::stringstream s;
        s << toEigen() << std::endl << "frame: " << getFrame() << (agent.empty() ? "" : (" agent: " + agent));
        return s.str();
    }

    void FramedOrientation::changeFrame(const SharedRobotInterfacePrx& referenceRobot, const string& newFrame)
    {
        if (newFrame == frame)
        {
            return;
        }

        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));

        changeFrame(sharedRobot, newFrame);
    }

    void FramedOrientation::changeFrame(const VirtualRobot::RobotPtr& referenceRobot, const string& newFrame)
    {
        if (newFrame == frame)
        {
            return;
        }

        if (newFrame == GlobalFrame)
        {
            changeToGlobal(referenceRobot);
            return;
        }

        Eigen::Matrix4f oldFrameTransformation = Eigen::Matrix4f::Identity();

        if (!referenceRobot->hasRobotNode(newFrame))
        {
            throw LocalException() << "The requested frame '" << newFrame << "' does not exists in the robot " << referenceRobot->getName();
        }

        if (frame != GlobalFrame)
        {
            oldFrameTransformation = referenceRobot->getRobotNode(frame)->getPoseInRootFrame();
        }
        else
        {
            oldFrameTransformation = referenceRobot->getRootNode()->getGlobalPose().inverse();
        }

        Eigen::Matrix4f oldPose = Eigen::Matrix4f::Identity();
        oldPose.block<3, 3>(0, 0) = toEigen();

        Eigen::Matrix4f newPose =
            (referenceRobot->getRobotNode(newFrame)->getPoseInRootFrame().inverse() * oldFrameTransformation) * oldPose;

        Eigen::Quaternionf quat(newPose.block<3, 3>(0, 0));
        qw = quat.w();
        qx = quat.x();
        qy = quat.y();
        qz = quat.z();
        frame = newFrame;
    }

    void FramedOrientation::changeToGlobal(const SharedRobotInterfacePrx& referenceRobot)
    {
        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        changeToGlobal(sharedRobot);
    }

    void FramedOrientation::changeToGlobal(const VirtualRobot::RobotPtr& referenceRobot)
    {
        if (frame == GlobalFrame)
        {
            return;
        }

        changeFrame(referenceRobot, referenceRobot->getRootNode()->getName());
        Eigen::Matrix3f rot = referenceRobot->getRootNode()->getGlobalPose().block<3, 3>(0, 0) * toEigen();
        Eigen::Quaternionf quat(rot);
        qw = quat.w();
        qx = quat.x();
        qy = quat.y();
        qz = quat.z();
        frame = GlobalFrame;
        agent = "";
    }

    FramedOrientationPtr FramedOrientation::toGlobal(const SharedRobotInterfacePrx& referenceRobot) const
    {
        VirtualRobot::RobotPtr sharedRobot(new RemoteRobot(referenceRobot));
        return toGlobal(sharedRobot);
    }

    FramedOrientationPtr FramedOrientation::toGlobal(const VirtualRobot::RobotPtr& referenceRobot) const
    {
        FramedOrientationPtr newPos = FramedOrientationPtr::dynamicCast(this->clone());
        newPos->changeToGlobal(referenceRobot);
        return newPos;
    }

    int FramedOrientation::readFromXML(const string& xmlData, const Ice::Current& c)
    {
        using namespace boost::property_tree;
        ptree pt;
        std::stringstream stream;
        stream << xmlData;
        xml_parser::read_xml(stream, pt);

        Quaternion::readFromXML(xmlData);
        frame = (pt.get<std::string>("frame"));
        agent = (pt.get<std::string>("agent"));

        return 1;
    }

    string FramedOrientation::writeAsXML(const Ice::Current&)
    {
        using namespace boost::property_tree;
        std::stringstream stream;
        stream << Quaternion::writeAsXML() <<
               "<frame>" << frame << "</frame>" <<
               "<agent>" << agent << "</agent>";
        return stream.str();
    }

    void FramedOrientation::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        Quaternion::serialize(obj, c);
        obj->setString("frame", frame);
        obj->setString("agent", agent);
    }

    void FramedOrientation::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        Quaternion::deserialize(obj);
        frame = obj->getString("frame");

        if (obj->hasElement("agent"))
        {
            agent = obj->getString("agent");
        }
    }


    VirtualRobot::LinkedCoordinate FramedPose::createLinkedCoordinate(const VirtualRobot::RobotPtr& virtualRobot, const FramedPositionPtr& position, const FramedOrientationPtr& orientation)
    {
        VirtualRobot::LinkedCoordinate c(virtualRobot);
        std::string frame;

        if (position)
        {
            frame = position->getFrame();

            if (orientation)
            {
                if (!frame.empty() && frame != orientation->getFrame())
                {
                    throw armarx::UserException("Error: frames mismatch");
                }
            }
        }
        else
        {
            if (!orientation)
            {
                armarx::UserException("createLinkedCoordinate: orientation and position are both NULL");
            }
            else
            {
                frame = orientation->getFrame();
            }
        }

        Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();

        if (orientation)
        {
            pose.block<3, 3>(0, 0) = orientation->toEigen();
        }

        if (position)
        {
            pose.block<3, 1>(0, 3) = position->toEigen();
        }

        c.set(frame, pose);

        return c;
    }





}
