/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::core::PIDController
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PIDController.h"


using namespace armarx;


PIDController::PIDController(float Kp, float Ki, float Kd) :
    Kp(Kp),
    Ki(Ki),
    Kd(Kd),
    integral(0),
    derivative(0),
    previousError(0),
    processValue(0),
    target(0),
    controlValue(0)
{
    reset();
}

void PIDController::reset()
{
    ScopedRecursiveLock lock(mutex);
    firstRun = true;
    previousError = 0;
    integral = 0;
    lastUpdateTime = IceUtil::Time::now();
}

void PIDController::update(double measuredValue, double targetValue)
{
    ScopedRecursiveLock lock(mutex);
    IceUtil::Time now = IceUtil::Time::now();

    if (firstRun)
    {
        lastUpdateTime = IceUtil::Time::now();
    }

    double dt = (now - lastUpdateTime).toSecondsDouble();
    update(dt, measuredValue, targetValue);
    lastUpdateTime = now;
}

void PIDController::update(double deltaSec, double measuredValue, double targetValue)
{
    ScopedRecursiveLock lock(mutex);
    processValue = measuredValue;
    target = targetValue;

    double error = target - processValue;

    //double dt = (now - lastUpdateTime).toSecondsDouble();
    //    ARMARX_INFO << deactivateSpam() << VAROUT(dt);
    if (!firstRun)
    {
        integral += error * deltaSec;

        if (deltaSec > 0.0)
        {
            derivative = (error - previousError) / deltaSec;
        }
    }

    firstRun = false;
    controlValue = Kp * error + Ki * integral + Kd * derivative;
    ARMARX_DEBUG << deactivateSpam(0.5) << " error: " << error << " cV: " << (controlValue) <<  " i: " << (Ki * integral) << " d: " << (Kd * derivative) << " dt: " << deltaSec;

    previousError = error;
    lastUpdateTime += IceUtil::Time::seconds(deltaSec);

}

double PIDController::getControlValue() const
{
    ScopedRecursiveLock lock(mutex);
    return controlValue;
}


MultiDimPIDController::MultiDimPIDController(float Kp, float Ki, float Kd) :
    Kp(Kp),
    Ki(Ki),
    Kd(Kd),
    integral(0),
    derivative(0),
    previousError(0)
{
    reset();
}

void MultiDimPIDController::update(const double deltaSec, const Eigen::VectorXf& measuredValue, const Eigen::VectorXf& targetValue)
{
    ScopedRecursiveLock lock(mutex);
    processValue = measuredValue;
    target = targetValue;

    double error = (target - processValue).norm();

    //double dt = (now - lastUpdateTime).toSecondsDouble();
    //    ARMARX_INFO << deactivateSpam() << VAROUT(dt);
    if (!firstRun)
    {
        integral += error * deltaSec;

        if (deltaSec > 0.0)
        {
            derivative = (error - previousError) / deltaSec;
        }
    }

    firstRun = false;
    Eigen::VectorXf direction = targetValue; // copy size

    if ((target - processValue).norm() > 0)
    {
        direction = (target - processValue).normalized();
    }
    else
    {
        direction.setZero();
    }

    controlValue = direction * (Kp * error + Ki * integral + Kd * derivative);
    ARMARX_DEBUG << deactivateSpam(0.5) << " error: " << error << " cV: " << (controlValue) <<  " i: " << (Ki * integral) << " d: " << (Kd * derivative) << " dt: " << deltaSec;

    previousError = error;
    lastUpdateTime += IceUtil::Time::seconds(deltaSec);

}

void MultiDimPIDController::update(const Eigen::VectorXf& measuredValue, const Eigen::VectorXf& targetValue)
{
    ScopedRecursiveLock lock(mutex);
    IceUtil::Time now = IceUtil::Time::now();

    if (firstRun)
    {
        lastUpdateTime = IceUtil::Time::now();
    }

    double dt = (now - lastUpdateTime).toSecondsDouble();
    update(dt, measuredValue, targetValue);
    lastUpdateTime = now;
}

const Eigen::VectorXf& MultiDimPIDController::getControlValue() const
{
    return controlValue;
}

void MultiDimPIDController::reset()
{
    ScopedRecursiveLock lock(mutex);
    firstRun = true;
    previousError = 0;
    integral = 0;
    lastUpdateTime = IceUtil::Time::now();
    //    controlValue.setZero();
    //    processValue.setZero();
    //    target.setZero();
}
