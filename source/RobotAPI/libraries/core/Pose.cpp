#include "Pose.h"

// boost includes
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>



#include <VirtualRobot/Robot.h>
#include <VirtualRobot/LinkedCoordinate.h>
#include <VirtualRobot/LinkedCoordinate.h>
#include <VirtualRobot/VirtualRobot.h>

using namespace Eigen;
using namespace std;

namespace armarx
{

    Vector2::Vector2()
    {
        x = 0;
        y = 0;
    }

    Vector2::Vector2(const Vector2f& v)
    {
        x = v(0);
        y = v(1);
    }

    Vector2::Vector2(::Ice::Float x, ::Ice::Float y)
    {
        this->x = x;
        this->y = y;
    }

    Vector2f Vector2::toEigen() const
    {
        Vector2f v;
        v << this->x, this->y;
        return v;
    }

    void Vector2::operator=(const Eigen::Vector2f& vec)
    {
        x = vec[0];
        y = vec[1];
    }

    string Vector2::output(const Ice::Current& c) const
    {
        std::stringstream s;
        s << toEigen();
        return s.str();
    }

    int Vector2::readFromXML(const string& xmlData, const Ice::Current& c)
    {
        boost::property_tree::ptree pt = Variant::GetPropertyTree(xmlData);

        x = (pt.get<float>("x"));
        y = (pt.get<float>("y"));
        return 1;
    }

    string Vector2::writeAsXML(const Ice::Current&)
    {
        using namespace boost::property_tree;
        ptree pt;
        pt.add("x", x);
        pt.add("y", y);

        std::stringstream stream;
        xml_parser::write_xml(stream, pt);
        return stream.str();
    }

    void Vector2::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setFloat("x", x);
        obj->setFloat("y", y);
    }

    void Vector2::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        x = obj->getFloat("x");
        y = obj->getFloat("y");
    }

    Vector3::Vector3()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    Vector3::Vector3(const Vector3f& v)
    {
        x = v(0);
        y = v(1);
        z = v(2);
    }

    Vector3::Vector3(const Matrix4f& m)
    {
        x = m(0, 3);
        y = m(1, 3);
        z = m(2, 3);
    }

    Vector3::Vector3(::Ice::Float x, ::Ice::Float y, ::Ice::Float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    Vector3f Vector3::toEigen() const
    {
        Vector3f v;
        v << this->x, this->y, this->z;
        return v;
    }

    void Vector3::operator=(const Eigen::Vector3f& vec)
    {
        x = vec[0];
        y = vec[1];
        z = vec[2];
    }

    string Vector3::output(const Ice::Current& c) const
    {
        std::stringstream s;
        s << toEigen();
        return s.str();
    }

    int Vector3::readFromXML(const string& xmlData, const Ice::Current& c)
    {
        boost::property_tree::ptree pt = Variant::GetPropertyTree(xmlData);

        x = (pt.get<float>("x"));
        y = (pt.get<float>("y"));
        z = (pt.get<float>("z"));
        return 1;
    }

    string Vector3::writeAsXML(const Ice::Current&)
    {
        using namespace boost::property_tree;
        ptree pt;
        pt.add("x", x);
        pt.add("y", y);
        pt.add("z", z);

        std::stringstream stream;
        xml_parser::write_xml(stream, pt);
        return stream.str();
    }

    void Vector3::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setFloat("x", x);
        obj->setFloat("y", y);
        obj->setFloat("z", z);
    }

    void Vector3::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        x = obj->getFloat("x");
        y = obj->getFloat("y");
        z = obj->getFloat("z");
    }


    Quaternion::Quaternion()
    {
    }

    Quaternion::Quaternion(const Matrix4f& m4)
    {
        Matrix3f m3 = m4.block<3, 3>(0, 0);
        this->init(m3);
    }

    Quaternion::Quaternion(const Matrix3f& m3)
    {
        this->init(m3);
    }

    Quaternion::Quaternion(const Eigen::Quaternionf& q)
    {
        this->init(q);
    }

    Quaternion::Quaternion(::Ice::Float qw, ::Ice::Float qx, ::Ice::Float qy, ::Ice::Float qz)
    {
        this->qw = qw;
        this->qx = qx;
        this->qy = qy;
        this->qz = qz;
    }

    Matrix3f Quaternion::toEigen() const
    {
        Matrix3f rot;
        rot = Quaternionf(
                  this->qw,
                  this->qx,
                  this->qy,
                  this->qz);
        return rot;
    }

    Eigen::Quaternionf Quaternion::toEigenQuaternion() const
    {
        return Quaternionf(this->qw, this->qx, this->qy, this->qz);
    }

    void Quaternion::init(const Matrix3f& m3)
    {
        Quaternionf q;
        q = m3;
        init(q);
    }

    void Quaternion::init(const Eigen::Quaternionf& q)
    {
        this->qw = q.w();
        this->qx = q.x();
        this->qy = q.y();
        this->qz = q.z();
    }

    Matrix3f Quaternion::slerp(float alpha, const Eigen::Matrix3f& m)
    {
        return Quaternion::slerp(alpha, this->toEigen(), m);
    }

    Matrix3f Quaternion::slerp(float alpha, const Eigen::Matrix3f& m1, const Eigen::Matrix3f& m2)
    {
        Matrix3f result;
        Quaternionf q1, q2;
        q1 = m1;
        q2 = m2;
        result = q1.slerp(alpha, q2);
        return result;
    }

    string Quaternion::output(const Ice::Current& c) const
    {
        std::stringstream s;
        s << toEigen();
        return s.str();
    }

    int Quaternion::readFromXML(const string& xmlData, const Ice::Current& c)
    {
        boost::property_tree::ptree pt = Variant::GetPropertyTree(xmlData);

        using namespace Eigen;

        Quaternionf q;

        if (pt.get_optional<float>("angle"))
        {
            // AxisAngle-Notation
            float angle = pt.get<float>("angle");

            Vector3f axis;
            axis << pt.get<float>("x")  , pt.get<float>("y"), pt.get<float>("z");

            AngleAxisf aa(angle, axis);
            q = aa;
            qw = q.w();
            qx = q.x();
            qy = q.y();
            qz = q.z();
        }
        else
        {
            qx = (pt.get<float>("qx"));
            qy = (pt.get<float>("qy"));
            qz = (pt.get<float>("qz"));
            qw = (pt.get<float>("qw"));
        }

        return 1;
    }

    string Quaternion::writeAsXML(const Ice::Current&)
    {
        using namespace boost::property_tree;
        ptree pt;
        pt.add("qx", qx);
        pt.add("qy", qy);
        pt.add("qz", qz);
        pt.add("qw", qw);

        std::stringstream stream;
        xml_parser::write_xml(stream, pt);
        return stream.str();
    }

    void Quaternion::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setFloat("qx", qx);
        obj->setFloat("qy", qy);
        obj->setFloat("qz", qz);
        obj->setFloat("qw", qw);
    }

    void Quaternion::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        qx = obj->getFloat("qx");
        qy = obj->getFloat("qy");
        qz = obj->getFloat("qz");
        qw = obj->getFloat("qw");
    }

    Pose::Pose(const Eigen::Matrix3f& m, const Eigen::Vector3f& v)
    {
        this->orientation = new Quaternion(m);
        this->position = new Vector3(v);
        init();
    }

    Pose::Pose(const armarx::Vector3BasePtr pos, const armarx::QuaternionBasePtr ori)
    {
        this->orientation = new Quaternion(*QuaternionPtr::dynamicCast(ori));
        this->position = new Vector3(*Vector3Ptr::dynamicCast(pos));
        init();
    }

    Pose::Pose()
    {
        this->orientation = new Quaternion();
        this->position = new Vector3();
        init();
    }

    Pose::Pose(const Pose& source) :
        IceUtil::Shared(source),
        PoseBase(source)
    {
        orientation = QuaternionBasePtr::dynamicCast(source.orientation->clone());
        position = Vector3Ptr::dynamicCast(source.position->clone());
        init();
    }

    Pose::Pose(const Matrix4f& m)
    {
        this->orientation = new Quaternion(m);
        this->position = new Vector3(m);
        init();
    }

    void Pose::operator=(const Matrix4f& matrix)
    {
        this->orientation = new Quaternion(matrix);
        this->position = new Vector3(matrix);
        init();
    }

    Matrix4f Pose::toEigen() const
    {
        Matrix4f m = Matrix4f::Identity();
        ARMARX_CHECK_EXPRESSION(c_orientation);
        ARMARX_CHECK_EXPRESSION(c_position);
        m.block<3, 3>(0, 0) = c_orientation->toEigen();
        m.block<3, 1>(0, 3) = c_position->toEigen();
        return m;
    }

    string Pose::output(const Ice::Current& c) const
    {
        std::stringstream s;
        s << toEigen();
        return s.str();
    }

    int Pose::readFromXML(const string& xmlData, const Ice::Current& c)
    {
        position->readFromXML(xmlData);
        orientation->readFromXML(xmlData);

        return 1;
    }

    string Pose::writeAsXML(const Ice::Current&)
    {
        using namespace boost::property_tree;
        std::stringstream stream;
        stream <<
               position->writeAsXML() <<
               orientation->writeAsXML();
        return stream.str();
    }

    void Pose::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c) const
    {
        position->serialize(serializer, c);
        orientation->serialize(serializer, c);
    }

    void Pose::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        position->deserialize(obj);
        orientation->deserialize(obj);
    }

    void Pose::init()
    {
        this->c_position = Vector3Ptr::dynamicCast(this->position);
        this->c_orientation = QuaternionPtr::dynamicCast(this->orientation);
    }


    void Pose::ice_postUnmarshal()
    {
        init();
    }


}
