/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::Core
 * @author     ( stefan dot ulbrich at kit dot edu)
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotAPI_Pose_H
#define _ARMARX_COMPONENT_RobotAPI_Pose_H

#include <sstream>
#include <Eigen/Core>
#include <Eigen/Geometry>

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <RobotAPI/interface/core/PoseBase.h>



namespace armarx
{
    namespace VariantType
    {
        // variant types
        const VariantTypeId Vector2 = Variant::addTypeName("::armarx::Vector2Base");
        const VariantTypeId Vector3 = Variant::addTypeName("::armarx::Vector3Base");
        const VariantTypeId Quaternion = Variant::addTypeName("::armarx::QuaternionBase");
        const VariantTypeId Pose = Variant::addTypeName("::armarx::PoseBase");
    }

    /**
     * @brief The Vector2 class
     * @ingroup VariantsGrp
     */
    class Vector2 : virtual public Vector2Base
    {
    public:
        Vector2();
        Vector2(const Eigen::Vector2f&);
        Vector2(::Ice::Float x, ::Ice::Float y);

        void operator=(const Eigen::Vector2f& ves);

        virtual Eigen::Vector2f toEigen() const;

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const
        {
            return new Vector2(*this);
        }
        std::string output(const Ice::Current& c = ::Ice::Current()) const;
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const
        {
            return VariantType::Vector2;
        }
        bool validate(const Ice::Current& c = ::Ice::Current())
        {
            return true;
        }

        /**
             * @brief Implementation of virtual function to read a Vector2 from an XML-file.
             *Example xml-layout:
             * @code
             *      <x>0</x>
             *      <y>0</y>
             * @endcode

             * @param xmlData String with xml-data. NOT a file path!
             * @return ErrorCode, 1 on Success
             */
        int readFromXML(const std::string& xmlData, const Ice::Current& c = ::Ice::Current());
        std::string writeAsXML(const Ice::Current& c = ::Ice::Current());

        friend std::ostream& operator<<(std::ostream& stream, const Vector2& rhs)
        {
            stream << "Vector2: " << std::endl << rhs.output() << std::endl;
            return stream;
        };

    public: // serialization
        virtual void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());

    };

    typedef IceInternal::Handle<Vector2> Vector2Ptr;

    /**
     * @class Vector3
     * @ingroup VariantsGrp
     * @ingroup RobotAPI-FramedPose
     * @brief The Vector3 class
     */
    class Vector3 :
        virtual public Vector3Base
    {
    public:
        Vector3();
        Vector3(const Eigen::Vector3f&);
        Vector3(const Eigen::Matrix4f&);
        Vector3(::Ice::Float x, ::Ice::Float y, ::Ice::Float z);

        void operator=(const Eigen::Vector3f& vec);

        virtual Eigen::Vector3f toEigen() const;

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const
        {
            return new Vector3(*this);
        }
        std::string output(const Ice::Current& c = ::Ice::Current()) const;
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const
        {
            return VariantType::Vector3;
        }
        bool validate(const Ice::Current& c = ::Ice::Current())
        {
            return true;
        }

        /**
             * @brief Implementation of virtual function to read a Vector3 from an XML-file.
             *Example xml-layout:
             * @code
             *      <x>0</x>
             *      <y>0</y>
             *      <z>0</z>
             * @endcode

             * @param xmlData String with xml-data. NOT a file path!
             * @return ErrorCode, 1 on Success
             */
        int readFromXML(const std::string& xmlData, const Ice::Current& c = ::Ice::Current());
        std::string writeAsXML(const Ice::Current& c = ::Ice::Current());

        friend std::ostream& operator<<(std::ostream& stream, const Vector3& rhs)
        {
            stream << "Vector3: " << std::endl << rhs.output() << std::endl;
            return stream;
        };

    public: // serialization
        virtual void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());
    };

    typedef IceInternal::Handle<Vector3> Vector3Ptr;


    /**
     * @class Quaternion
     * @ingroup VariantsGrp
     * @ingroup RobotAPI-FramedPose
     * @brief The Quaternion class
     */
    class Quaternion :
        virtual public QuaternionBase
    {
    public:
        Quaternion();
        Quaternion(const Eigen::Matrix4f&);
        Quaternion(const Eigen::Matrix3f&);
        Quaternion(const Eigen::Quaternionf&);
        Quaternion(::Ice::Float qw, ::Ice::Float qx, ::Ice::Float qy, ::Ice::Float qz);

        Eigen::Matrix3f toEigen() const;
        Eigen::Quaternionf toEigenQuaternion() const;
        Eigen::Matrix3f slerp(float, const Eigen::Matrix3f&);

        static Eigen::Matrix3f slerp(float, const Eigen::Matrix3f&, const Eigen::Matrix3f&);

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const
        {
            return new Quaternion(*this);
        }
        std::string output(const Ice::Current& c = ::Ice::Current()) const;
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const
        {
            return VariantType::Quaternion;
        }
        bool validate(const Ice::Current& c = ::Ice::Current())
        {
            return true;
        }

        /**
             * @brief Implementation of virtual function to read a Quaternion from an XML-file.
             *Example xml-layout:
             * @code
             *      <qw>0</qw>
             *      <qx>0</qx>
             *      <qy>0</qy>
             *      <qz>0</qz>
             * @endcode
             *
             *OR in AngleAxis-Notation with radiant
             *
             * @code
             *      <angle>0.5</qw>
             *      <x>0.2</x>
             *      <y>0.1</y>
             *      <z>0.7</z>
             * @endcode
             *
             * @param xmlData String with xml-data. NOT a file path!
             * @param c
             * @return ErrorCode, 1 on Success
             */
        int readFromXML(const std::string& xmlData, const Ice::Current& c = ::Ice::Current());
        std::string writeAsXML(const Ice::Current& c = ::Ice::Current());

        friend std::ostream& operator<<(std::ostream& stream, const Quaternion& rhs)
        {
            stream << "Quaternion: " << std::endl << rhs.output() << std::endl;
            return stream;
        };

    public: // serialization
        virtual void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());

    private:
        void init(const Eigen::Matrix3f&);
        void init(const Eigen::Quaternionf&);
    };

    typedef IceInternal::Handle<Quaternion> QuaternionPtr;


    /**
     * @class Pose
     * @ingroup VariantsGrp
     * @ingroup RobotAPI-FramedPose
     * @brief The Pose class
     */
    class Pose :
        virtual public PoseBase
    {
    public:
        Pose();
        Pose(const Pose& source);
        Pose(const Eigen::Matrix4f&);
        Pose(const Eigen::Matrix3f&, const Eigen::Vector3f&);
        Pose(const armarx::Vector3BasePtr pos, const armarx::QuaternionBasePtr ori);

        void operator=(const Eigen::Matrix4f& matrix);
        virtual Eigen::Matrix4f toEigen() const;

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const
        {
            return new Pose(*this);
        }
        std::string output(const Ice::Current& c = ::Ice::Current()) const;
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const
        {
            return VariantType::Pose;
        }
        bool validate(const Ice::Current& c = ::Ice::Current())
        {
            return true;
        }

        /**
             * @brief Implementation of virtual function to read a FramedOrientation from an XML-file.
             *Example xml-layout:
             * @code
             *      <frame>leftHand</frame>
             *      <x>0</x>
             *      <y>0</y>
             *      <z>0</z>
             *      <qw>0.45</qw>
             *      <qx>0.2</qx>
             *      <qy>0</qy>
             *      <qz>0</qz>
             * @endcode

             * @param xmlData String with xml-data. NOT a file path!
             * @param c
             * @return ErrorCode, 1 on Success
             * @see Quaternion::readFromXml() for AxisAngle-Notation
             */
        int readFromXML(const std::string& xmlData, const Ice::Current& c = ::Ice::Current());
        std::string writeAsXML(const Ice::Current& c = ::Ice::Current());

        friend std::ostream& operator<<(std::ostream& stream, const Pose& rhs)
        {
            stream << "Pose: " << std::endl << rhs.output() << std::endl;
            return stream;
        };

    public: // serialization
        virtual void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());

    protected:
        void init();
        void ice_postUnmarshal();
    private:
        //! To void unnecessary upcasts
        QuaternionPtr c_orientation;
        Vector3Ptr c_position;

    };

    typedef IceInternal::Handle<Pose> PosePtr;

}
#endif
