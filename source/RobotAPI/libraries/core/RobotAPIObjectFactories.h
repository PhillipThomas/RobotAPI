/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::ROBOTAPI
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_ROBOTAPI_OBJECT_FACTORIES_H
#define _ARMARX_ROBOTAPI_OBJECT_FACTORIES_H

#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/LinkedPose.h>
#include <Ice/Ice.h>
#include <RobotAPI/libraries/core/observerfilters/PoseMedianFilter.h>
#include <RobotAPI/libraries/core/observerfilters/OffsetFilter.h>
#include <RobotAPI/libraries/core/observerfilters/MatrixFilters.h>

namespace armarx
{
    class QuaternionObjectFactory : public Ice::ObjectFactory
    {
    public:
        virtual Ice::ObjectPtr create(const std::string& type)
        {
            assert(type == armarx::QuaternionBase::ice_staticId());
            return new Quaternion();
        }
        virtual void destroy()
        {}
    };

    class Vector3ObjectFactory : public Ice::ObjectFactory
    {
    public:
        virtual Ice::ObjectPtr create(const std::string& type)
        {
            assert(type == armarx::Vector3::ice_staticId());
            return new Vector3();
        }
        virtual void destroy()
        {}
    };

    class FramedDirectionObjectFactory : public Ice::ObjectFactory
    {
    public:
        virtual Ice::ObjectPtr create(const std::string& type)
        {
            assert(type == armarx::FramedDirectionBase::ice_staticId());
            return new FramedDirection();
        }
        virtual void destroy()
        {}
    };

    class LinkedDirectionObjectFactory : public Ice::ObjectFactory
    {
    public:
        virtual Ice::ObjectPtr create(const std::string& type)
        {
            assert(type == armarx::LinkedDirectionBase::ice_staticId());
            return new LinkedDirection();
        }
        virtual void destroy()
        {}
    };

    class PoseObjectFactory : public Ice::ObjectFactory
    {
    public:
        virtual Ice::ObjectPtr create(const std::string& type)
        {
            assert(type == armarx::PoseBase::ice_staticId());
            return new Pose();
        }
        virtual void destroy()
        {}
    };

    class FramedPoseObjectFactory : public Ice::ObjectFactory
    {
    public:
        virtual Ice::ObjectPtr create(const std::string& type)
        {
            assert(type == armarx::FramedPoseBase::ice_staticId());
            return new FramedPose();
        }
        virtual void destroy()
        {}
    };

    class FramedPositionObjectFactory : public Ice::ObjectFactory
    {
    public:
        virtual Ice::ObjectPtr create(const std::string& type)
        {
            assert(type == armarx::FramedPositionBase::ice_staticId());
            return new FramedPosition();
        }
        virtual void destroy()
        {}
    };

    class FramedOrientationObjectFactory : public Ice::ObjectFactory
    {
    public:
        virtual Ice::ObjectPtr create(const std::string& type)
        {
            assert(type == armarx::FramedOrientationBase::ice_staticId());
            return new FramedOrientation();
        }
        virtual void destroy()
        {}
    };

    class LinkedPoseObjectFactory : public Ice::ObjectFactory
    {
    public:
        virtual Ice::ObjectPtr create(const std::string& type)
        {
            assert(type == armarx::LinkedPoseBase::ice_staticId());
            return new LinkedPose();
        }
        virtual void destroy()
        {}
    };



    namespace ObjectFactories
    {

        /**
         * @class RobotAPIObjectFactories
         */
        class RobotAPIObjectFactories : public FactoryCollectionBase
        {
        public:
            ObjectFactoryMap getFactories()
            {
                ObjectFactoryMap map;

                map.insert(std::make_pair(armarx::Vector3Base::ice_staticId(), new Vector3ObjectFactory));
                map.insert(std::make_pair(armarx::FramedDirectionBase::ice_staticId(), new FramedDirectionObjectFactory));
                map.insert(std::make_pair(armarx::LinkedDirectionBase::ice_staticId(), new LinkedDirectionObjectFactory));
                map.insert(std::make_pair(armarx::QuaternionBase::ice_staticId(), new QuaternionObjectFactory));
                map.insert(std::make_pair(armarx::PoseBase::ice_staticId(), new PoseObjectFactory));
                map.insert(std::make_pair(armarx::FramedPoseBase::ice_staticId(), new FramedPoseObjectFactory));
                map.insert(std::make_pair(armarx::FramedOrientationBase::ice_staticId(), new FramedOrientationObjectFactory));
                map.insert(std::make_pair(armarx::FramedPositionBase::ice_staticId(), new FramedPositionObjectFactory));
                map.insert(std::make_pair(armarx::LinkedPoseBase::ice_staticId(), new LinkedPoseObjectFactory));

                add<armarx::PoseMedianFilterBase, armarx::filters::PoseMedianFilter>(map);
                add<armarx::OffsetFilterBase, armarx::filters::OffsetFilter>(map);
                add<armarx::MatrixMaxFilterBase, armarx::filters::MatrixMaxFilter>(map);
                add<armarx::MatrixMinFilterBase, armarx::filters::MatrixMinFilter>(map);
                add<armarx::MatrixAvgFilterBase, armarx::filters::MatrixAvgFilter>(map);
                add<armarx::MatrixPercentileFilterBase, armarx::filters::MatrixPercentileFilter>(map);
                add<armarx::MatrixPercentilesFilterBase, armarx::filters::MatrixPercentilesFilter>(map);
                add<armarx::MatrixCumulativeFrequencyFilterBase, armarx::filters::MatrixCumulativeFrequencyFilter>(map);

                return map;
            }
            static const FactoryCollectionBaseCleanUp RobotAPIObjectFactoriesVar;
        };

    }

}

#endif
