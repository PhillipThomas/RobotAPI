/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    Armar4::api
* @author     Nikolaus Vahrenkamp
* @date       2012 Nikolaus Vahrenkamp
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RobotStatechartContext.h"

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    // ****************************************************************
    // Implementation of Component
    // ****************************************************************
    void RobotStatechartContext::onInitStatechartContext()
    {
        //        StatechartContext::onInitStatechart();
        ARMARX_LOG << eINFO << "Init RobotStatechartContext" << flush;

        kinematicUnitObserverName = getProperty<std::string>("KinematicUnitObserverName").getValue();

        usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
        usingProxy("RobotStateComponent");
        usingProxy(kinematicUnitObserverName);

        if (!getProperty<std::string>("HandUnits").getValue().empty())
        {
            std::string handUnitsProp = getProperty<std::string>("HandUnits").getValue();
            std::vector<std::string> handUnitList;
            boost::split(handUnitList, handUnitsProp, boost::is_any_of(","));

            for (size_t i = 0; i < handUnitList.size(); i++)
            {
                boost::algorithm::trim(handUnitList.at(i));
                usingProxy(handUnitList.at(i));
            }
        }

        // headIKUnit
        headIKUnitName = getProperty<std::string>("HeadIKUnitName").getValue();
        headIKKinematicChainName = getProperty<std::string>("HeadIKKinematicChainName").getValue();

        if (!headIKUnitName.empty())
        {
            usingProxy(headIKUnitName);
        }
    }


    void RobotStatechartContext::onConnectStatechartContext()
    {
        //        StatechartContext::onConnectStatechart();
        ARMARX_LOG << eINFO << "Starting RobotStatechartContext" << flush;

        // retrieve proxies
        std::string rbStateName = "RobotStateComponent";
        robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(rbStateName);
        std::string kinUnitName = getProperty<std::string>("KinematicUnitName").getValue();
        kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(kinUnitName);
        kinematicUnitObserverPrx = getProxy<KinematicUnitObserverInterfacePrx>(kinematicUnitObserverName);

        ARMARX_LOG << eINFO << "Fetched proxies " << kinUnitName << ":" << kinematicUnitPrx << ", " << rbStateName << ": " << robotStateComponent << flush;

        if (!headIKUnitName.empty())
        {
            headIKUnitPrx = getProxy<HeadIKUnitInterfacePrx>(headIKUnitName);
            ARMARX_LOG << eINFO << "Fetched headIK proxy " << headIKUnitName << ":" << headIKUnitPrx << ", head IK kin chain:" << headIKKinematicChainName << flush;
        }


        if (!getProperty<std::string>("HandUnits").getValue().empty())
        {
            std::string handUnitsProp = getProperty<std::string>("HandUnits").getValue();
            std::vector<std::string> handUnitList;
            boost::split(handUnitList, handUnitsProp, boost::is_any_of(","));

            for (size_t i = 0; i < handUnitList.size(); i++)
            {
                boost::algorithm::trim(handUnitList.at(i));
                HandUnitInterfacePrx handPrx = getProxy<HandUnitInterfacePrx>(handUnitList.at(i));
                handUnits[handUnitList.at(i)] = handPrx;
                ARMARX_LOG << eINFO << "Fetched handUnit proxy " << handUnitList.at(i) << ": " << handPrx << flush;
            }
        }

        // initialize remote robot
        remoteRobot.reset(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));
        ARMARX_LOG << eINFO << "Created remote robot" << flush;
    }

    PropertyDefinitionsPtr RobotStatechartContext::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new RobotStatechartContextProperties(
                                          getConfigIdentifier()));
    }

    HandUnitInterfacePrx RobotStatechartContext::getHandUnit(const std::string& handUnitName)
    {
        if (handUnits.find(handUnitName) != handUnits.end())
        {
            ARMARX_LOG << eINFO << "Found proxy of hand unit with name  " << handUnitName << flush;
            return handUnits[handUnitName];
        }

        ARMARX_LOG << eINFO << "Do not know proxy of hand unit with name  " << handUnitName << flush;
        std::map<std::string, HandUnitInterfacePrx>::iterator it = handUnits.begin();

        while (it != handUnits.end())
        {
            ARMARX_LOG << eINFO << "************ Known hand units: " << it->first << ":" << it->second << flush;
            it++;
        }

        return HandUnitInterfacePrx();
    }

    /* const VirtualRobot::RobotPtr armarx::Armar4Context::getRobot()
     {
         return remoteRobot;
     }*/

}

