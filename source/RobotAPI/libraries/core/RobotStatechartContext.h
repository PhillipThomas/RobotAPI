/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    Armar4::api
* @author     Nikolaus Vahrenkamp
* @date       2012 Nikolaus Vahrenkamp
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#ifndef ARMARX_COMPONENT_RobotApi_StatechartContext_H
#define ARMARX_COMPONENT_RobotApi_StatechartContext_H

#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/units/HandUnitInterface.h>
#include <RobotAPI/interface/units/TCPControlUnit.h>
#include <RobotAPI/interface/units/HeadIKUnit.h>
#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/StatechartContext.h>

#include <IceUtil/Time.h>

namespace armarx
{

    // ****************************************************************
    // Component and context
    // ****************************************************************

    struct RobotStatechartContextProperties : StatechartContextPropertyDefinitions
    {
        RobotStatechartContextProperties(std::string prefix):
            StatechartContextPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the kinematic unit that should be used");
            defineRequiredProperty<std::string>("KinematicUnitObserverName", "Name of the kinematic unit observer that should be used");
            //HandUnits should only be changed via config file and default parameter should remain empty
            defineOptionalProperty<std::string>("HandUnits", "", "Name of the comma-seperated hand units that should be used. Unitname for left hand should be LeftHandUnit, and for right hand RightHandUnit");
            defineOptionalProperty<std::string>("HeadIKUnitName", "", "Name of the head unit that should be used.");
            defineOptionalProperty<std::string>("HeadIKKinematicChainName", "", "Name of the kinematic chain that should be used for head IK.");
        }
    };

    /**
      * @class HumanoidRobotStatechartContext is the implementation of the StatechartContext
      * for a HumanoidRobot
      */
    class ARMARXCOMPONENT_IMPORT_EXPORT RobotStatechartContext :
        virtual public StatechartContext
    {
    public:
        // inherited from Component
        virtual std::string getDefaultName()
        {
            return "RobotStatechartContext";
        }
        virtual void onInitStatechartContext();
        virtual void onConnectStatechartContext();

        // todo:read access should only be allowed via const getters ?!
        //const VirtualRobot::RobotPtr getRobot();
        //private:


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        std::string getKinematicUnitObserverName()
        {
            return kinematicUnitObserverName;
        }

        HandUnitInterfacePrx getHandUnit(const std::string& handUnitName);

        //! Prx for the RobotState
        RobotStateComponentInterfacePrx robotStateComponent;
        KinematicUnitInterfacePrx kinematicUnitPrx;
        KinematicUnitObserverInterfacePrx kinematicUnitObserverPrx;
        TCPControlUnitInterfacePrx tcpControlPrx;
        VirtualRobot::RobotPtr remoteRobot;
        std::map<std::string, HandUnitInterfacePrx> handUnits;

        HeadIKUnitInterfacePrx headIKUnitPrx;
        std::string headIKKinematicChainName;

    private:
        std::string kinematicUnitObserverName;
        std::string headIKUnitName;
    };
}

#endif
