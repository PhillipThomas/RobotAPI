#ifndef _ARMARX_ROBOTAPI_CONDITIONCHECKEQUALSPOSE_H
#define _ARMARX_ROBOTAPI_CONDITIONCHECKEQUALSPOSE_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/ConditionCheck.h>
#include <ArmarXCore/core/FramedPose.h>

namespace armarx
{
    class ARMARXCORE_IMPORT_EXPORT ConditionCheckEqualsPose :
        public ConditionCheck
    {
    public:
        ConditionCheckEqualsPose()
        {
            setNumberParameters(1);
            addSupportedType(VariantType::FramedPosition, createParameterTypeList(1, VariantType::FramedPosition);
                             addSupportedType(VariantType::FramedOrientation, createParameterTypeList(1, VariantType::FramedOrientation);
                                              addSupportedType(VariantType::Vector3, createParameterTypeList(1, VariantType::Vector3);
                                                      addSupportedType(VariantType::Quaternion, createParameterTypeList(1, VariantType::Quaternion);
                                                              addSupportedType(VariantType::Pose, createParameterTypeList(1, VariantType::Pose);
        }

                                                          ConditionCheck* clone()
        {
            return new ConditionCheckEqualsPose(*this);
        }

        bool evaluate(const StringVariantMap& dataFields)
        {
            if (dataFields.size() != 1)
            {
                printf("Size of dataFields: %d\n", (int)dataFields.size());
                throw InvalidConditionException("Wrong number of datafields for condition equals ");
            }

            Variant& value = dataFields.begin()->second;
            VariantTypeId type = value.getType();

            if (type == VariantType::Vector3)
                return (sqrt(((value.x - getParameter(0).getClass<Vector3>().x) * (value.x - getParameter(0).getClass<Vector3>().x)) +
                             ((value.y - getParameter(0).getClass<Vector3>().y) * (value.y - getParameter(0).getClass<Vector3>().y)) +
                             ((value.z - getParameter(0).getClass<Vector3>().x) * (value.x - getParameter(0).getClass<Vector3>().z))) == 0);

            if (type == VariantType::FramedPosition)
                return (sqrt(((value.x - getParameter(0).getClass<FramedPosition>().x) * (value.x - getParameter(0).getClass<FramedPosition>().x)) +
                             ((value.y - getParameter(0).getClass<FramedPosition>().y) * (value.y - getParameter(0).getClass<FramedPosition>().y)) +
                             ((value.z - getParameter(0).getClass<FramedPosition>().x) * (value.x - getParameter(0).getClass<FramedPosition>().z))) == 0);

            if (type == VariantType::Quaternion)
                return (sqrt(((value.qw - getParameter(0).getClass<Quaternion>().qw) * (value.qw - getParameter(0).getClass<Quaternion>().qw)) +
                             ((value.qx - getParameter(0).getClass<Quaternion>().qx) * (value.qx - getParameter(0).getClass<Quaternion>().qx)) +
                             ((value.qy - getParameter(0).getClass<Quaternion>().qy) * (value.qy - getParameter(0).getClass<Quaternion>().qy)) +
                             ((value.qz - getParameter(0).getClass<Quaternion>().qx) * (value.qx - getParameter(0).getClass<Quaternion>().qz))) == 0);

            if (type == VariantType::FramedOrientation)
                return (sqrt(((value.qw - getParameter(0).getClass<FramedOrientation>().qw) * (value.qw - getParameter(0).getClass<FramedOrientation>().qw)) +
                             ((value.qx - getParameter(0).getClass<FramedOrientation>().qx) * (value.qx - getParameter(0).getClass<FramedOrientation>().qx)) +
                             ((value.qy - getParameter(0).getClass<FramedOrientation>().qy) * (value.qy - getParameter(0).getClass<FramedOrientation>().qy)) +
                             ((value.qz - getParameter(0).getClass<FramedOrientation>().qx) * (value.qx - getParameter(0).getClass<FramedOrientation>().qz))) == 0);

            if (type == VariantType::Pose)
                return (sqrt(((value.x - getParameter(0).getClass<Pose>().x) * (value.x - getParameter(0).getClass<Pose>().x)) +
                             ((value.y - getParameter(0).getClass<Pose>().y) * (value.y - getParameter(0).getClass<Pose>().y)) +
                             ((value.z - getParameter(0).getClass<Pose>().x) * (value.x - getParameter(0).getClass<Pose>().z))) +
                        sqrt(((value.qw - getParameter(0).getClass<Pose>().qw) * (value.qw - getParameter(0).getClass<Pose>().qw)) +
                             ((value.qx - getParameter(0).getClass<Pose>().qx) * (value.qx - getParameter(0).getClass<Pose>().qx)) +
                             ((value.qy - getParameter(0).getClass<Pose>().qy) * (value.qy - getParameter(0).getClass<Pose>().qy)) +
                             ((value.qz - getParameter(0).getClass<Pose>().qx) * (value.qx - getParameter(0).getClass<Pose>().qz))) == 0);

            return false;
        }
    };
}
#endif // CONDITIONCHECKEQUALSPOSITION_H
