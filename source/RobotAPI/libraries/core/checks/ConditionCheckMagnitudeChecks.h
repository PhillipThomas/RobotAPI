#ifndef ARMARX_CONDITIONCHECKMAGNITUDECHECKS_H
#define ARMARX_CONDITIONCHECKMAGNITUDECHECKS_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/ConditionCheck.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/LinkedPose.h>

namespace armarx
{

    class ConditionCheckMagnitudeLarger : public ConditionCheck, Logging
    {
    public:
        ConditionCheckMagnitudeLarger();

        ConditionCheck* clone();
        bool evaluate(const StringVariantMap& dataFields);
    };

    class ConditionCheckMagnitudeSmaller : public ConditionCheck, Logging
    {
    public:
        ConditionCheckMagnitudeSmaller();

        ConditionCheck* clone();
        bool evaluate(const StringVariantMap& dataFields);
    };

    class ConditionCheckMagnitudeInRange : public ConditionCheck, Logging
    {
    public:
        ConditionCheckMagnitudeInRange();

        ConditionCheck* clone();
        bool evaluate(const StringVariantMap& dataFields);
    };

}
#endif
