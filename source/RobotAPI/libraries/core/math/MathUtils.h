/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef ARMARX_ROBOTAPI_MATH_MATHUTILS_H_
#define ARMARX_ROBOTAPI_MATH_MATHUTILS_H_

#include <math.h>
#include <Eigen/Eigen>
#include <vector>

namespace armarx
{
    namespace math
    {
        class MathUtils
        {
        public:
            static int LimitMinMax(int min, int max, int value)
            {
                return value < min ? min : (value > max ? max : value);
            }
            static float LimitMinMax(float min, float max, float value)
            {
                return value < min ? min : (value > max ? max : value);
            }
            static double LimitMinMax(double min, double max, double value)
            {
                return value < min ? min : (value > max ? max : value);
            }
            static Eigen::Vector3f LimitMinMax(const Eigen::Vector3f& min, const Eigen::Vector3f& max, const Eigen::Vector3f& value)
            {
                return Eigen::Vector3f(LimitMinMax(min(0), max(0), value(0)), LimitMinMax(min(1), max(1), value(1)), LimitMinMax(min(2), max(2), value(2)));
            }

            static bool CheckMinMax(int min, int max, int value)
            {
                return value >= min && value <= max;
            }
            static bool CheckMinMax(float min, float max, float value)
            {
                return value >= min && value <= max;
            }
            static bool CheckMinMax(double min, double max, double value)
            {
                return value >= min && value <= max;
            }
            static bool CheckMinMax(const Eigen::Vector3f& min, const Eigen::Vector3f& max, const Eigen::Vector3f& value)
            {
                return CheckMinMax(min(0), max(0), value(0)) && CheckMinMax(min(1), max(1), value(1)) && CheckMinMax(min(2), max(2), value(2));
            }

            static std::vector<float> VectorSubtract(const std::vector<float>& v1, const std::vector<float>& v2)
            {
                std::vector<float> result;

                for (size_t i = 0; i < v1.size() && i < v2.size(); i++)
                {
                    result.push_back(v1.at(i) - v2.at(i));
                }

                return result;
            }
            static std::vector<float> VectorAbsDiff(const std::vector<float>& v1, const std::vector<float>& v2)
            {
                std::vector<float> result;

                for (size_t i = 0; i < v1.size() && i < v2.size(); i++)
                {
                    result.push_back(std::fabs(v1.at(i) - v2.at(i)));
                }

                return result;
            }

            static float VectorMax(const std::vector<float>& vec)
            {
                float max = vec.at(0);

                for (size_t i = 1; i < vec.size(); i++)
                {
                    max = std::max(max, vec.at(i));
                }

                return max;
            }

        };
    }
}

#endif
