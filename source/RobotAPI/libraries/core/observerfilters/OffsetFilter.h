/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_ROBOTAPI_OFFSETFILTER_H
#define _ARMARX_ROBOTAPI_OFFSETFILTER_H

#include <RobotAPI/interface/observers/ObserverFilters.h>
#include <ArmarXCore/observers/filters/DatafieldFilter.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXCore/util/variants/eigen3/MatrixVariant.h>

namespace armarx
{
    namespace filters
    {

        /**
         * @class OffsetFilter
         * @ingroup ObserverFilters
         * @brief The OffsetFilter class returns values
         * relative to value from the first call of the filter.
         * E.g. this is useful for Forces which should be nulled
         * at a specific moment.
         */
        class OffsetFilter :
            public ::armarx::OffsetFilterBase,
            public DatafieldFilter
        {
        public:
            OffsetFilter()
            {
                firstRun = true;
                windowFilterSize = 1;
            }


            // DatafieldFilterBase interface
        public:
            VariantBasePtr calculate(const Ice::Current& c = Ice::Current()) const
            {

                VariantPtr newVariant;

                if (initialValue && dataHistory.size() > 0)
                {
                    VariantTypeId type = dataHistory.begin()->second->getType();
                    VariantPtr currentValue = VariantPtr::dynamicCast(dataHistory.rbegin()->second);

                    if (currentValue->getType() != initialValue->getType())
                    {
                        ARMARX_ERROR_S << "Types in OffsetFilter are different: " << Variant::typeToString(currentValue->getType()) << " and " << Variant::typeToString(initialValue->getType());
                        return NULL;
                    }

                    if (type == VariantType::Int)
                    {
                        int newValue = dataHistory.rbegin()->second->getInt() - initialValue->getInt();
                        newVariant = new Variant(newValue);
                    }
                    else if (type == VariantType::Float)
                    {
                        float newValue = dataHistory.rbegin()->second->getFloat() - initialValue->getFloat();
                        newVariant = new Variant(newValue);
                    }
                    else if (type == VariantType::Double)
                    {
                        double newValue = dataHistory.rbegin()->second->getDouble() - initialValue->getDouble();
                        newVariant = new Variant(newValue);
                    }
                    else if (type == VariantType::FramedDirection)
                    {
                        FramedDirectionPtr vec = FramedDirectionPtr::dynamicCast(currentValue->get<FramedDirection>());
                        FramedDirectionPtr intialVec = FramedDirectionPtr::dynamicCast(initialValue->get<FramedDirection>());
                        Eigen::Vector3f newValue =  vec->toEigen() - intialVec->toEigen();

                        newVariant = new Variant(new FramedDirection(newValue, vec->frame, vec->agent));
                    }
                    else if (type == VariantType::FramedPosition)
                    {
                        FramedPositionPtr pos = FramedPositionPtr::dynamicCast(currentValue->get<FramedPosition>());
                        FramedPositionPtr intialPos = FramedPositionPtr::dynamicCast(initialValue->get<FramedPosition>());
                        Eigen::Vector3f newValue =  pos->toEigen() - intialPos->toEigen();
                        newVariant = new Variant(new FramedPosition(newValue, pos->frame, pos->agent));
                    }
                    else if (type == VariantType::MatrixFloat)
                    {
                        MatrixFloatPtr matrix = MatrixFloatPtr::dynamicCast(currentValue->get<MatrixFloat>());
                        MatrixFloatPtr initialMatrix = MatrixFloatPtr::dynamicCast(initialValue->get<MatrixFloat>());
                        Eigen::MatrixXf newMatrix = matrix->toEigen() - initialMatrix->toEigen();
                        newVariant = new Variant(new MatrixFloat(newMatrix));
                    }
                }

                return newVariant;

            }
            ParameterTypeList getSupportedTypes(const Ice::Current&) const
            {
                ParameterTypeList result;
                result.push_back(VariantType::Int);
                result.push_back(VariantType::Float);
                result.push_back(VariantType::Double);
                result.push_back(VariantType::FramedDirection);
                result.push_back(VariantType::FramedPosition);
                result.push_back(VariantType::MatrixFloat);
                return result;
            }
        private:
            bool firstRun;
            VariantPtr initialValue;

            // DatafieldFilterBase interface
        public:
            void update(Ice::Long timestamp, const VariantBasePtr& value, const Ice::Current& c)
            {
                DatafieldFilter::update(timestamp, value, c);

                if (firstRun)
                {
                    if (dataHistory.size() == 0)
                    {
                        return;
                    }

                    initialValue = VariantPtr::dynamicCast(dataHistory.begin()->second);
                    firstRun = false;
                }
            }
        };
    }
}

#endif // _ARMARX_ROBOTAPI_OFFSETFILTER_H
