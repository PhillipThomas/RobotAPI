#ifndef _ARMARX_ROBOTAPI_MEDIANFILTER_H
#define _ARMARX_ROBOTAPI_MEDIANFILTER_H

#include <ArmarXCore/observers/filters/MedianFilter.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/interface/core/PoseBase.h>

namespace armarx
{
    namespace filters
    {

        /**
         * @class PoseMedianFilter
         * @ingroup ObserverFilters
         * @brief The MedianFilter class provides an implementation
         *  for a median for datafields of type float, int and double.
         */
        class PoseMedianFilter :
            public ::armarx::PoseMedianFilterBase,
            public MedianFilter
        {
        public:
            PoseMedianFilter(int windowSize = 11)
            {
                this->windowFilterSize = windowSize;
            }

            // DatafieldFilterBase interface
        public:
            VariantBasePtr calculate(const Ice::Current& c) const
            {
                if (dataHistory.size() == 0)
                {
                    return NULL;
                }

                VariantTypeId type = dataHistory.begin()->second->getType();

                if ((type == VariantType::Vector3) || (type == VariantType::FramedDirection) || (type == VariantType::FramedPosition))
                {

                    Eigen::Vector3f vec;
                    vec.setZero();
                    std::string frame = "";
                    std::string agent = "";
                    VariantPtr var = VariantPtr::dynamicCast(dataHistory.begin()->second);

                    if (type == VariantType::FramedDirection)
                    {
                        FramedDirectionPtr p = var->get<FramedDirection>();
                        frame = p->frame;
                        agent = p->agent;
                    }
                    else if (type == VariantType::FramedPosition)
                    {
                        FramedPositionPtr p = var->get<FramedPosition>();
                        frame = p->frame;
                        agent = p->agent;
                    }

                    for (int i = 0; i < 3; ++i)
                    {
                        std::vector<double> values;

                        for (auto v : dataHistory)
                        {
                            VariantPtr v2 = VariantPtr::dynamicCast(v.second);
                            values.push_back(v2->get<Vector3>()->toEigen()[i]);
                        }

                        std::sort(values.begin(), values.end());
                        vec[i] = values.at(values.size() / 2);
                    }

                    if (type == VariantType::Vector3)
                    {
                        Vector3Ptr vecVar = new Vector3(vec);
                        return new Variant(vecVar);
                    }
                    else if (type == VariantType::FramedDirection)
                    {

                        FramedDirectionPtr vecVar = new FramedDirection(vec, frame, agent);
                        return new Variant(vecVar);
                    }
                    else if (type == VariantType::FramedPosition)
                    {
                        FramedPositionPtr vecVar = new FramedPosition(vec, frame, agent);
                        return new Variant(vecVar);
                    }
                    else
                    {
                        ARMARX_WARNING_S << "Implementation missing here";
                        return NULL;
                    }
                }
                else if (type == VariantType::Double)
                {
                    //                    auto values = SortVariants<double>(dataHistory);
                    //                    return new Variant(values.at(values.size()/2));
                }
                else if (type == VariantType::Int)
                {
                    //                    auto values = SortVariants<int>(dataHistory);
                    //                    return new Variant(values.at(values.size()/2));
                }

                return MedianFilter::calculate(c);
            }

            /**
             * @brief This filter supports: Vector3, FramedDirection, FramedPosition
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current& c) const
            {
                ParameterTypeList result = MedianFilter::getSupportedTypes(c);
                result.push_back(VariantType::Vector3);
                result.push_back(VariantType::FramedDirection);
                result.push_back(VariantType::FramedPosition);
                return result;
            }

        };

    } // namespace Filters
}

#endif
