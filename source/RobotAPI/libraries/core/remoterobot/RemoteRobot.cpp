#include "RemoteRobot.h"
#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/Nodes/RobotNodeFixedFactory.h>
#include <VirtualRobot/Nodes/RobotNodePrismaticFactory.h>
#include <VirtualRobot/Nodes/RobotNodeRevoluteFactory.h>
#include <Eigen/Geometry>

//#define DMES(Obj) cout << format("[%1%|%2%|%3%] %4%") % __FILE__ % __LINE__ % __func__ % Obj << endl;
#define DMES(Obj) ;

using namespace std;
using namespace boost;
using namespace VirtualRobot;
using namespace Eigen;

namespace armarx
{

    boost::recursive_mutex RemoteRobot::m;

    RemoteRobot::RemoteRobot(SharedRobotInterfacePrx robot) :
        Robot(),
        _robot(robot)
    {
        _robot->ref();
    }

    RemoteRobot::~RemoteRobot()
    {
        try
        {
            _robot->unref();
        }
        catch (std::exception& e)
        {
            ARMARX_DEBUG_S << "Unref of SharedRobot failed: " << e.what();
        }
        catch (...)
        {
            ARMARX_DEBUG_S << "Unref of SharedRobot failed: reason unknown";
        }

    }

    RobotNodePtr RemoteRobot::getRootNode()
    {
        if (!_root)
        {
            _root = RemoteRobot::createRemoteRobotNode(_robot->getRootNode(), shared_from_this());
        }

        return _root;
    }

    bool RemoteRobot::hasRobotNode(const string& robotNodeName)
    {
        if (_cachedNodes.find(name) == _cachedNodes.end())
        {
            return _robot->hasRobotNode(robotNodeName);
        }
        else
        {
            return true;
        }
    }


    bool RemoteRobot::hasRobotNode(RobotNodePtr robotNode)
    {
        return this->hasRobotNode(robotNode->getName());

        /*
         * This just does not work. because you cannot tell wheter RemoteRobotNode<RobotNodeRevolute> or another type is used
         * perhaps you can infer the actual RobotNodeType somehow. Until now we just check for names which is
         * much faster!
         *
        if  ( (_cachedNodes.find(name)==_cachedNodes.end()) || _robot->hasRobotNode(robotNode->getName())) {
            shared_ptr<RemoteRobotNode> remoteNode(dynamic_pointer_cast<RemoteRobotNode>(robotNode));
            if (! remoteNode) return false;

            SharedRobotNodeInterfacePrx sharedNode = remoteNode->getSharedNode();
            SharedRobotNodeInterfacePrx otherSharedNode = dynamic_pointer_cast<RemoteRobotNode>(this->getRobotNode(robotNodeName))->getSharedNode();
            if (sharedNode == otherSharedNode)
                return true;
        }

        return false;
        */
    }


    RobotNodePtr RemoteRobot::getRobotNode(const string& robotNodeName)
    {
        DMES((format("Node: %1%") % robotNodeName));

        if (_cachedNodes.find(robotNodeName) == _cachedNodes.end())
        {
            DMES("No cache hit");
            _cachedNodes[robotNodeName] = RemoteRobot::createRemoteRobotNode(_robot->getRobotNode(robotNodeName),
                                          shared_from_this());
        }
        else
        {
            DMES("Cache hit");
        }

        return _cachedNodes[robotNodeName];
    }

    void RemoteRobot::getRobotNodes(vector< RobotNodePtr >& storeNodes, bool clearVector)
    {
        if (clearVector)
        {
            storeNodes.clear();
        }

        NameList nodes = _robot->getRobotNodes();
        BOOST_FOREACH(string name, nodes)
        {
            storeNodes.push_back(this->getRobotNode(name));
        }
    }

    bool RemoteRobot::hasRobotNodeSet(const string& name)
    {
        return _robot->hasRobotNodeSet(name);
    }

    RobotNodeSetPtr RemoteRobot::getRobotNodeSet(const string& nodeSetName)
    {
        vector<RobotNodePtr> storeNodes;
        RobotNodeSetInfoPtr info = _robot->getRobotNodeSet(nodeSetName);
        return RobotNodeSet::createRobotNodeSet(
                   shared_from_this(), nodeSetName, info->names, info->rootName, info->tcpName);
    }


    void RemoteRobot::getRobotNodeSets(vector<RobotNodeSetPtr>& storeNodeSet)
    {
        NameList sets = _robot->getRobotNodeSets();

        BOOST_FOREACH(string name, sets)
        {
            storeNodeSet.push_back(this->getRobotNodeSet(name));
        }
    }

    Matrix4f RemoteRobot::getGlobalPose() const
    {
        PosePtr p = PosePtr::dynamicCast(_robot->getGlobalPose());
        return p->toEigen(); // convert to eigen first
    }

    string RemoteRobot::getName()
    {
        return _robot->getName();
    }

    VirtualRobot::RobotNodePtr RemoteRobot::createLocalNode(SharedRobotNodeInterfacePrx remoteNode, std::vector<VirtualRobot::RobotNodePtr>& allNodes, std::map< VirtualRobot::RobotNodePtr, std::vector<std::string> >& childrenMap, RobotPtr robo)
    {
        boost::recursive_mutex::scoped_lock cloneLock(m);
        static int nonameCounter = 0;

        if (!remoteNode || !robo)
        {
            ARMARX_ERROR_S << " NULL data " << endl;
            return VirtualRobot::RobotNodePtr();
        }

        VirtualRobot::RobotNodeFactoryPtr revoluteNodeFactory = VirtualRobot::RobotNodeFactory::fromName(VirtualRobot::RobotNodeRevoluteFactory::getName(), NULL);
        VirtualRobot::RobotNodeFactoryPtr fixedNodeFactory = VirtualRobot::RobotNodeFactory::fromName(VirtualRobot::RobotNodeFixedFactory::getName(), NULL);
        VirtualRobot::RobotNodeFactoryPtr prismaticNodeFactory = VirtualRobot::RobotNodeFactory::fromName(VirtualRobot::RobotNodePrismaticFactory::getName(), NULL);

        Eigen::Matrix4f idMatrix = Eigen::Matrix4f::Identity();
        Eigen::Vector3f idVec3 = Eigen::Vector3f::Zero();
        std::string name = remoteNode->getName();

        if (name.empty())
        {
            ARMARX_LOG_S << "Node without name!!!" << endl;
            std::stringstream ss;
            ss << "robot_node_" << nonameCounter;
            nonameCounter++;
            name = ss.str();
        }

        VirtualRobot::RobotNodePtr result;
        PosePtr lTbase = PosePtr::dynamicCast(remoteNode->getLocalTransformation());
        Eigen::Matrix4f localTransform = lTbase->toEigen();

        //float jv = remoteNode->getJointValue();
        float jvLo = remoteNode->getJointLimitLow();
        float jvHi = remoteNode->getJointLimitHigh();
        float jointOffset = 0;//remoteNode->getJointOffset();

        JointType jt = remoteNode->getType();

        switch (jt)
        {
            case ePrismatic:
            {
                Vector3Ptr axisBase = Vector3Ptr::dynamicCast(remoteNode->getJointTranslationDirection());
                Eigen::Vector3f axis = axisBase->toEigen();
                // convert axis to local coord system
                Eigen::Vector4f result4f = Eigen::Vector4f::Zero();
                result4f.segment(0, 3) = axis;
                PosePtr gp = PosePtr::dynamicCast(remoteNode->getGlobalPose());
                result4f = gp->toEigen().inverse() * result4f;
                axis = result4f.block(0, 0, 3, 1);

                result = prismaticNodeFactory->createRobotNode(robo, name, VirtualRobot::VisualizationNodePtr(), VirtualRobot::CollisionModelPtr(),
                         jvLo, jvHi, jointOffset, idMatrix, idVec3, axis);
            }
            break;

            case eFixed:
                result = fixedNodeFactory->createRobotNode(robo, name, VirtualRobot::VisualizationNodePtr(), VirtualRobot::CollisionModelPtr(), 0,
                         0, 0, localTransform, idVec3, idVec3);
                break;

            case eRevolute:
            {
                Vector3Ptr axisBase = Vector3Ptr::dynamicCast(remoteNode->getJointRotationAxis());
                Eigen::Vector3f axis = axisBase->toEigen();
                // convert axis to local coord system
                Eigen::Vector4f result4f = Eigen::Vector4f::Zero();
                result4f.segment(0, 3) = axis;
                PosePtr gp = PosePtr::dynamicCast(remoteNode->getGlobalPose());
                result4f = gp->toEigen().inverse() * result4f;
                axis = result4f.block(0, 0, 3, 1);

                result = revoluteNodeFactory->createRobotNode(robo, name, VirtualRobot::VisualizationNodePtr(), VirtualRobot::CollisionModelPtr(),
                         jvLo, jvHi, jointOffset, localTransform, axis, idVec3);
            }
            break;

            default:
                ARMARX_ERROR_S << "JointType nyi..." << endl;
                return VirtualRobot::RobotNodePtr();
                break;
        }

        robo->registerRobotNode(result);
        allNodes.push_back(result);


        // setup joint->nextNodes children
        std::vector<std::string> childrenBase = remoteNode->getChildren();
        std::vector<std::string> children;

        // check for RobotNodes (sensors do also register as children!)
        for (size_t i = 0; i < childrenBase.size(); i++)
        {
            if (_robot->hasRobotNode(childrenBase[i]))
            {
                SharedRobotNodeInterfacePrx rnRemote = _robot->getRobotNode(childrenBase[i]);
                VirtualRobot::RobotNodePtr localNode = createLocalNode(rnRemote, allNodes, childrenMap, robo);
                /* boost::shared_ptr< RemoteRobotNode<VirtualRobotNodeType> > rnRemote = boost::dynamic_pointer_cast<RemoteRobotNode>(rnRemoteBase);
                if (!rnRemote)
                {
                    ARMARX_ERROR_S << "RemoteRobot does not know robot node with name " << children[i] << endl;
                    continue;
                }*/


                if (!localNode)
                {
                    ARMARX_ERROR_S << "Could not create local node: " << children[i] << endl;
                    continue;
                }

                children.push_back(childrenBase[i]);
            }
        }

        childrenMap[result] = children;
        return result;
    }

    VirtualRobot::RobotPtr RemoteRobot::createLocalClone()
    {
        //ARMARX_IMPORTANT_S << "RemoteRobot local clone" << flush;
        boost::recursive_mutex::scoped_lock cloneLock(m);
        std::string robotType = getName();
        std::string robotName = getName();
        VirtualRobot::RobotPtr robo(new VirtualRobot::LocalRobot(robotName, robotType));

        //RobotNodePtr
        SharedRobotNodeInterfacePrx root = _robot->getRootNode();

        std::vector<VirtualRobot::RobotNodePtr> allNodes;
        std::map< VirtualRobot::RobotNodePtr, std::vector<std::string> > childrenMap;

        VirtualRobot::RobotNodePtr rootLocal = createLocalNode(root, allNodes, childrenMap, robo);

        bool res = VirtualRobot::RobotFactory::initializeRobot(robo, allNodes, childrenMap, rootLocal);

        if (!res)
        {
            ARMARX_ERROR_S << "Failed to initialize local robot..." << endl;
            return VirtualRobot::RobotPtr();
        }

        // clone rns
        std::vector<std::string> rns = _robot->getRobotNodeSets();

        for (size_t i = 0; i < rns.size(); i++)
        {
            RobotNodeSetInfoPtr rnsInfo = _robot->getRobotNodeSet(rns[i]);
            RobotNodeSet::createRobotNodeSet(robo, rnsInfo->name, rnsInfo->names, rnsInfo->rootName, rnsInfo->tcpName, true);
        }

        //ARMARX_IMPORTANT_S << "RemoteRobot local clone end" << flush;
        auto pose = PosePtr::dynamicCast(_robot->getGlobalPose());
        robo->setGlobalPose(pose->toEigen());
        return robo;
    }

    VirtualRobot::RobotPtr RemoteRobot::createLocalClone(RobotStateComponentInterfacePrx robotStatePrx, const string& filename)
    {
        return createLocalClone(robotStatePrx->getSynchronizedRobot(), filename);
    }

    RobotPtr RemoteRobot::createLocalClone(SharedRobotInterfacePrx sharedRobotPrx, const string& filename)
    {
        boost::recursive_mutex::scoped_lock cloneLock(m);
        ARMARX_VERBOSE_S << "Creating local clone of remote robot (filename:" << filename << ")" << endl;
        VirtualRobot::RobotPtr result;

        if (!sharedRobotPrx)
        {
            ARMARX_ERROR_S << "NULL sharedRobotPrx. Aborting..." << endl;
            return result;
        }

        if (filename.empty())
        {
            RemoteRobotPtr rob(new RemoteRobot(sharedRobotPrx));
            result = rob->createLocalClone();

            if (!result)
            {
                ARMARX_ERROR_S << "Could not create local clone. Aborting..." << endl;
                return result;
            }
        }
        else
        {
            result = RobotIO::loadRobot(filename);

            if (!result)
            {
                ARMARX_ERROR_S << "Could not load robot file " << filename << ". Aborting..." << endl;
                return result;
            }
        }

        synchronizeLocalClone(result, sharedRobotPrx);
        return result;
    }

    bool RemoteRobot::synchronizeLocalClone(VirtualRobot::RobotPtr robot, RobotStateComponentInterfacePrx robotStatePrx)
    {
        return synchronizeLocalClone(robot, robotStatePrx->getSynchronizedRobot());
    }

    bool RemoteRobot::synchronizeLocalClone(VirtualRobot::RobotPtr robot, SharedRobotInterfacePrx sharedRobotPrx)
    {
        if (!sharedRobotPrx || !robot)
        {
            ARMARX_ERROR_S << "NULL data. Aborting..." << endl;
            return false;
        }

        RobotConfigPtr c(new RobotConfig(robot, "synchronizeLocalClone"));
        NameValueMap jv = sharedRobotPrx->getConfig();

        for (NameValueMap::const_iterator it = jv.begin(); it != jv.end(); it++)
        {
            // joint values
            const std::string& jointName = it->first;
            float jointAngle = it->second;

            if (!c->setConfig(jointName, jointAngle))
            {
                ARMARX_WARNING_S << "Joint not known in local copy:" << jointName << ". Skipping..." << endl;
            }
        }

        robot->setConfig(c);
        auto pose = PosePtr::dynamicCast(sharedRobotPrx->getGlobalPose());
        robot->setGlobalPose(pose->toEigen());
        return true;
    }


    // Private (unused methods)

    bool RemoteRobot::hasEndEffector(const string& endEffectorName)
    {
        return false;
    }

    EndEffectorPtr RemoteRobot::getEndEffector(const string& endEffectorName)
    {
        return EndEffectorPtr();
    }

    void RemoteRobot::getEndEffectors(vector<EndEffectorPtr>& storeEEF) {}
    void RemoteRobot::setRootNode(RobotNodePtr node) {}
    void RemoteRobot::registerRobotNode(RobotNodePtr node) {}
    void RemoteRobot::deregisterRobotNode(RobotNodePtr node) {}
    void RemoteRobot::registerRobotNodeSet(RobotNodeSetPtr nodeSet) {}
    void RemoteRobot::deregisterRobotNodeSet(RobotNodeSetPtr nodeSet) {}
    void RemoteRobot::registerEndEffector(EndEffectorPtr endEffector) {}

    void RemoteRobot::setGlobalPose(const Eigen::Matrix4f& globalPose, bool applyValues)
    {
        if (_robot)
        {
            _robot->setGlobalPose(new Pose(globalPose));
        }
    }

}
