/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::
 * @author     (stefan dot ulbrich at kit dot edu)
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_REMOTE_ROBOT_H__
#define _ARMARX_REMOTE_ROBOT_H__


#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/Nodes/RobotNodeRevolute.h>
#include <VirtualRobot/Nodes/RobotNodePrismatic.h>
#include <VirtualRobot/Nodes/RobotNodeFixed.h>
#include <VirtualRobot/CollisionDetection/CollisionChecker.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/interface/core/RobotState.h>

// boost
#include <boost/thread/mutex.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>

namespace armarx
{
    // forward declaration of RemoteRobotNode
    template<class VirtualRobotNodeType> class RemoteRobotNode;

    /** @brief RemoteRobotNodeInitializer is used to initialize the robot node for a given node type.
     *  For each robot type to be supported make a specialization of the initialize function.
     * Currently supports: RobotNodeRevolute, RobotNodePrismatic, RobotNodeFixed. Node type specific
     * initializations go here.
     */
    template<typename VirtualRobotNodeType>
    struct RemoteRobotNodeInitializer
    {
        static void initialize(RemoteRobotNode<VirtualRobotNodeType>* remoteNode);
    };

    // specializations
    template<>
    void RemoteRobotNodeInitializer<VirtualRobot::RobotNodeRevolute>::initialize(RemoteRobotNode<VirtualRobot::RobotNodeRevolute>* remoteNode);
    template<>
    void RemoteRobotNodeInitializer<VirtualRobot::RobotNodePrismatic>::initialize(RemoteRobotNode<VirtualRobot::RobotNodePrismatic>* remoteNode);
    template<>
    void RemoteRobotNodeInitializer<VirtualRobot::RobotNodeFixed>::initialize(RemoteRobotNode<VirtualRobot::RobotNodeFixed>* remoteNode);


    /** @brief Mimics the behaviour of robot nodes while redirecting everything to Ice proxies.
     * @tparam VirtualRobotNodeType Must be a descendant of VirtualRobot::RobotNode
     * @details This class is for <b> internal use only</b> as classes cannot be referenced!
     */
    template<class VirtualRobotNodeType>
    class RemoteRobotNode :
        public VirtualRobotNodeType
    {
        friend struct RemoteRobotNodeInitializer<VirtualRobotNodeType>;

    public:
        RemoteRobotNode(SharedRobotNodeInterfacePrx node, VirtualRobot::RobotPtr vr) :
            _node(node)
        {
            _node->ref();
            this->name = _node->getName();
            this->robot = vr;
            _node->getJointValueOffest();
            setJointLimits(_node->getJointLimitLow(), _node->getJointLimitHigh());

            this->collisionChecker = VirtualRobot::CollisionChecker::getGlobalCollisionChecker();

            RemoteRobotNodeInitializer<VirtualRobotNodeType>::initialize(this);
        }

        virtual ~RemoteRobotNode();

        virtual float getJointValue() const;
        virtual float getJointLimitHi() const;
        virtual float getJointLimitLo() const;

        virtual Eigen::Matrix4f getLocalTransformation();

        virtual Eigen::Matrix4f getGlobalPose() const;
        virtual Eigen::Matrix4f getPoseInRootFrame() const;
        virtual Eigen::Vector3f getPositionInRootFrame() const;
        virtual bool hasChildNode(const std::string& child, bool recursive = false) const;

        virtual std::vector<VirtualRobot::RobotNodePtr> getAllParents(VirtualRobot::RobotNodeSetPtr rns);
        virtual VirtualRobot::RobotNodePtr getParent();
        inline SharedRobotNodeInterfacePrx getSharedRobotNode()
        {
            return _node;
        }

        virtual std::vector<std::string> getChildrenNames() const;

    protected:
        ///////////////////////// SETUP ////////////////////////////////////
        virtual void setJointLimits(float lo, float hi);
        //virtual void setPostJointTransformation(const Eigen::Matrix4f &trafo);
        virtual void setLocalTransformation(const Eigen::Matrix4f& trafo);

        virtual std::string getParentName() const;
        virtual std::vector< VirtualRobot::SceneObjectPtr> getChildren() const;

        virtual void updateTransformationMatrices();
        virtual void updateTransformationMatrices(const Eigen::Matrix4f& globalPose);


        virtual bool hasChildNode(const VirtualRobot::RobotNodePtr child, bool recursive = false) const;
        virtual void addChildNode(VirtualRobot::RobotNodePtr child);
        virtual bool initialize(VirtualRobot::RobotNodePtr parent, bool initializeChildren = false);
        virtual void reset();
        virtual void setGlobalPose(const Eigen::Matrix4f& pose);
        virtual void setJointValue(float q, bool updateTransformations = true, bool clampToLimits = true);

        SharedRobotNodeInterfacePrx _node;
    };

    /** @brief Mimics the behaviour of the VirtualRobot::Robot class while redirecting everything to an Ice proxy.
     * @details For a description of the  members please refer to the Simox documentation (VirtualRobot::Robot).
     * Note that not the complete interface has been made available yet. These functions are marked as protected.
     */
    class RemoteRobot : public VirtualRobot::Robot
    {
        template<class T>
        friend void boost::checked_delete(T*);
    protected:
        ~RemoteRobot();
    public:
        RemoteRobot(SharedRobotInterfacePrx robot);


        virtual VirtualRobot::RobotNodePtr getRootNode();

        virtual bool hasRobotNode(const std::string& robotNodeName);
        virtual bool hasRobotNode(VirtualRobot::RobotNodePtr);

        virtual VirtualRobot::RobotNodePtr getRobotNode(const std::string& robotNodeName);
        virtual void getRobotNodes(std::vector< VirtualRobot::RobotNodePtr >& storeNodes, bool clearVector = true);

        virtual bool hasRobotNodeSet(const std::string& name);
        virtual VirtualRobot::RobotNodeSetPtr getRobotNodeSet(const std::string& nodeSetName);
        virtual void getRobotNodeSets(std::vector<VirtualRobot::RobotNodeSetPtr>& storeNodeSet);

        /**
         *
         * @return Global pose of the robot
         */
        Eigen::Matrix4f getGlobalPose() const;



        /// Use this method to share the robot instance over Ice.
        inline SharedRobotInterfacePrx getSharedRobot()
        {
            return this->_robot;
        }

        std::string getName();

        /*!
              Creates a local robot that is synchronized once but not updated any more. You can use the synchronizeLocalClone method to update the joint values.
              Note that only the kinematic structure is cloned, but the visualization files, collision mdoels, end effectors, etc are not copied.
              This means you can use this model for kinematic computations (e.g. coordinate transformations) but not for advanced features like collision detection.
              In order to get a fully featured robot model you can pass a filename to VirtualRobot::Robot,
              but then you need to make sure that the loaded model is identical to the model of the remote robot (otherwise errors will occur).
            */
        static VirtualRobot::RobotPtr createLocalClone(RobotStateComponentInterfacePrx robotStatePrx, const std::string& filename = std::string());

        static VirtualRobot::RobotPtr createLocalClone(SharedRobotInterfacePrx sharedRobotPrx, const std::string& filename = std::string());

        /*!
                Use this method to synchronize (i.e. copy the joint values) from the remote robot to the local clone.
                The local clone must have the identical structure as the remote robot model, otherwise an error will be reported.
              */
        static bool synchronizeLocalClone(VirtualRobot::RobotPtr robot, RobotStateComponentInterfacePrx robotStatePrx);

        static bool synchronizeLocalClone(VirtualRobot::RobotPtr robot, SharedRobotInterfacePrx sharedRobotPrx);

        // VirtualRobot::RobotPtr getRobotPtr() { return shared_from_this();} // only for debugging

        //! Clones the structure of this remote robot to a local instance
        VirtualRobot::RobotPtr createLocalClone();

    protected:

        /// Not implemented yet
        virtual bool hasEndEffector(const std::string& endEffectorName);
        /// Not implemented yet
        virtual VirtualRobot::EndEffectorPtr getEndEffector(const std::string& endEffectorName);
        /// Not implemented yet
        virtual void getEndEffectors(std::vector<VirtualRobot::EndEffectorPtr>& storeEEF);

        /// Not implemented yet
        virtual void setRootNode(VirtualRobot::RobotNodePtr node);
        /// Not implemented yet
        virtual void registerRobotNode(VirtualRobot::RobotNodePtr node);
        /// Not implemented yet
        virtual void deregisterRobotNode(VirtualRobot::RobotNodePtr node);
        /// Not implemented yet
        virtual void registerRobotNodeSet(VirtualRobot::RobotNodeSetPtr nodeSet);
        /// Not implemented yet
        virtual void deregisterRobotNodeSet(VirtualRobot::RobotNodeSetPtr nodeSet);
        /// Not implemented yet
        virtual void registerEndEffector(VirtualRobot::EndEffectorPtr endEffector);
        /**
         * @brief Sets the global pose of the robot.
         * @param globalPose new global pose
         * @param applyValues No effect. Will be always applied.
         */
        virtual void setGlobalPose(const Eigen::Matrix4f& globalPose, bool applyValues = true);

        VirtualRobot::RobotNodePtr createLocalNode(SharedRobotNodeInterfacePrx remoteNode, std::vector<VirtualRobot::RobotNodePtr>& allNodes, std::map<VirtualRobot::RobotNodePtr, std::vector<std::string> >& childrenMap, VirtualRobot::RobotPtr robo);

    protected:
        SharedRobotInterfacePrx _robot;
        std::map<std::string, VirtualRobot::RobotNodePtr> _cachedNodes;
        VirtualRobot::RobotNodePtr _root;

        static boost::recursive_mutex m;

        static VirtualRobot::RobotNodePtr createRemoteRobotNode(SharedRobotNodeInterfacePrx, VirtualRobot::RobotPtr);
    };

    typedef boost::shared_ptr<RemoteRobot> RemoteRobotPtr;
}

#endif
