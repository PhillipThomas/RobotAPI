/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Stefan Ulbrich <stefan dot ulbrich at kit dot edu>, Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_CORE_KINEMATIC_UNIT_OBSERVER_H
#define _ARMARX_CORE_KINEMATIC_UNIT_OBSERVER_H

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/interface/core/RobotStateObserverInterface.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <ArmarXCore/observers/ConditionCheck.h>
#include <ArmarXCore/observers/Observer.h>

#include <string>

namespace VirtualRobot
{
    class Robot;
    typedef boost::shared_ptr<Robot> RobotPtr;
    class RobotNode;
    typedef boost::shared_ptr<RobotNode> RobotNodePtr;
}

namespace armarx
{

    /**
     * RobotStatePropertyDefinition Property Definitions
     */
    class RobotStateObserverPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        RobotStateObserverPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("TCPsToReport", "", "comma seperated list of nodesets' endeffectors, which poses and velocities that should be reported. * for all, empty for none");
        }
    };

    typedef ::std::map< ::std::string, ::armarx::FramedPoseBasePtr> FramedPoseBaseMap;

    /**
     * ArmarX RobotStateObserver.
     *
     * The RobotStateObserver allows to install conditions on all channel reported by the KinematicUnit.
     * These include joint angles, velocities, torques and motor temperatures
     *
     * The RobotStateObserver retrieves its configuration from a VirtualRobot robot model. Within the model, the joints
     * which are observer by the unit are define by a robotnodeset
     */
    class ARMARXCORE_IMPORT_EXPORT RobotStateObserver :
        virtual public Observer,
        virtual public RobotStateObserverInterface
    {
    public:
        RobotStateObserver();
        // framework hooks
        void onInitObserver();
        void onConnectObserver();

        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        void setRobot(VirtualRobot::RobotPtr robot);

        virtual std::string getDefaultName() const
        {
            return "RobotStateObserver";
        }

        void updatePoses();
        void updateNodeVelocities(const NameValueMap& jointVel);
    protected:

        void updateVelocityDatafields(const FramedDirectionMap& tcpTranslationVelocities, const FramedDirectionMap& tcpOrientationVelocities);

        void udpatePoseDatafields(const FramedPoseBaseMap& poseMap);
    private:
        std::string robotNodeSetName;
        RobotStateComponentInterfacePrx server;
        VirtualRobot::RobotPtr  robot, velocityReportRobot;
        std::vector<VirtualRobot::RobotNodePtr > nodesToReport;
        RecursiveMutex dataMutex;

        // RobotStateObserverInterface interface
    public:
        DatafieldRefBasePtr getPoseDatafield(const std::string& nodeName, const Ice::Current&) const;
    };

    typedef IceInternal::Handle<RobotStateObserver> RobotStateObserverPtr;
}

#endif
