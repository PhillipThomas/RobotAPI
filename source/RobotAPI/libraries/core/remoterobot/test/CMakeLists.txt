find_package(jsoncpp QUIET)

armarx_build_if(jsoncpp_FOUND "jsoncpp not available")

if (jsoncpp_FOUND)
    include_directories(${jsoncpp_INCLUDE_DIR})
endif()


set(LIBS ${LIBS} RobotAPICore ArmarXCoreJsonObject ${jsoncpp_LIBRARIES})

armarx_add_test(ArmarPoseTest ArmarPoseTest.cpp "${LIBS}")

