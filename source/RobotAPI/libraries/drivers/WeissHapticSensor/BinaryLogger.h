#ifndef BINARYLOGGER_H
#define BINARYLOGGER_H

#include <fstream>

class BinaryLogger
{
public:
    BinaryLogger(std::string filename);
    ~BinaryLogger();

    void logRead(unsigned char* buf, unsigned int len);
    void logWrite(unsigned char* buf, unsigned int len);
    void logText(std::string message);

private:
    std::ofstream log;
};

#endif // BINARYLOGGER_H
