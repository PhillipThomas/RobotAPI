#ifndef CALIBRATIONINFO_H
#define CALIBRATIONINFO_H

#include <Eigen/Core>
namespace armarx
{
    class CalibrationInfo
    {
    public:
        CalibrationInfo(Eigen::MatrixXf averageNoiseValues, Eigen::MatrixXf maximumValues, float calibratedMinimum, float calibratedMaximum);

        Eigen::MatrixXf applyCalibration(Eigen::MatrixXf rawData);

    private:
        Eigen::MatrixXf averageNoiseValues;
        Eigen::MatrixXf maximumValues;

        float calibratedMaximum;
        float calibratedMinimum;
    };
}

#endif // CALIBRATIONINFO_H
