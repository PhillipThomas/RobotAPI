#ifndef CHECKSUM_H
#define CHECKSUM_H

class Checksum
{
public:
    static unsigned short Crc16(unsigned char* data, unsigned int size);
    static unsigned short Update_crc16(unsigned char* data, unsigned int size, unsigned short crc);

private:
    static const unsigned short CRC_TABLE_CCITT16[256];
};

#endif // CHECKSUM_H
