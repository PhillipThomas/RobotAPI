#include "TextWriter.h"

using namespace armarx;

TextWriter::TextWriter(std::string filename)
{
    this->file.open(filename.c_str());
}

TextWriter::~TextWriter()
{
    file.close();
}

void TextWriter::writeLine(std::string message)
{
    file << message;
    file << std::endl;
    file.flush();
}
