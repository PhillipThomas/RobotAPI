#ifndef TEXTWRITER_H
#define TEXTWRITER_H

#include <fstream>

namespace armarx
{
    class TextWriter
    {
    public:
        TextWriter(std::string filename);
        ~TextWriter();

        void writeLine(std::string message);

    private:
        std::ofstream file;
    };
}

#endif
