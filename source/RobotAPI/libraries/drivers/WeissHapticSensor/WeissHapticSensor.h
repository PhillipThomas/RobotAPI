#ifndef WEISSHAPTICSENSOR_H
#define WEISSHAPTICSENSOR_H

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include "SerialInterface.h"
#include "TactileSensor.h"
#include <RobotAPI/interface/units/HapticUnit.h>
#include <ArmarXCore/util/variants/eigen3/Eigen3VariantObjectFactories.h>
//#include <ArmarXCore/util/variants/eigen3/Eigen3LibRegistry.h>
#include "TextWriter.h"
#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <RobotAPI/libraries/core/math/SlidingWindowVectorMedian.h>
#include <ArmarXCore/util/variants/eigen3/MatrixVariant.h>
#include <boost/thread/mutex.hpp>

namespace armarx
{

    class WeissHapticSensor;
    typedef boost::shared_ptr<WeissHapticSensor> WeissHapticSensorPtr;

    class WeissHapticSensor : public Logging
    {
    public:
        WeissHapticSensor(std::string device, int minimumReportIntervalMs);

        void connect();
        void disconnect();

        void setListenerPrx(HapticUnitListenerPrx listenerPrx);
        void startSampling();

        std::string getDeviceName();
        void scheduleSetDeviceTag(std::string tag);

    private:
        RunningTask<WeissHapticSensor>::pointer_type sensorTask;
        boost::shared_ptr<TextWriter> jsWriter;
        void frameAcquisitionTaskLoop();
        std::string device;
        std::string deviceFileName;
        boost::shared_ptr<SerialInterface> interface;
        boost::shared_ptr<TactileSensor> sensor;
        bool connected;
        string tag;
        tac_matrix_info_t mi;
        HapticUnitListenerPrx listenerPrx;
        void writeMatrixToJs(MatrixFloatPtr matrix, TimestampVariantPtr timestamp);

        bool setDeviceTagScheduled;
        std::string setDeviceTagValue;

        boost::mutex mutex;
        int minimumReportIntervalMs;
    };
}

#endif
