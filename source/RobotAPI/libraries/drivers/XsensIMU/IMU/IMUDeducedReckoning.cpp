/*
 * IMUDeducedReckoning.cpp
 *
 *  Created on: Mar 16, 2014
 *      Author: Dr.-Ing. David Israel González Aguirre
 *      Mail:   david.gonzalez@kit.edu
 */

#include "IMUDeducedReckoning.h"

namespace IMU
{
    CIMUDeducedReckoning::CIMUDeducedReckoning(const bool IsVerbose) :
        IIMUEventDispatcher(), m_IsVerbose(IsVerbose), m_G(IMU::CGeolocationInformation::GetGravitationalAcceleration())
    {
        SetEventHandling(CIMUEvent::eOnIMUStart, true);
        SetEventHandling(CIMUEvent::eOnIMUStop, true);
        SetEventHandling(CIMUEvent::eOnIMUCycle, false);
        SetEventHandling(CIMUEvent::eOnIMUFusedCycle, true);
        SetEventHandling(CIMUEvent::eOnIMUIntegratedState, false);
        SetEventHandling(CIMUEvent::eOnIMUCustomEvent, true);
    }

    CIMUDeducedReckoning::~CIMUDeducedReckoning()
    {
    }

    void CIMUDeducedReckoning::OnIMUEvent(const CIMUEvent& Event)
    {
        switch (Event.GetEventType())
        {
            case CIMUEvent::eOnIMUCycle:
                OnIMUCycle(Event.GetTimeStamp(), Event.GetIMU());
                break;

            case CIMUEvent::eOnIMUFusedCycle:
                OnIMUFusedCycle(Event.GetTimeStamp(), Event.GetIMU());
                break;

            case CIMUEvent::eOnIMUIntegratedState:
                OnIMUIntegratedState(Event.GetTimeStamp(), Event.GetIMU());
                break;

            case CIMUEvent::eOnIMUCustomEvent:
                OnIMUCustomEvent(Event);
                break;

            case CIMUEvent::eOnIMUStart:
                OnIMUStart(Event.GetTimeStamp(), Event.GetIMU());
                break;

            case CIMUEvent::eOnIMUStop:
                OnIMUStop(Event.GetTimeStamp(), Event.GetIMU());
                break;
        }
    }

    void CIMUDeducedReckoning::OnIMUStart(const timeval& TimeStamp, const CIMUDevice* pIMUDevice)
    {
        if (m_IsVerbose)
        {
            std::cout << "OnIMUStart(IMU Device ID = " << pIMUDevice->GetDeviceId() << ((GetDispatchingMode() == eCoupled) ? " Coupled" : " Decoupled") << ")" << std::endl;
            std::cout << "\t[ Global time  = " << CTimeStamp::GetElapsedSeconds(TimeStamp, pIMUDevice->GetReferenceTimeStamp()) << " s]" << std::endl;
            std::cout << "\t[ Latency  = " << CTimeStamp::GetElapsedMicroseconds(TimeStamp, GetLastStartTimeStamp()) << "]" << std::endl;

            if (GetDispatchingMode() == eDecoupled)
            {
                std::cout << "\t[ Pending Events  = " << GetTotalPendingEvents() << "]" << std::endl;
                std::cout << "\t[ Maximal Pending Events  = " << GetMaximalPendingEvents() << "]" << std::endl;
            }
        }
    }

    void CIMUDeducedReckoning::OnIMUStop(const timeval& TimeStamp, const CIMUDevice* pIMUDevice)
    {
        if (m_IsVerbose)
        {
            std::cout << "OnIMUStop(IMU Device ID = " << pIMUDevice->GetDeviceId() << ((GetDispatchingMode() == eCoupled) ? " Coupled" : " Decoupled") << ")" << std::endl;
            std::cout << "\t[ Global time  = " << CTimeStamp::GetElapsedSeconds(TimeStamp, pIMUDevice->GetReferenceTimeStamp()) << " s]" << std::endl;
            std::cout << "\t[ Latency  = " << CTimeStamp::GetElapsedMicroseconds(TimeStamp, GetLastStopTimeStamp()) << "]" << std::endl;

            if (GetDispatchingMode() == eDecoupled)
            {
                std::cout << "\t[ Pending Events  = " << GetTotalPendingEvents() << "]" << std::endl;
                std::cout << "\t[ Maximal Pending Events  = " << GetMaximalPendingEvents() << "]" << std::endl;
            }
        }
    }

    void CIMUDeducedReckoning::OnIMUCycle(const timeval& TimeStamp, const CIMUDevice* pIMUDevice)
    {
        const IMUState CurrentState = pIMUDevice->GetIMUState();

        if (m_IsVerbose)
        {
            std::cout << "OnIMUCycle(IMU Device ID = 0x" << std::hex << pIMUDevice->GetDeviceId() << ((GetDispatchingMode() == eCoupled) ? " Coupled" : " Decoupled") << std::dec << ")" << std::endl;
            std::cout << "\t[ Global time  = " << CTimeStamp::GetElapsedSeconds(TimeStamp, pIMUDevice->GetReferenceTimeStamp()) << " s]" << std::endl;
            std::cout << "\t[ Latency  = " << CTimeStamp::GetElapsedMicroseconds(TimeStamp, GetLastCycleReferenceTimeStamp()) << " µs]" << std::endl;

            if (GetDispatchingMode() == eDecoupled)
            {
                std::cout << "\t[ Pending Events  = " << GetTotalPendingEvents() << "]" << std::endl;
                std::cout << "\t[ Maximal Pending Events  = " << GetMaximalPendingEvents() << "]" << std::endl;
            }

            std::cout << "\t[ Cycle  = " << CurrentState.m_ControlData.m_MessageCounter << "]" << std::endl;
            std::cout << "\t[ Acceleration  = [" << CurrentState.m_PhysicalData.m_Acceleration[0] << "," << CurrentState.m_PhysicalData.m_Acceleration[1] << "," << CurrentState.m_PhysicalData.m_Acceleration[2] << "]" << std::endl;
            std::cout << "\t[ Acceleration Magnitude = [" << std::abs(CurrentState.m_PhysicalData.m_AccelerationMagnitud - m_G) * 1000.0f << " mm/s^2]" << std::endl;
            std::cout << "\t[ Gyroscope Rotation  = [" << CurrentState.m_PhysicalData.m_GyroscopeRotation[0] << "," << CurrentState.m_PhysicalData.m_GyroscopeRotation[1] << "," << CurrentState.m_PhysicalData.m_GyroscopeRotation[2] << "]" << std::endl;
            std::cout << "\t[ Magnetic Rotation  = [" << CurrentState.m_PhysicalData.m_MagneticRotation[0] << "," << CurrentState.m_PhysicalData.m_MagneticRotation[1] << "," << CurrentState.m_PhysicalData.m_MagneticRotation[2] << "]" << std::endl;
            std::cout << "\t[ Quaternion Rotation  = [" << CurrentState.m_PhysicalData.m_QuaternionRotation[0] << "," << CurrentState.m_PhysicalData.m_QuaternionRotation[1] << "," << CurrentState.m_PhysicalData.m_QuaternionRotation[2] << "," << CurrentState.m_PhysicalData.m_QuaternionRotation[3] << "]" << std::endl;
        }

        memcpy(m_OrientationQuaternion, CurrentState.m_PhysicalData.m_QuaternionRotation, sizeof(float) * 4);
        memcpy(m_MagneticRotation, CurrentState.m_PhysicalData.m_MagneticRotation, sizeof(float) * 3);
        memcpy(m_GyroscopeRotation, CurrentState.m_PhysicalData.m_GyroscopeRotation, sizeof(float) * 3);
        memcpy(m_Accelaration, CurrentState.m_PhysicalData.m_Acceleration, sizeof(float) * 3);
    }

    void CIMUDeducedReckoning::OnIMUFusedCycle(const timeval& TimeStamp, const CIMUDevice* pIMUDevice)
    {
        const IMUState CurrentState = pIMUDevice->GetIMUState();

        if (m_IsVerbose)
        {
            std::cout << "OnIMUFusedCycle(IMU Device ID = 0x" << std::hex << pIMUDevice->GetDeviceId() << ((GetDispatchingMode() == eCoupled) ? " Coupled" : " Decoupled") << std::dec << ")" << std::endl;
            std::cout << "\t[ Global time  = " << CTimeStamp::GetElapsedSeconds(TimeStamp, pIMUDevice->GetReferenceTimeStamp()) << " s]" << std::endl;
            std::cout << "\t[ Latency  = " << CTimeStamp::GetElapsedMicroseconds(TimeStamp, GetLastFusedCycleReferenceTimeStamp()) << " µs]" << std::endl;

            if (GetDispatchingMode() == eDecoupled)
            {
                std::cout << "\t[ Pending Events  = " << GetTotalPendingEvents() << "]" << std::endl;
                std::cout << "\t[ Maximal Pending Events  = " << GetMaximalPendingEvents() << "]" << std::endl;
            }

            std::cout << "\t[ Cycle  = " << CurrentState.m_ControlData.m_MessageCounter << "]" << std::endl;
            std::cout << "\t[ Acceleration  = [" << CurrentState.m_PhysicalData.m_Acceleration[0] << "," << CurrentState.m_PhysicalData.m_Acceleration[1] << "," << CurrentState.m_PhysicalData.m_Acceleration[2] << "]" << std::endl;
            std::cout << "\t[ Acceleration Magnitude = [" << std::abs(CurrentState.m_PhysicalData.m_AccelerationMagnitud - m_G) * 1000.0f << " mm/s^2]" << std::endl;
            std::cout << "\t[ Gyroscope Rotation  = [" << CurrentState.m_PhysicalData.m_GyroscopeRotation[0] << "," << CurrentState.m_PhysicalData.m_GyroscopeRotation[1] << "," << CurrentState.m_PhysicalData.m_GyroscopeRotation[2] << "]" << std::endl;
            std::cout << "\t[ Magnetic Rotation  = [" << CurrentState.m_PhysicalData.m_MagneticRotation[0] << "," << CurrentState.m_PhysicalData.m_MagneticRotation[1] << "," << CurrentState.m_PhysicalData.m_MagneticRotation[2] << "]" << std::endl;
            std::cout << "\t[ Quaternion Rotation  = [" << CurrentState.m_PhysicalData.m_QuaternionRotation[0] << "," << CurrentState.m_PhysicalData.m_QuaternionRotation[1] << "," << CurrentState.m_PhysicalData.m_QuaternionRotation[2] << "," << CurrentState.m_PhysicalData.m_QuaternionRotation[3] << "]" << std::endl;
        }

        memcpy(m_OrientationQuaternion, CurrentState.m_PhysicalData.m_QuaternionRotation, sizeof(float) * 4);
        memcpy(m_MagneticRotation, CurrentState.m_PhysicalData.m_MagneticRotation, sizeof(float) * 3);
        memcpy(m_GyroscopeRotation, CurrentState.m_PhysicalData.m_GyroscopeRotation, sizeof(float) * 3);
        memcpy(m_Accelaration, CurrentState.m_PhysicalData.m_Acceleration, sizeof(float) * 3);
    }

    void CIMUDeducedReckoning::OnIMUIntegratedState(const timeval& TimeStamp, const CIMUDevice* pIMUDevice)
    {
        const IMUState CurrentState = pIMUDevice->GetIMUState();

        if (m_IsVerbose)
        {
            std::cout << "OnIMUStateUpdate(IMU Device ID = 0x" << std::hex << pIMUDevice->GetDeviceId() << ((GetDispatchingMode() == eCoupled) ? " Coupled" : " Decoupled") << std::dec << ")" << std::endl;
            std::cout << "\t[ Global time  = " << CTimeStamp::GetElapsedSeconds(TimeStamp, pIMUDevice->GetReferenceTimeStamp()) << " s]" << std::endl;
            std::cout << "\t[ Latency = " << CTimeStamp::GetElapsedMicroseconds(TimeStamp, GetLastIntegratedStateReferenceTimeStamp()) << " µs]" << std::endl;

            if (GetDispatchingMode() == eDecoupled)
            {
                std::cout << "\t[ Pending Events  = " << GetTotalPendingEvents() << "]" << std::endl;
                std::cout << "\t[ Maximal Pending Events  = " << GetMaximalPendingEvents() << "]" << std::endl;
            }

            std::cout << "\t[ Cycle  = " << CurrentState.m_ControlData.m_MessageCounter << "]" << std::endl;
            std::cout << "\t[ Acceleration  = [" << CurrentState.m_PhysicalData.m_Acceleration[0] << "," << CurrentState.m_PhysicalData.m_Acceleration[1] << "," << CurrentState.m_PhysicalData.m_Acceleration[2] << "]" << std::endl;
            std::cout << "\t[ Acceleration Magnitude = [" << std::abs(CurrentState.m_PhysicalData.m_AccelerationMagnitud - m_G) * 1000.0f << " mm/s^2]" << std::endl;
            std::cout << "\t[ Gyroscope Rotation  = [" << CurrentState.m_PhysicalData.m_GyroscopeRotation[0] << "," << CurrentState.m_PhysicalData.m_GyroscopeRotation[1] << "," << CurrentState.m_PhysicalData.m_GyroscopeRotation[2] << "]" << std::endl;
            std::cout << "\t[ Magnetic Rotation  = [" << CurrentState.m_PhysicalData.m_MagneticRotation[0] << "," << CurrentState.m_PhysicalData.m_MagneticRotation[1] << "," << CurrentState.m_PhysicalData.m_MagneticRotation[2] << "]" << std::endl;
            std::cout << "\t[ Quaternion Rotation  = [" << CurrentState.m_PhysicalData.m_QuaternionRotation[0] << "," << CurrentState.m_PhysicalData.m_QuaternionRotation[1] << "," << CurrentState.m_PhysicalData.m_QuaternionRotation[2] << "," << CurrentState.m_PhysicalData.m_QuaternionRotation[3] << "]" << std::endl;
        }
    }

    void CIMUDeducedReckoning::OnIMUCustomEvent(const CIMUEvent& CustomEvent)
    {
        if (m_IsVerbose)
        {
            std::cout << "OnIMUCustomEvent(IMU Device ID = 0x" << std::hex << CustomEvent.GetIMU()->GetDeviceId() << ((GetDispatchingMode() == eCoupled) ? " Coupled" : " Decoupled") << std::dec << ")" << std::endl;
            std::cout << "\t[ Latency  = " << CTimeStamp::GetElapsedMicroseconds(CustomEvent.GetTimeStamp(), CustomEvent.GetIMU()->GetReferenceTimeStamp()) << " µs]" << std::endl;
            std::cout << "\t[ Latency  = " << CTimeStamp::GetElapsedMicroseconds(CustomEvent.GetTimeStamp(), GetLastCustomEventReferenceTimeStamp()) << " µs]" << std::endl;

            if (GetDispatchingMode() == eDecoupled)
            {
                std::cout << "\t[ Pending Events  = " << GetTotalPendingEvents() << "]" << std::endl;
                std::cout << "\t[ Maximal Pending Events  = " << GetMaximalPendingEvents() << "]" << std::endl;
            }
        }
    }
}
