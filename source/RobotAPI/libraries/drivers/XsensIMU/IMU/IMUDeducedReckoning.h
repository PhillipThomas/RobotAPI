/*
 * IMUDeducedReckoning.h
 *
 *  Created on: Mar 16, 2014
 *      Author: Dr.-Ing. David Israel González Aguirre
 *      Mail:   david.gonzalez@kit.edu
 */

#ifndef IMUDEDUCEDRECKONING_H_
#define IMUDEDUCEDRECKONING_H_

#include "IIMUEventDispatcher.h"
#include "IMUState.h"
#include "IMUDevice.h"

namespace IMU
{
    class CIMUDeducedReckoning : public IIMUEventDispatcher
    {
    public:

        CIMUDeducedReckoning(const bool IsVerbose);
        virtual ~CIMUDeducedReckoning();

        inline const float* GetOrientationQuaternion() const
        {
            return m_OrientationQuaternion;
        }

        inline const float* GetMagneticRotation() const
        {
            return m_MagneticRotation;
        }

        inline const float* GetGyroscopeRotation() const
        {
            return m_GyroscopeRotation;
        }

        inline const float* GetAccelaration() const
        {
            return m_Accelaration;
        }

    protected:

        virtual void OnIMUEvent(const CIMUEvent& Event);

        void OnIMUStart(const timeval& TimeStamp, const CIMUDevice* pIMUDevice);
        void OnIMUStop(const timeval& TimeStamp, const CIMUDevice* pIMUDevice);
        void OnIMUFusedCycle(const timeval& TimeStamp, const CIMUDevice* pIMUDevice);
        void OnIMUCycle(const timeval& TimeStamp, const CIMUDevice* pIMUDevice);
        void OnIMUIntegratedState(const timeval& TimeStamp, const CIMUDevice* pIMUDevice);
        void OnIMUCustomEvent(const CIMUEvent& CustomEvent);

        const bool m_IsVerbose;
        const float m_G;

        float m_OrientationQuaternion[4];
        float m_MagneticRotation[3];
        float m_GyroscopeRotation[3];
        float m_Accelaration[3];
    };
}

#endif
