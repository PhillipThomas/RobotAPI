/*
 * IMUEvent.cpp
 *
 *  Created on: Mar 16, 2014
 *     Author:  Dr.-Ing. David Israel González Aguirre
 *      Mail:   david.gonzalez@kit.edu
 */

#include "IMUEvent.h"
#include "IMUDevice.h"

namespace IMU
{
    uint32_t CIMUEvent::s_IdCounter = 0;
    pthread_mutex_t CIMUEvent::s_IdCounterMutex = PTHREAD_MUTEX_INITIALIZER;

    CIMUEvent::CIMUEvent(const timeval& TimeStamp, const EventType EventType, const CIMUDevice* pIMUDevice, const IMUState& EventState) :
        m_Id(CreatId()) ,
        m_TimeStamp(TimeStamp), m_EventType(EventType), m_pIMUDevice(pIMUDevice), m_IMUState(EventState)
    {
    }

    CIMUEvent::CIMUEvent(const timeval& TimeStamp, const EventType EventType, const CIMUDevice* pIMUDevice) :
        m_Id(CreatId()), m_TimeStamp(TimeStamp), m_EventType(EventType), m_pIMUDevice(pIMUDevice), m_IMUState(pIMUDevice->GetIMUState())
    {
    }

    CIMUEvent::CIMUEvent(const EventType EventType, const CIMUDevice* pIMUDevice) :
        m_Id(CreatId()), m_TimeStamp(CTimeStamp::GetCurrentTimeStamp()), m_EventType(EventType), m_pIMUDevice(pIMUDevice), m_IMUState(pIMUDevice->GetIMUState())
    {
    }

    CIMUEvent::CIMUEvent(const CIMUEvent& Event) :
        m_Id(CreatId()), m_TimeStamp(Event.m_TimeStamp), m_EventType(Event.m_EventType), m_pIMUDevice(Event.m_pIMUDevice), m_IMUState(Event.m_IMUState)
    {
    }

    CIMUEvent::CIMUEvent() :
        m_Id(CreatId()), m_TimeStamp(CTimeStamp::s_Zero), m_EventType(EventType(0x0)), m_pIMUDevice(NULL), m_IMUState()
    {
    }

    CIMUEvent::~CIMUEvent()
    {
    }

    uint32_t CIMUEvent::CreatId()
    {
        pthread_mutex_lock(&s_IdCounterMutex);
        uint32_t Id = CIMUEvent::s_IdCounter++;
        pthread_mutex_unlock(&s_IdCounterMutex);
        return Id;
    }
}
