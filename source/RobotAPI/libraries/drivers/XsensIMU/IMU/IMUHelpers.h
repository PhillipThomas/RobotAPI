/*
 * IMUHelpers.h
 *
 *  Created on: Mar 17, 2014
 *      Author: Dr.-Ing. David Israel González Aguirre
 *      Mail:   david.gonzalez@kit.edu
 */

#ifndef IMUHELPERS_H_
#define IMUHELPERS_H_

#include "Includes.h"

#define _MINIMAL___LOCK(MUTEX) pthread_mutex_lock(&MUTEX);
#define _MINIMAL_UNLOCK(MUTEX) pthread_mutex_unlock(&MUTEX);

namespace IMU
{
    class CTimeStamp
    {
    public:

        inline static timeval GetCurrentTimeStamp()
        {
            timeval TimeStamp;
            gettimeofday(&TimeStamp, NULL);
            return TimeStamp;
        }

        inline static float GetElapsedSeconds(const timeval& Post, const timeval& Pre)
        {
            return float(double(GetElapsedMicroseconds(Post, Pre)) / 1000000.0);
        }

        inline static float GetElapsedMilliseconds(const timeval& Post, const timeval& Pre)
        {
            return float(double(GetElapsedMicroseconds(Post, Pre)) / 1000.0);
        }

        inline static long GetElapsedMicroseconds(const timeval& Post, const timeval& Pre)
        {
            return ((Post.tv_sec - Pre.tv_sec) * 1000000) + (Post.tv_usec - Pre.tv_usec);
        }

        inline static long GetElapsedMicroseconds(const timeval& Pre)
        {
            timeval Post;
            gettimeofday(&Post, NULL);
            return ((Post.tv_sec - Pre.tv_sec) * 1000000) + (Post.tv_usec - Pre.tv_usec);
        }

        static const timeval s_Zero;
    };

    class CGeolocationInformation
    {
    public:

        static const float s_G_LPoles;
        static const float s_G_L45;
        static const float s_G_LEquator;

        //LatitudeInDegrees of your location:
        //http://www.mapsofworld.com/lat_long/germany-lat-long.html
        //49.0167 Karlsruhe, Germany

        static float GetGravitationalAcceleration(const float LatitudeInDegrees = 49.0167f)
        {
            return s_G_L45 - (s_G_LPoles - s_G_LEquator) * std::cos((float(M_PI) / 90.0f) * LatitudeInDegrees);
        }
    };



}

#endif
