/*
 * IMUState.h
 *
 *  Created on: Mar 16, 2014
 *      Author: Dr.-Ing. David Israel González Aguirre
 *      Mail:   david.gonzalez@kit.edu
 */

#ifndef IMUSTATE_H_
#define IMUSTATE_H_

#include "Includes.h"

namespace IMU
{
    struct IMUState
    {
        inline IMUState() :
            m_ControlData(), m_PhysicalData()
        {
        }

        struct ControlData
        {
            ControlData() :
                m_PreviousSampleCount(-1), m_CurrentSampleCount(0), m_MessageCounter(0), m_IsConsecutive(false)
            {
                memset(&m_TimeStamp, 0, sizeof(timeval));
            }

            long m_PreviousSampleCount;
            unsigned short m_CurrentSampleCount;
            long m_MessageCounter;
            bool m_IsConsecutive;
            timeval m_TimeStamp;
        };

        struct PhysicalData
        {
            PhysicalData() :
                m_AccelerationMagnitud(0.0f)
            {
                memset(m_Acceleration, 0, sizeof(float) * 3);
                memset(m_GyroscopeRotation, 0, sizeof(float) * 3);
                memset(m_MagneticRotation, 0, sizeof(float) * 3);
                memset(m_QuaternionRotation, 0, sizeof(float) * 4);
            }

            float m_Acceleration[3];
            float m_AccelerationMagnitud;
            float m_GyroscopeRotation[3];
            float m_MagneticRotation[3];
            float m_QuaternionRotation[4];

            inline void UpdateAccelerationMagnitud()
            {
                m_AccelerationMagnitud = std::sqrt(m_Acceleration[0] * m_Acceleration[0] + m_Acceleration[1] * m_Acceleration[1] + m_Acceleration[2] * m_Acceleration[2]);
            }
        };

        ControlData m_ControlData;
        PhysicalData m_PhysicalData;
    };
}

#endif
