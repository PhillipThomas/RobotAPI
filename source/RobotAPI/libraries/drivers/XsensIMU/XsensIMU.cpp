/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::XsensIMU
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "XsensIMU.h"



using namespace armarx;
using namespace IMU;


PropertyDefinitionsPtr XsensIMU::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new XsensIMUPropertyDefinitions(getConfigIdentifier()));
}

void XsensIMU::frameAcquisitionTaskLoop()
{
    while (!sensorTask->isStopped())
    {

        while (HasPendingEvents())
        {

            ProcessPendingEvent();

            TimestampVariantPtr now = TimestampVariant::nowPtr();
            IMUData data;

            data.acceleration.push_back(m_Accelaration[0]);
            data.acceleration.push_back(m_Accelaration[1]);
            data.acceleration.push_back(m_Accelaration[2]);

            data.gyroscopeRotation.push_back(m_GyroscopeRotation[0]);
            data.gyroscopeRotation.push_back(m_GyroscopeRotation[1]);
            data.gyroscopeRotation.push_back(m_GyroscopeRotation[2]);


            data.magneticRotation.push_back(m_MagneticRotation[0]);
            data.magneticRotation.push_back(m_MagneticRotation[1]);
            data.magneticRotation.push_back(m_MagneticRotation[2]);


            data.orientationQuaternion.push_back(m_OrientationQuaternion[0]);
            data.orientationQuaternion.push_back(m_OrientationQuaternion[1]);
            data.orientationQuaternion.push_back(m_OrientationQuaternion[2]);
            data.orientationQuaternion.push_back(m_OrientationQuaternion[3]);

            IMUTopicPrx->reportSensorValues("device", "name", data, now);

        }

        usleep(10000);
    }
}


/*
void XsensIMU::OnIMUCycle(const timeval& TimeStamp, const CIMUDevice* pIMUDevice)
{
    const IMUState CurrentState = pIMUDevice->GetIMUState();

    TimestampVariantPtr now = TimestampVariant::nowPtr();
    IMUData data;

    data.acceleration.push_back(CurrentState.m_PhysicalData.m_Acceleration[0]);
    data.acceleration.push_back(CurrentState.m_PhysicalData.m_Acceleration[1]);
    data.acceleration.push_back(CurrentState.m_PhysicalData.m_Acceleration[2]);

    data.gyroscopeRotation.push_back(CurrentState.m_PhysicalData.m_GyroscopeRotation[0]);
    data.gyroscopeRotation.push_back(CurrentState.m_PhysicalData.m_GyroscopeRotation[1]);
    data.gyroscopeRotation.push_back(CurrentState.m_PhysicalData.m_GyroscopeRotation[2]);

    data.magneticRotation.push_back(CurrentState.m_PhysicalData.m_MagneticRotation[0]);
    data.magneticRotation.push_back(CurrentState.m_PhysicalData.m_MagneticRotation[1]);
    data.magneticRotation.push_back(CurrentState.m_PhysicalData.m_MagneticRotation[2]);

    data.orientationQuaternion.push_back(CurrentState.m_PhysicalData.m_OrientationQuaternion[0]);
    data.orientationQuaternion.push_back(CurrentState.m_PhysicalData.m_OrientationQuaternion[1]);
    data.orientationQuaternion.push_back(CurrentState.m_PhysicalData.m_OrientationQuaternion[2]);
    data.orientationQuaternion.push_back(CurrentState.m_PhysicalData.m_OrientationQuaternion[3]);

    IMUTopicPrx->reportSensorValues("device", pIMUDevice->GetDeviceId(), data, now);

}

*/

void XsensIMU::onInitIMU()
{
    sensorTask = new RunningTask<XsensIMU>(this, &XsensIMU::frameAcquisitionTaskLoop);

    SetDispatchingMode(IMU::IIMUEventDispatcher::eDecoupled);
    SetMaximalPendingEvents(5);

    IMUDevice.SetFusion(IMU::CIMUDevice::eMeanFusion, 4);
    IMUDevice.RegisterEventDispatcher(this);

    IMUDevice.Connect(_IMU_DEVICE_DEFAUL_CONNECTION_, IMU::CIMUDevice::eSamplingFrequency_120HZ);
}

void XsensIMU::onStartIMU()
{
    IMUDevice.Start(false);
    sensorTask->start();
}

void XsensIMU::onExitIMU()
{
    IMUDevice.Stop();
    sensorTask->stop();
}

