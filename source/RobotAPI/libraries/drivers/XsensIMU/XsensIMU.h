/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::XsensIMU
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotAPI_XsensIMU_H
#define _ARMARX_COMPONENT_RobotAPI_XsensIMU_H


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>


#include <RobotAPI/components/units/InertialMeasurementUnit.h>

#include "IMU/IMU.h"

namespace armarx
{
    /**
     * @class XsensIMUPropertyDefinitions
     * @brief
     */
    class XsensIMUPropertyDefinitions:
        public InertialMeasurementUnitPropertyDefinitions
    {
    public:
        XsensIMUPropertyDefinitions(std::string prefix):
            InertialMeasurementUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("deviceConnection", "/dev/ttyUSB0", "");

            defineOptionalProperty<std::string>("frequency", "", "");
            defineOptionalProperty<std::string>("maxPendingEvents", "", "");
        }
    };

    /**
     * @class XsensIMU
     * @brief A brief description
     *
     * Detailed Description
     */
    class XsensIMU :
        virtual public InertialMeasurementUnit,
        virtual public IMU::CIMUDeducedReckoning
    {
    public:


        XsensIMU(): CIMUDeducedReckoning(false)
        {

        }

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "XsensIMU";
        }

    protected:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

        void frameAcquisitionTaskLoop();

        // InertialMeasurementUnit interface

        void onInitIMU();
        void onStartIMU();
        void onExitIMU();


    private:

        RunningTask<XsensIMU>::pointer_type sensorTask;
        IMU::CIMUDevice IMUDevice;
    };
}

#endif
