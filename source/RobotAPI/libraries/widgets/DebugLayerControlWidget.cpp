#include "DebugLayerControlWidget.h"
#include <RobotAPI/libraries/widgets/ui_DebugLayerControlWidget.h>

#define UPDATE_INTERVAL 1.0 // update every second

DebugLayerControlWidget::DebugLayerControlWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DebugLayerControlWidget)
{
    entityDrawer = NULL;
    ui->setupUi(this);

    //init timer
    SoSensorManager* sensor_mgr = SoDB::getSensorManager();
    timerSensor = new SoTimerSensor(onTimer, this);
    timerSensor->setInterval(SbTime(UPDATE_INTERVAL));
    sensor_mgr->insertTimerSensor(timerSensor);

    //connect signal mapper
    QObject::connect(&layerSignalMapperVisible,SIGNAL(mapped(QString)),this,SLOT(layerToggleVisibility(QString)));
    QObject::connect(&layerSignalMapperRemove,SIGNAL(mapped(QString)),this,SLOT(layerRemove(QString)));
}

DebugLayerControlWidget::~DebugLayerControlWidget()
{
    //destroy timer
    if (timerSensor)
    {
        SoSensorManager* sensor_mgr = SoDB::getSensorManager();
        sensor_mgr->removeTimerSensor(timerSensor);
    }
    delete ui;
}

void DebugLayerControlWidget::setEntityDrawer(DebugDrawerComponentPtr entityDrawer)
{
    this->entityDrawer = entityDrawer;
}

void DebugLayerControlWidget::updateLayers()
{
    //ui.layerTable->clear();
    if (entityDrawer) {
        armarx::LayerInformationSequence layers = entityDrawer->layerInformation();
        ui->layerTable->setRowCount(layers.size());

        for (std::size_t i=0;i<layers.size();++i)
        {
            const auto& layer=layers.at(i);
            QString name=QString::fromStdString(layer.layerName);

            //store visibility
            layerVisibility[layer.layerName]=layer.visible;

            //add name and number of elements
            ui->layerTable->setItem(i,0,new QTableWidgetItem{name});
            ui->layerTable->setItem(i,1,new QTableWidgetItem{QString::number(layer.elementCount)});

            //add visibility checkbox
            std::unique_ptr<QCheckBox> box{new QCheckBox};
            box->setChecked(layer.visible);
            layerSignalMapperVisible.setMapping(box.get(),name);
            QObject::connect(box.get(), SIGNAL(stateChanged(int)), &layerSignalMapperVisible, SLOT(map()));
            ui->layerTable->setCellWidget(i,2,box.release());

            //add remove button
            std::unique_ptr<QPushButton> removeB{new QPushButton("remove")};
            layerSignalMapperRemove.setMapping(removeB.get(),name);
            QObject::connect(removeB.get(), SIGNAL(clicked()), &layerSignalMapperRemove, SLOT(map()));
            ui->layerTable->setCellWidget(i, 3, removeB.release());
        }
    } else {
        VR_INFO << "No Debug Drawer" << std::endl;
    }
}

void DebugLayerControlWidget::layerToggleVisibility(QString layerName)
{
    //VR_INFO << "should toggle: " << layerName.toStdString() << std::endl;
    auto name = layerName.toStdString();
    if (layerVisibility.find(name)!=layerVisibility.end())
    {
        if (entityDrawer)
        {
            entityDrawer->enableLayerVisu(name, !layerVisibility.at(name));
        }
    } else
        VR_INFO << "name not present" << std::endl;
}

void DebugLayerControlWidget::layerRemove(QString layerName)
{
    auto name = layerName.toStdString();
    VR_INFO << "remove layer: " << name << std::endl;

    if (entityDrawer->hasLayer(name)) {
        entityDrawer->removeLayer(name);
    } else
        VR_INFO << "name not present" << std::endl;
}

void DebugLayerControlWidget::onTimer(void* data, SoSensor *sensor)
{
    DebugLayerControlWidget* controller = static_cast<DebugLayerControlWidget*>(data);
    if (controller)
        controller->updateLayers();
}
