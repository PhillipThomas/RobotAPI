#ifndef _ARMARX_XMLUSERCODE_ARMARX_WEISSHAPTICGROUP_WEISSHAPTICSENSORTESTGENERATEDBASE_H
#define _ARMARX_XMLUSERCODE_ARMARX_WEISSHAPTICGROUP_WEISSHAPTICSENSORTESTGENERATEDBASE_H

#include <ArmarXCore/statechart/xmlstates/XMLState.h>
#include "WeissHapticGroupStatechartContext.generated.h"
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <RobotAPI/interface/units/HapticUnit.h>
#include <RobotAPI/interface/units/WeissHapticUnit.h>

namespace armarx
{
    namespace WeissHapticGroup
    {
        template<typename StateType>
        class WeissHapticSensorTestGeneratedBase
            : virtual public XMLStateTemplate < StateType >,
              public XMLStateFactoryBase
        {
        protected:
            class WeissHapticSensorTestIn
            {
            private:
                WeissHapticSensorTestGeneratedBase<StateType>* parent;

            public:
                WeissHapticSensorTestIn(WeissHapticSensorTestGeneratedBase<StateType>* parent)
                    : parent(parent)
                {
                }
            }; // class WeissHapticSensorTestIn

            class WeissHapticSensorTestLocal
            {
            private:
                WeissHapticSensorTestGeneratedBase<StateType>* parent;

            public:
                WeissHapticSensorTestLocal(WeissHapticSensorTestGeneratedBase<StateType>* parent)
                    : parent(parent)
                {
                }

            public:
                std::map<std::string, ::armarx::DatafieldRefPtr> getTactileDatafields_MaximumValue() const
                {
                    return parent->State::getLocal< ::armarx::StringValueMap>("TactileDatafields_MaximumValue")->::armarx::StringValueMap::toStdMap< ::armarx::DatafieldRefPtr>();
                }
                void setTactileDatafields_MaximumValue(const std::map<std::string, ::armarx::DatafieldRefPtr>& value) const
                {
                    ::armarx::StringValueMapPtr container = ::armarx::StringValueMap::FromStdMap< ::armarx::DatafieldRefPtr>(value);
                    parent->State::setLocal("TactileDatafields_MaximumValue", *container);
                }
                bool isTactileDatafields_MaximumValueSet() const
                {
                    return parent->State::isLocalParameterSet("TactileDatafields_MaximumValue");
                }
            }; // class WeissHapticSensorTestLocal

            class WeissHapticSensorTestOut
            {
            private:
                WeissHapticSensorTestGeneratedBase<StateType>* parent;

            public:
                WeissHapticSensorTestOut(WeissHapticSensorTestGeneratedBase<StateType>* parent)
                    : parent(parent)
                {
                }
            }; // class WeissHapticSensorTestOut

        protected:
            const WeissHapticSensorTestIn in;
            const WeissHapticSensorTestLocal local;
            const WeissHapticSensorTestOut out;

        public:
            WeissHapticSensorTestGeneratedBase(const XMLStateConstructorParams& stateData)
                : XMLStateTemplate < StateType > (stateData),
                  in(WeissHapticSensorTestIn(this)),
                  local(WeissHapticSensorTestLocal(this)),
                  out(WeissHapticSensorTestOut(this))
            {
            }
            WeissHapticSensorTestGeneratedBase(const WeissHapticSensorTestGeneratedBase& source)
                : IceUtil::Shared(source),
                  armarx::StateIceBase(source),
                  armarx::StateBase(source),
                  armarx::StateController(source),
                  armarx::State(source),
                  XMLStateTemplate < StateType > (source),
                  in(WeissHapticSensorTestIn(this)),
                  local(WeissHapticSensorTestLocal(this)),
                  out(WeissHapticSensorTestOut(this))
            {
            }

        public:
            HapticUnitObserverInterfacePrx getHapticObserver() const
            {
                return StateBase::getContext<WeissHapticGroupStatechartContext>()->getHapticObserver();
            }
            WeissHapticUnitInterfacePrx getWeissHapticUnit() const
            {
                return StateBase::getContext<WeissHapticGroupStatechartContext>()->getWeissHapticUnit();
            }
            static std::string GetName()
            {
                return "WeissHapticSensorTest";
            }
            void __forceLibLoading()
            {
                // Do not call this method.
                // The sole purpose of this method is to force the compiler/linker to include all libraries.
                ::armarx::DatafieldRef type1;
            }
        }; // class WeissHapticSensorTestGeneratedBase
    } // namespace WeissHapticGroup
} // namespace armarx

#endif // _ARMARX_XMLUSERCODE_ARMARX_WEISSHAPTICGROUP_WEISSHAPTICSENSORTESTGENERATEDBASE_H
