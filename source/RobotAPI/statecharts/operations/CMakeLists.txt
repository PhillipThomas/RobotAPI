
armarx_set_target("Core Library: RobotAPIOperations")
find_package(Eigen3 QUIET)
find_package(Simox ${ArmarX_Simox_VERSION} QUIET)

armarx_build_if(Eigen3_FOUND "Eigen3 not available")
armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")

include_directories(${Eigen3_INCLUDE_DIR})
include_directories(${Simox_INCLUDE_DIRS})

set(LIB_NAME       RobotAPIOperations)



set(LIBS RobotAPIUnits ArmarXCoreStatechart ArmarXCoreObservers)

set(LIB_FILES RobotControl.cpp)
set(LIB_HEADERS RobotControl.h)

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")
