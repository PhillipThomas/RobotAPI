/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    Robot::
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_COMPONENT_ROBOTCONTROL_H
#define _ARMARX_COMPONENT_ROBOTCONTROL_H

#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/interface/operations/RobotControlIceBase.h>

namespace armarx
{
    // ****************************************************************
    // Component and context
    // ****************************************************************

    struct RobotControlContextProperties : StatechartContextPropertyDefinitions
    {
        RobotControlContextProperties(std::string prefix):
            StatechartContextPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("proxyName", "", "name of the proxy to load");
            defineOptionalProperty<std::string>("stateName", "", "name of the state to load");
        }
    };



    /**
     * \class RobotControl
     * \brief RobotControl is used for dynamically loading and starting robot programs.
     * \ingroup RobotAPI-Statecharts
     *
     * The toplevel state Statechart_Robot describes the basic operational levels
     * of the robot.
     *
     * The behavior of the functional state is described in StateRobotControl
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT RobotControl :
        virtual public RemoteStateOfferer<StatechartContext>,
        virtual public RobotControlIceBase
    {
    public:
        /**
         * Refernce to the currently active Functionsl state.
         */
        StateBasePtr robotFunctionalState;
        // inherited from RemoteStateOfferer
        std::string getStateOffererName() const
        {
            return "RobotControl";
        }

        void onInitRemoteStateOfferer();
        void onConnectRemoteStateOfferer();
        void onExitRemoteStateOfferer();
        void startRobotStatechart();

        void hardReset(const Ice::Current& = ::Ice::Current());

        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    private:
        void createStaticInstance();
        RunningTask< RobotControl >::pointer_type task;
        int stateId;
    };

    DEFINEEVENT(EvRobotFailure)
    DEFINEEVENT(EvStopRobot)
    DEFINEEVENT(EvStartRobot)
    DEFINEEVENT(EvLoadingFailed)
    /**
     * Statechart which describes the most basic states of a robot:

       \dot
           digraph States {
               functional [ label="Functional (Initial State)" ];
               stopped [ label="Stopped" ];
               failure [ label="Failure" ];
               functional -> failure [ label="Failure" ];
               functional -> stopped [ label="EvStopRobot" ];
               stopped -> functional [ label="EvStartRobot" ];
               failure -> functional [ label="EvStartRobot" ];
           }
       \enddot
     */
    struct Statechart_Robot :
            StateTemplate<Statechart_Robot>
    {
        void defineState();
        void defineSubstates();
    };


    DEFINEEVENT(EvInit)
    DEFINEEVENT(EvInitialized)
    DEFINEEVENT(EvLoadScenario)
    DEFINEEVENT(EvStartScenario)
    DEFINEEVENT(EvInitFailed)
    /**
     * Statechart which describes the operational states of a robot program.
     * The state "Robot Program Running" is a DynamicRemoteState and needs to
     * be passed the input parameters \p proxyName and \p stateName via Event
     * parameters.

       \dot
           digraph States {
               idle [ label="Idling" ];
               preinit [ label="RobotPreInitialized" ];
               init [ label="RobotInitialized (Initial State)" ];
               running [ label="Robot Program Running" ];
               error [ label="FatalError" ];
               idle -> preinit [ label="EvInit" ];
               preinit -> init [ label="EvInitialized" ];
               init -> running [ label="EvLoadScenario" ];
               running -> preinit [ label="EvInit" ];
               running -> preinit [ label="Success" ];
               running -> error [ label="EvLoadingFailed" ];
               preinit -> error [ label="EvInitFailed" ];
               ALL -> error [ label="Failure" ];
           }
       \enddot
     */
    struct StateRobotControl :
            StateTemplate<StateRobotControl>
    {
        void onEnter();
        void defineSubstates();
    };

    /**
     * Robot is in the state preinitialized.
     */
    struct RobotPreInitialized :
            StateTemplate<RobotPreInitialized>
    {
        RobotPreInitialized();
        void onEnter();
    };
}

#endif
